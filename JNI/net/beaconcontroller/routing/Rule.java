package net.beaconcontroller.routing;

public class Rule{
    
    
    Rule(long sw, long matchsrc, long matchdst, short srcPort, short dstPort, long nexthop, int priority) {
        this.sw = sw;
        this.matchsrc = matchsrc;
        this.matchdst = matchdst;
        this.srcPort = srcPort;
        this.dstPort = dstPort;
        this.nexthop = nexthop;
        this.priority = priority;
    }
    
    public String toString() {
        return "switch: "+ sw + ", src:" + matchsrc + ", dst:" + matchdst + ", srcport:" + srcPort + ", dstPort:" + dstPort + ", next:" + nexthop + ", " + priority;
    }
    
    // No star (0), one star (1) , all star (2)
    int priority;
    // Id switch
    long sw;
    // Id next switch
    long nexthop;
    // Entry port
    short srcPort;
    // Exit port
    short dstPort;
    // Src
    long matchsrc;
    // Dest
    long matchdst;
    
    public native int getSw();
    public native void setSw(int sw);
    
    public native int getNexthop() ;
    public native void setNexthop(int nexthop);
    public native short getSrcPort() ;
    public native void setSrcPort(short srcPort);
    public native short getDstPort();
    public native void setDstPort(short dstPort);
    public native String getMatchsrc();
    public native void setMatchsrc(String matchsrc);
    public native String getMatchdst() ;
    public native void setMatchdst(String matchdst);
    public native int getPriority();
    public native void setPriority(int priority) ;
    
}