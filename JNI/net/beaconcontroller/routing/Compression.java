package net.beaconcontroller.routing;

public class Compression {
    static {
      System.load("/Users/nhuin/Documents/These/CompressEAR/JNI/compression.jnilib"); // Load native library at runtime
    }

    
    public native void setTableLimit(int tableLimit);
    // adds the nodes 
    public native void addNodes(long[] nodes);
    // sets the links 
    public native void addLink(long srcNodeID, long dstNodeID, short srcPort, short dstPort, double capacity);
    // to initialize the algorithm object, to call after setTableLimit, addNodes & addLink
    public native void init();
    // deletes a link from the topology
    public native void deleteLink(long srcNodeID, long dstNodeID);
    // returns the route to the destination 
    public native Rule[] calculateRoute(long srcnode, long dstnode, double charge);
    // returns the list of rules that correspond to the links to be turned off 
    public native void turnOffLinks();
    // returns the list of new rules to be added
    public native Rule[] getRules(long node);
    // to call at the end, free memory
    public native void exit();
    // returns true if table of node is full
    public native boolean isFull(long node);
    // compress table of node
    public native void compress(long node);
    // returns the number of rules of node
    public native int getNbRules(long node);
    // returns true the actual number of rules of node
    public native int getActualNbRules(long node);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
        long n0 = 9223372036854775807L;
        long n1 = 9223372036854775806L;
        long n2 = 9223372036854775805L;
        long n3 = 9223372036854775804L;
        long[] nodes = {n0, n1, n2, n3};
        Compression comp = new Compression();
        comp.addNodes(nodes);
        comp.addLink(n3, n2, (short)0, (short)2, 1000);
        comp.addLink(n2, n3, (short)0, (short)1, 1000);
        
        comp.addLink(n2, n1, (short)1, (short)2, 1000);
        comp.addLink(n1, n2, (short)0, (short)3, 1000);
        
        comp.addLink(n1, n0, (short)1, (short)1, 1000);
        comp.addLink(n0, n1, (short)0, (short)3, 1000);
        
        comp.setTableLimit(3);
        comp.init();
        System.out.println("Add flow " + n3 + ", " + n0);
        Rule[] rules = comp.calculateRoute(n3, n0, 1);
        if (rules == null) {
            System.out.println("Can't find route");
            return;
        } else {
            System.out.print("Rules :");
            for(Rule r : rules) {
                System.out.println(r);
            }
        }
        System.out.println();

        // System.out.print('{');
        for(Rule r : rules) {
            if(comp.isFull(r.sw)) {
                comp.compress(r.sw);
            }
            // System.out.println(r);
        }
        // System.out.println('}');

        System.out.println("Add flow " + n3 + ", " + n0);
        rules = comp.calculateRoute(n3, n0, 1);
        if (rules == null) {
            System.out.println("Can't find route");
            return;
        } else {
            System.out.print("Rules :");
            for(Rule r : rules) {
                System.out.println(r);
            }
        }
        System.out.println();

        // System.out.print('{');
        for(Rule r : rules) {
            if(comp.isFull(r.sw)) {
                comp.compress(r.sw);
            }
            // System.out.println(r);
        }
        // System.out.println('}');

        System.out.println("Add flow " + n2 + ", " + n0);
        rules = comp.calculateRoute(n2, n0, 1);
        if (rules == null) {
            System.out.println("Can't find route");
            return;
        } else {
            System.out.print("Rules :");
            for(Rule r : rules) {
                System.out.println(r);
            }
        }
        System.out.println();

        // System.out.print('{');
        for(Rule r : rules) {
            if(comp.isFull(r.sw)) {
                comp.compress(r.sw);
            }
        //     System.out.println(r);
        }
        // System.out.println('}');

        System.out.println("Add flow " + n2 + ", " + n0);
        rules = comp.calculateRoute(n2, n0, 1);
        if (rules == null) {
            System.out.println("Can't find route");
            return;
        } else {
            System.out.print("Rules :");
            for(Rule r : rules) {
                System.out.println(r);
            }
        }
        System.out.println();

        // System.out.print('{');
        for(Rule r : rules) {
            if(comp.isFull(r.sw)) {
                comp.compress(r.sw);
            }
        //     System.out.println(r);
        }
        // System.out.println('}');
        
        System.out.println("Add flow "+ n1 + ", " + n0);
        rules = comp.calculateRoute(n1, n0, 1);
        if (rules == null) {
            System.out.println("Can't find route");
            return;
        } else {
            System.out.print("Rules :");
            for(Rule r : rules) {
                System.out.println(r);
            }
        }
        System.out.println();

        // System.out.print('{');
        for(Rule r : rules) {
            if(comp.isFull(r.sw)) {
                comp.compress(r.sw);
            }
        //     System.out.println(r);
        }
        // System.out.println('}');
        
        for(long node : nodes ) {
            System.out.println("Rules for " + node);
            for(Rule r : comp.getRules(node)) {
                System.out.print(r);
            }
            System.out.println();
        }
        comp.turnOffLinks();
        comp.exit();
	}
}
