#include <cassert>
#include <chrono>
#include <fstream> //loadDemandsFromFile
#include <functional>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <utility> //loadDemandsFromFile
#include <vector>  //loadDemandsFromFile

#include "DiGraph.hpp"

#include "CplexRules.hpp"
#include "DefaultPortRules.hpp"
#include "MostSaveRulesB.hpp"
#include "WildcardRules.hpp"

#include "Routing.hpp"

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

std::vector<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    int s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        // std::cout << line << std::endl;
        std::stringstream lineStream(line);
        if (line != "") {
            lineStream >> s >> t >> d;
            // std::cout << s << ", " << t << ", "<< d << std::endl;
            demands.emplace_back(s, t, d * percent);
        }
    }
    return demands;
}

// template<typename Rules, typename Weight>
// Routing<Rules> runPathRestart(int _maxRule, const DiGraph& topology, int
// _nbServers, int _nbSwitches, const std::vector<Demand>& demands) {

//     Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

//     std::set<Graph::Edge> nonRemovableEdge;

//     if(!ear.template routeAllDemands<Weight>(demands)) {
//         return ear;
//     }

//     Routing<Rules> solution = ear;

//     while (true) {
//         auto edges = ear.getRemovableEdges(nonRemovableEdge);
//         if(edges.empty()) {
//             break;
//         }

//         for (const auto& edge : edges) {
//             auto demands = ear.deleteLink(edge);
//             if (ear.template routeAllDemands<Weight>(demands)) {
//                 solution = ear;
//             } else {
//                 nonRemovableEdge.insert(edge);
//                 ear = solution;
//                 break;
//             }
//         }
//     }
//     return solution;
// }

// template<typename Rules, typename Weight>
// Routing<Rules> runNoCompress(int _maxRule, const DiGraph& topology, int
// _nbServers, int _nbSwitches, const std::vector<Demand>& demands) {

//     Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

//     std::set<Graph::Edge> nonRemovableEdge;

//     if(!ear.template routeAllDemandsNoCompress<Weight>(demands)) {
//         return ear;
//     }

//     Routing<Rules> solution = ear;

//     while (true) {
//         auto edges = ear.getRemovableEdges(nonRemovableEdge);
// //        std::cout << nonRemovableEdge.size() << std::endl;
//         if(edges.empty()) {
//             break;
//         }

//         for (const auto& edge : edges) {
//             auto demands = ear.deleteLink(edge);
//             if (ear.template routeAllDemandsNoCompress<Weight>(demands)) {
//                 solution = ear;
//             } else {
//                 nonRemovableEdge.insert(edge);
//                 ear = solution;
//                 break;
//             }
//         }
//     }
//     return solution;
// }

// template<typename Rules, typename Weight>
// Routing<Rules> runFullRestart(int _maxRule, const DiGraph& topology, int
// _nbServers, int _nbSwitches, const std::vector<Demand>& demands) {

//     Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

//     std::set<Graph::Edge> nonRemovableEdge;

//     if(!ear.template routeAllDemands<Weight>(demands)) {
//         return ear;
//     }

//     auto solution = ear;
//     Routing<Rules> base(topology, _nbServers, _nbSwitches, _maxRule);
//     std::list<std::pair<int, int>> toRemove;
//     while (true) {
//         auto edges = ear.getRemovableEdges(nonRemovableEdge);
//         if(edges.empty()) {
//             break;
//         }
//         for (const auto& edge : edges) {
//             ear = base;
//             ear.deleteEdge(edges.front());
//             if(ear.template routeAllDemands<Weight>(demands)) {
//                 base.deleteEdge(edge);
//                 std::swap(solution, ear);
//                 break;
//             } else {
//                 nonRemovableEdge.insert(edges.front());
//                 break;
//             }
//         }
//     }
//     return solution;
// }

template <typename Rules, typename Weight>
Routing<Rules> runOnce(int _maxRule, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands) {

    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);
    ear.template routeAllDemands<Weight>(demands);
    // ear.compressAll();
    return ear;
}

template <typename Rules, typename Weight>
Routing<Rules> runOnceEnd(int _maxRule, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands) {

    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);
    ear.template routeAllDemands<Weight>(demands);
    ear.compressAll();
    return ear;
}

template <typename Rules, typename Weight>
Routing<Rules> runOnceFinal(int _maxRule, const DiGraph& topology,
    int _nbServers, int _nbSwitches,
    const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

    ear.template routeAllDemands<Weight>(demands, false);
    ear.compressAll();
    return ear;
}

template <typename Rules, typename Weight>
Routing<Rules> runOnceFinalNoLimit(int _maxRule, const DiGraph& topology,
    int _nbServers, int _nbSwitches,
    const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

    ear.template routeAllDemandsNoLimit<Weight>(demands, false);
    ear.compressAll();
    return ear;
}

// template<typename Rules, typename Weight>
// Routing<Rules> runCheat(int _maxRule, const DiGraph& topology, int
// _nbServers, int _nbSwitches, const std::vector<Demand>& demands) {

//     Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);
//     ear.cheatRouteAllDemands(demands);
//     ear.compressAll();
//     return ear;
// }

template <typename Rules, typename Weight>
Routing<Rules> runForceDemandsOnce(int _maxRule, const DiGraph& topology,
    int _nbServers, int _nbSwitches,
    const std::vector<Demand>& demands) {

    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);
    ear.template forceRouteAllDemands<Weight>(demands);
    ear.template cleanFullRules<Weight>();
    return ear;
}

template <typename Rules>
std::function<Routing<Rules>(int, const DiGraph&, int, int,
    const std::vector<Demand>&)>
getHeuristic(const std::string& heuristicName,
    const std::string& weightFunctionName) {

    typedef std::function<Routing<Rules>(int, const DiGraph&, int, int,
        const std::vector<Demand>&)>
        HeuristicFunction;
#define findHeuristic(name, func)                                                    \
    if (heuristicName.compare(#name) == 0) {                                         \
        if (weightFunctionName.compare("dumb") == 0) {                               \
            run = func<Rules, typename Routing<Rules>::DumbWeight>;                  \
        } else if (weightFunctionName.compare("metric") == 0) {                      \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 1>>; \
        } else if (weightFunctionName.compare("metricPlusOne") == 0) {               \
            run = func<Rules,                                                        \
                typename Routing<Rules>::template MetricPlusOneWeight<1, 1>>;        \
        } else if (weightFunctionName.compare("metricPlusOneCapa") == 0) {           \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 0>>; \
        } else if (weightFunctionName.compare("metricPlusOneRules") == 0) {          \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<0, 1>>; \
        } else if (weightFunctionName.compare("metricPlusOne75Rules") == 0) {        \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 3>>; \
        } else if (weightFunctionName.compare("metricPlusOne75Capa") == 0) {         \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<3, 1>>; \
        }                                                                            \
    }

    HeuristicFunction run;

    findHeuristic(once, runOnce) else findHeuristic(
        onceFinal, runOnceFinal) else findHeuristic(onceFinalNoLimit,
        runOnceFinalNoLimit)
        // else findHeuristic(cheat, runCheat)
        else findHeuristic(forceOnce, runForceDemandsOnce) else {
        std::cout << "No heuristic given!" << std::endl;
        exit(-1);
    }
    return run;
}

template <typename Rules>
void launch(int _nbRules, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands,
    const std::string& name, const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl
              << "Percent: " << percent << std::endl;

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();
    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);

    auto solution = run(_nbRules, topology, _nbServers, _nbSwitches, demands);
    double pathFindingTime =
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count();
    std::cout << pathFindingTime << "ms. " << std::endl;

    if (heuristicName.compare("forceOnce") != 0 and (std::size_t) solution.getNbDemands() != demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << "\t" << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem()
                  << std::endl;
    }

    for (auto& i : solution.getVertices()) {
        if (solution.isSwitch(i)) {
            auto& rule = solution.getRules(i);
#ifdef DEBUG
            if (!rule.isCompressionValid()) {
                std::cout << "Compression not valid for " << i << std::endl;
            }
#endif
            std::cout << i << ':' << rule.getCompressedRules().size() << "/"
                      << rule.getActualNbRules() << '\t'
                      << 100 * (rule.getCompressedRules().size() / (double)rule.getActualNbRules())
                      << '\t' << solution.getDensity(i) << '\t'
                      << solution.getResidual().getOutDegree(i) << '\t';
            if (rule.getCompressedRules().size() <= 3) {
                for (auto& r : rule.getCompressedRules()) {
                    std::cout << r << ", ";
                }
            }
            std::cout << std::endl;
        }
    }

#ifdef DYNAMIC
    std::string resultsFolder("results_dynamic");
#else
    std::string resultsFolder("results");
#endif

    mkdir(std::string("./" + resultsFolder + "/" + name).c_str(), 0700);
    solution.saveMetrics("./" + resultsFolder + "/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_" + std::to_string(_nbRules) + "_" + std::to_string(percent) + ".txt",
        demands.size());
}

template <typename Rules>
void searchMini(const DiGraph& topology, int _nbServers, int _nbSwitches,
    const std::vector<Demand>& demands, const std::string& name,
    const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);

    int minRules = 0;
    int maxRules = std::numeric_limits<int>::max();

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl;

    int nbRulesTest = std::numeric_limits<int>::max() / 2;
    std::cout << "Test with " << nbRulesTest << " max mem =>" << std::flush;

    Routing<Rules> solution =
        run(nbRulesTest, topology, _nbServers, _nbSwitches, demands);

    if (solution.getNbDemands() != (int)demands.size()) {
        minRules = nbRulesTest;
        std::cout << " failed. " << solution.getNbDemands() << '/' << demands.size()
                  << std::endl;
    } else {
        std::cout << " success." << std::endl;
        maxRules = nbRulesTest;
    }

    while (minRules + 1 < maxRules) {
        nbRulesTest = (maxRules - minRules) / 2 + minRules;
        std::cout << "Test with " << nbRulesTest << " max mem =>" << std::flush;
        auto ear = run(nbRulesTest, topology, _nbServers, _nbSwitches, demands);
        if ((std::size_t)ear.getNbDemands() != demands.size()) {
            minRules = nbRulesTest;
            std::cout << " failed. " << ear.getNbDemands() << '/' << demands.size()
                      << std::endl;
        } else {
            std::cout << " success." << std::endl;
            maxRules = nbRulesTest;
            // nbRules = nbRulesTest;
            solution = std::move(ear);
        }
    }

    if ((std::size_t)solution.getNbDemands() != demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << " " << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem()
                  << std::endl;
    }

    for (std::size_t i = 0; i < solution.getRules().size(); ++i) {
        auto& rule = solution.getRules()[i];
#ifdef DEBUG
        if (!rule.isCompressionValid()) {
            std::cout << "Compression not valid for " << i << std::endl;
            for (auto& neighbor : solution.getResidual().getNeighbors(i)) {
                std::cout << neighbor << ", ";
            }
            std::cout << std::endl;
        }
#endif
        std::cout << i << ":" << rule.getCompressedRules().size() << "/"
                  << rule.getActualNbRules() << std::endl;
    }

#ifdef DYNAMIC
    std::string resultsFolder("results_dynamic");
#else
    std::string resultsFolder("results");
#endif

    mkdir(std::string("./" + resultsFolder + "/" + name).c_str(), 0700);
    solution.saveMetrics("./" + resultsFolder + "/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_mini_" + std::to_string(percent) + ".txt",
        demands.size());
}

void findSolution(int argc, char** argv) {
    std::string name(argv[1]);
    auto demands = loadDemandsFromFile(
        std::string("./instances/" + name + "_demand.txt"), std::stod(argv[6]));
    auto topology =
        DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));

    std::string strNbRules(argv[5]);

    int nbRules = 0;
    std::string method = std::string(argv[2]);

    if (strNbRules.compare("max") == 0) {
        nbRules = std::numeric_limits<int>::max();
    } else if (strNbRules.compare("mini") == 0) {
        if (method == "Heur") {
            searchMini<WildcardRules>(
                std::get<0>(topology), std::get<1>(topology), std::get<2>(topology),
                demands, name, "WildcardRules", argv[4], argv[3], std::stod(argv[6]));
        } else if (method == "Heur-DP") {
            searchMini<DefaultPortRules>(std::get<0>(topology), std::get<1>(topology),
                std::get<2>(topology), demands, name,
                "DefaultPortRules", argv[4], argv[3],
                std::stod(argv[6]));
        } else if (method == "HeurSB") {
            searchMini<MostSaveRulesB>(
                std::get<0>(topology), std::get<1>(topology), std::get<2>(topology),
                demands, name, "WildcardRules", argv[4], argv[3], std::stod(argv[6]));
        } else if (method == "LP") {
            searchMini<CplexRules>(std::get<0>(topology), std::get<1>(topology),
                std::get<2>(topology), demands, name, "CplexRules",
                argv[4], argv[3], std::stod(argv[6]));
        } else {
            std::cerr << "No Rule type given!: " << method << std::endl;
            exit(-1);
        }
        return;
    } else {
        nbRules = std::stoi(strNbRules);
    }

    if (method == "Heur") {
        launch<WildcardRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "WildcardRules",
            argv[4], argv[3], std::stod(argv[6]));
    } else if (method == "Heur-DP") {
        launch<DefaultPortRules>(nbRules, std::get<0>(topology),
            std::get<1>(topology), std::get<2>(topology),
            demands, name, "DefaultPortRules", argv[4],
            argv[3], std::stod(argv[6]));
    } else if (method == "HeurSB") {
        launch<MostSaveRulesB>(nbRules, std::get<0>(topology),
            std::get<1>(topology), std::get<2>(topology),
            demands, name, "HeurSB", argv[4], argv[3],
            std::stod(argv[6]));
    } else if (method == "LP") {
        launch<CplexRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "CplexRules",
            argv[4], argv[3], std::stod(argv[6]));
    } else {
        std::cerr << "No Rule type given!: " << method << std::endl;
        exit(-1);
    }
}

int main(int argc, char** argv) {
    findSolution(argc, argv);
    return 0;
}