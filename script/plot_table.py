import matplotlib.pyplot as plt
import numpy as np
import Compression
import json

def getMetrics(instance, heuristic, weight, rules, nbRules, capa):
	with open("./results/{}_{}_{}_{}_{}_{:.6f}.txt".format(instance, heuristic, weight, rules, nbRules, capa), 'rb') as datafile:
		return json.load(datafile)

def texNumber(n):
	return r"\num{"+str(n)+r"}"

def printTimeTable():
	with open("table.tex", "w") as texFile:
		texFile.write(r"\documentclass{article}"+"\n")
		texFile.write(r"\usepackage{siunitx}"+"\n")
		texFile.write(r"\usepackage{fullpage}"+"\n")
		texFile.write(r"\usepackage{graphicx}"+"\n") # resizebox
		texFile.write(r"\usepackage{multirow}"+"\n")
		texFile.write(r"\begin{document}"+"\n")
		for nbRules in Compression.TABLE_SIZES:
			texFile.write(r"\section{"+str(nbRules)+" table size}\n")
			texFile.write(r"\resizebox{\linewidth}{!}{"+"\n")
			texFile.write(r"\begin{tabular}{|l|c|"+("|".join(["c" for i in range(3*len(Compression.COMPRESSION_MODULES))]))+"|}"+"\n")
			texFile.write(r"\hline"+"\n")

			texFile.write(r"Instance & Period ")
			for compModule in Compression.COMPRESSION_MODULES:
				texFile.write(r"& \multicolumn{3}{c|}{"+compModule+r"}")
			texFile.write(r"\\\hline"+"\n")

			texFile.write(r" & ")
			for compModule in Compression.COMPRESSION_MODULES:
				texFile.write("& Total time & Comp. time & \# comp")
			texFile.write(r"\\\hline"+"\n")

			for instance in Compression.INSTANCES_NAMES:
				texFile.write(r"\hline"+"\n")
				for period, factor in sorted(Compression.PERIODS[instance].iteritems()):
					texFile.write(" {} ".format(instance))
					texFile.write("& {}".format(period))

					for compModule in Compression.COMPRESSION_MODULES:
						try:
							data = getMetrics(instance, "path", "metricPlusOne75Capa", compModule, nbRules, factor)
							texFile.write("& {} & {} & {}".format(texNumber(int(data["CPUTime_total"])), 
								int(data["CPUTime_comp"]), texNumber(sum(data["nbCompression"]))))
						except IOError:
							texFile.write("& - & - & -")
					texFile.write(r"\\"+"\n")
					texFile.write(r"\hline"+"\n")				
			texFile.write(r"\end{tabular}"+"\n\n")
			texFile.write(r"}"+"\n") # Close resizebox
		texFile.write(r"\end{document}"+"\n")
