import glob
import itertools
import os
import Compression



def isSolved(instance, routModule, factor, weight, compModule, tabSize):
	resultFilename = "results/{}_{}_{}_{}_{}_{:.6f}.txt".format(instance, routModule, weight, compModule, tabSize, factor)
	return os.path.isfile(resultFilename)

def printSimulations():
	for instance, routModule, weight, compModule, tabSize in itertools.product(Compression.INSTANCES_NAMES, Compression.ROUTING_MODULES, Compression.WEIGHTS, Compression.COMPRESSION_MODULES, Compression.TABLE_SIZES):
		for factor in Compression.PERIODS[instance].itervalues():
			if not isSolved(instance, routModule, factor, weight, compModule, tabSize):
				print "{} -d {} -n {} -m {} -l {}".format(instance, factor, weight, compModule, tabSize)
