#!/usr/bin/env sh
python -c "import instance; instance.printSimulations()" | parallel -j2 --bar --shuf --joblog job.log --colsep ' ' "./EARC {}"
