import matplotlib.pyplot as plt
import sys
import csv
import collections
import re
import os
import numpy as np

import numpy
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import matplotlib

from matplotlib.font_manager import FontProperties

fontP = FontProperties()
fontP.set_size(40)

matplotlib.rc('xtick', labelsize=40)
matplotlib.rc('ytick', labelsize=40)
matplotlib.rcParams['font.serif'] = "Computer Modern Roman"

matplotlib.rcParams['pdf.fonttype'] = 42

matplotlib.rcParams['legend.numpoints'] = 1
matplotlib.rcParams['figure.figsize'] = 20, 8
matplotlib.rcParams['lines.markersize'] = 15
matplotlib.rcParams['font.size'] = 40

def getMetrics(name, heuristic, weight, rules, nbRules, capa):
    try:
        with open("./results/{}/{}_{}_{}_{}_{}.txt".format(name, heuristic, weight, rules, nbRules, capa), 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            # links
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            # nb rules
            filenbRules = int(dataReader.next()[0])
            #
            actualRules = [int(i) for i in dataReader.next()[:-1]]
            #
            compressedRules = [int(i) for i in dataReader.next()[:-1]]

            compressionRatio = []
            for i in range(len(actualRules)):
                if compressedRules[i] != 0:
                    compressionRatio.append(compressedRules[i] / float(actualRules[i]))
                else:
                    compressionRatio.append(0)

            # nbPaths = dataReader.next()[0]
            pathsLength = [int(i) for i in dataReader.next()[:-1]]
            shortestPathsLength = [int(i) for i in dataReader.next()[:-1]]
            pathDiffLength = [ p - sp for (p, sp) in zip(pathsLength, shortestPathsLength)]
            actual_nbDegree = int(dataReader.next()[0])
            actualRulesPerDegree = {}
            for _ in range(actual_nbDegree):
                row = [int(i) for i in dataReader.next()[:-1]]
                actualRulesPerDegree[row[0]] = row[1:]
                
            compressed_nbDegree = int(dataReader.next()[0])
            compressedRulesPerDegree = {}
            for _ in range(compressed_nbDegree):
                row = [int(i) for i in dataReader.next()[:-1]]
                compressedRulesPerDegree[int(row[0])] = row[1:]

            tmp = collections.defaultdict(int)
            for v in actualRules:
                tmp[v] += 1
            return [activeLinks, totalLinks, actualRules, compressedRules, compressionRatio, pathDiffLength, pathsLength, shortestPathsLength, actualRulesPerDegree, compressedRulesPerDegree]
    except:         
        print "./results/{}/{}_{}_{}_{}_{}.txt".format(name, heuristic, weight, rules, nbRules, capa)
        return 0

def plotSimpleFigure(name, heuristic, rules, weight, nbRules, capa):
    figures = []
    
    with open("./results/{}/{}_{}_{}_{}_{}.txt".format(name, heuristic, weight, rules, nbRules, capa), 'rb') as datafile:
        dataReader = csv.reader(datafile, delimiter=' ')
        # links
        links = dataReader.next()
        activeLinks = int(links[0])
        totalLinks = int(links[1])
        # nb rules
        filenbRules = int(dataReader.next()[0])
        #
        actualRules = [int(i) for i in dataReader.next()[:-1]]
        #
        compressedRules = [int(i) for i in dataReader.next()[:-1]]
        
        compressionRatio = []
        for i in range(len(actualRules)):
            if compressedRules[i] != 0:
                compressionRatio.append(actualRules[i] / float(compressedRules[i]))
            else:
                compressionRatio.append(0)

        # nbPaths = dataReader.next()[0]
        pathsDiffLength = [int(i) for i in dataReader.next()[:-1]]

        actual_nbDegree = int(dataReader.next()[0])
        actualRulesPerDegree = {}
        for _ in range(actual_nbDegree):
            row = [int(i) for i in dataReader.next()[:-1]]
            actualRulesPerDegree[row[0]] = row[1:]
            
        compressed_nbDegree = int(dataReader.next()[0])
        compressedRulesPerDegree = {}
        for _ in range(compressed_nbDegree):
            row = [int(i) for i in dataReader.next()[:-1]]
            compressedRulesPerDegree[int(row[0])] = row[1:]

        tmp = collections.defaultdict(int)
        for v in actualRules:
            tmp[v] += 1
        
        
#        //////////////// Plots

        endTitle = "for {}\nwith {} active links ({} & {}& {}) max # rules".format(name, activeLinks, rules, heuristic, weight, nbRules)
        saveName = "{}_{}_{}_{}_{}.pdf".format(rules, heuristic, weight, nbRules, capa)
        plt.clf()
        plt.bar(tmp.keys(), tmp.values(), align='center')
        if(max(tmp.keys()) > 750):
            plt.axvline(750, color="red")
        plt.axvline(np.mean(tmp.keys()))
        plt.xlabel("# rules")
        plt.ylabel("# nodes")
#        plt.title("Uncompressed rules "+endTitle)
        plt.savefig("./plots/"+name+"/"+"unc_rules_"+saveName)
        figures.append("./plots/"+name+"/"+"unc_rules_"+saveName)
        
        plt.clf()
        tmp = collections.defaultdict(int)
        for v in compressedRules:
            tmp[v] += 1
        plt.bar(tmp.keys(), tmp.values(), align='center')
        if(max(tmp.keys()) > 750):
            plt.axvline(750, color="red")
        plt.axvline(np.mean(tmp.keys()))
        plt.xlabel("# rules")
        plt.ylabel("# nodes")
#        plt.title("Compressed rules "+endTitle)
        plt.savefig("./plots/"+name+"/"+"comp_rules_"+saveName)
        figures.append("./plots/"+name+"/"+"comp_rules_"+saveName)


        # plots number of extra hops
        plt.clf()
        tmp = collections.defaultdict(int)
        for v in pathsDiffLength:
            tmp[v] += 1
        plt.bar(tmp.keys(), tmp.values(), align='center')
        plt.xlabel("# hops")
        plt.ylabel("# nodes")
#        plt.title("# extra hops "+endTitle)
        plt.savefig("./plots/"+name+"/"+"hops_"+saveName)
        figures.append("./plots/"+name+"/"+"hops_"+saveName)

        # Compression ratio
#        plt.clf()
#        tmp = collections.defaultdict(int)
#        for v in compressionRatio:
#            tmp[v] += 1
#        plt.bar(tmp.keys(), tmp.values(), align='center')
#        plt.xlabel("Compression ratio")
#        plt.ylabel("# nodes")
#        plt.title("Compression ratio "+endTitle)
#        plt.savefig("./plots/"+name+"/"+"compRatio_"+saveName)
#        figures.append("./plots/"+name+"/"+"compRatio_"+saveName)

        compressionRatioPerDegree = collections.defaultdict(list)
        for deg in actualRulesPerDegree.keys():
            for i in range(len(actualRulesPerDegree[deg])):
                if (actualRulesPerDegree[deg][i]):
                    compressionRatioPerDegree[deg].append( float(compressedRulesPerDegree[deg][i]) / actualRulesPerDegree[deg][i] )
                else:
                    compressionRatioPerDegree[deg].append(0)
        plt.clf()
        plt.boxplot(compressionRatioPerDegree.values(), positions=compressionRatioPerDegree.keys())
        plt.xlabel("Degree")
        plt.ylabel("Compression ratio")
#        plt.title("Compression ration "+endTitle)
        plt.savefig("./plots/"+name+"/"+"compRatio_deg_"+saveName)
        figures.append("./plots/"+name+"/"+"compRatio_deg_"+saveName)

        plt.clf()
        plt.boxplot(actualRulesPerDegree.values(), positions=actualRulesPerDegree.keys())
        plt.xlabel("Degree")
        plt.ylabel("# rules")
#        plt.title("Distribution # rules "+endTitle)
        plt.savefig("./plots/"+name+"/"+"unc_deg_"+saveName)
        figures.append("./plots/"+name+"/"+"unc_deg_"+saveName)

        plt.clf()
        plt.boxplot(compressedRulesPerDegree.values(), positions=compressedRulesPerDegree.keys())
        plt.xlabel("Degree")
        plt.ylabel("# rules")
#        plt.title("Distribution # rules (compressed) "+endTitle)
        plt.savefig("./plots/"+name+"/"+"comp_deg_"+saveName)
        figures.append("./plots/"+name+"/"+"comp_deg_"+saveName)

    return figures

def plotAllSimpleFigures():
    for (dirpath, dirnames, filenames) in os.walk("."):
        name = ""
        dirpath_tab = dirpath.split("/")
        if len(dirpath_tab) > 2 and dirpath_tab[1] == "results":
            networkFigure = []
            name = dirpath_tab[2]
            if name != "fattree":
                continue
            print name
            for filename in filenames:
                if filename[-4:] == ".txt":
                    filename_tab = filename[:-4].split("_")
                    print filename_tab
                    heuristic = filename_tab[0]
                    weight = filename_tab[1]
                    rules = filename_tab[2]
                    nbRules = filename_tab[3]
                    capa = filename_tab[4]
                    networkFigure.append(plotSimpleFigure(name, heuristic, rules, weight, nbRules, capa))

def plotEnergySavingsFigure(name, heuristics, nbRules):
    ruless = ["WildcardRules"]
    weights = ["dumb", "metricPlusOne", "metricPlusOneCapa", "metricPlusOneRules", "metricPlusOne75Capa", "metricPlusOne75Rules"]
    capas = ["1.000000"]
    
    print heuristics
    
    i = 0
    x = []
    label = []
    nbActiveLinks = []
    for rules in ruless:
        for heuristic in heuristics:
            for weight in weights:
                for capa in capas:
                    try:
                        with open("./results/"+name+"/"+heuristic+"_"+weight+"_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
                            dataReader = csv.reader(datafile, delimiter=' ')
                            
                            links = dataReader.next()
                            activeLinks = int(links[0])
                            totalLinks = int(links[1])
                            nbActiveLinks.append((totalLinks-activeLinks) / float(totalLinks) * 100)
                            x.append(i)
                            label.append("{} {} {} {}".format(rules, heuristic, weight, capa))
                            i+=1
                    except:
                        pass
                i +=1
            i+=1
        i+=1
    print nbActiveLinks
    if nbActiveLinks:
        plt.clf()
        plt.barh(x, nbActiveLinks, align='center')
        plt.title("Energy savings for " + name)
        plt.yticks(x, label)
        plt.subplots_adjust(left=0.46)
        plt.grid(True)
        plt.show()

def plotDistibutionActualFigure(heuristics, nbRules):
    names =["atlanta", "germany50", "ta2", "zib54", "fattree"]
    linksName = {"atlanta":46, "germany50":178, "ta2":218, "zib54":168, "fattree":642}
    ruless = ["DefaultPortRules", "WildcardRules"]
#    heuristics = ["fullRestart", "pathRestart", "forceAll"]
    weights = ["dumb", "metricPlusOne", "metric", "khoa"]
    capas = ["1.000000" "1.500000" "2.000000" "2.500000" "3.000000"]
    
    for name in names:
        if name == "fattree":
            continue
        i = 0
        x = []
        label = []
        actualRules = []
        for rules in ruless:
            for heuristic in heuristics:
                for capa in capas:
                    for weight in weights:
                        try:
                            with open("./results/"+name+"/"+heuristic+"_"+weight+"_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
                                dataReader = csv.reader(datafile, delimiter=' ')
                                dataReader.next()
                                dataReader.next()
                                actualRules.append([int(r) for r in dataReader.next()[:-1]])
                                x.append(i)
                                label.append("{} {} {} {}".format(rules, heuristic, weight, capa))
                                i+=1
                        except:
                            pass
                        i += 1
                    i+= 1
            i+= 1
#        print nbActiveLinks
        if actualRules:
            plt.clf()
            plt.boxplot(actualRules, positions=x, vert=False)
            if(max([max(m) for m in actualRules]) > 750):
                plt.axvline(750)
            plt.title("Distribution of non compressed rules for " + name)
            plt.yticks(x, label)
            plt.subplots_adjust(left=0.46)
            plt.show()

def plotDistibutionCompFigure(heuristics):
    names =["atlanta", "germany50", "ta2", "zib54", "fattree"]
    linksName = {"atlanta":46, "germany50":178, "ta2":218, "zib54":168, "fattree":642}
    ruless = ["DefaultPortRules", "WildcardRules"]
    weights = ["dumb", "metricPlusOne", "metric", "khoa"]
    capas = ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000" ]
    
    for name in names:
        if name == "fattree":
            continue
        for rules in ruless:
            i = 0
            x = []
            label = []
            compRules = []
            for heuristic in heuristics:
                for weight in weights:
                    for capa in capas:
                        try:
                            with open("./results/"+name+"/"+heuristic+"_"+weight+"_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
                                dataReader = csv.reader(datafile, delimiter=' ')
                                dataReader.next()
                                dataReader.next()
                                dataReader.next()
                                compRules.append([int(r) for r in dataReader.next()[:-1]])
                                x.append(i)
                                label.append("{} {} {} {}".format(rules, heuristic, weight, capa))
                                i+=1
                        except:
                            pass
                        i += 1
                i+= 1
            i+= 1
#        print nbActiveLinks
            if compRules:
                plt.clf()
                plt.boxplot(compRules, positions=x, vert=False)
                print max([max(m) for m in compRules])
                if(max([max(m) for m in compRules]) >= 750):
                    plt.axvline(750)
                plt.title("Distribution of compressed rules for " + name + " " + rules)
                plt.yticks(x, label)
                plt.subplots_adjust(left=0.46)
                plt.show()

def plotDistibutionCompFigure(heuristics, nbRules):
    names =["atlanta", "germany50", "ta2", "zib54", "fattree"]
    linksName = {"atlanta":46, "germany50":178, "ta2":218, "zib54":168, "fattree":642}
    ruless = ["DefaultPortRules", "WildcardRules"]
    weights = ["dumb", "metricPlusOne", "metric", "khoa"]
    capas = ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000" ]
    
    for name in names:
        if name == "fattree":
            continue
        i = 0
        x = []
        label = []
        compRatios = []
        for rules in ruless:
            for capa in capas:
                for heuristic in heuristics:
                    for weight in weights:
                        try:
                            with open("./results/"+name+"/"+heuristic+"_"+weight+"_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
                                dataReader = csv.reader(datafile, delimiter=' ')
                                dataReader.next()
                                dataReader.next()
                                actualRules = [int(r) for r in dataReader.next()[:-1]]
                                compRules = [int(r) for r in dataReader.next()[:-1]]
                                compRatio = []
                                for j in xrange(len(actualRules)):
                                    compRatio.append(100 * compRules[j] // actualRules[j])
                                compRatios.append(compRatio)
                                x.append(i)
                                label.append("{} {} {} {}".format(rules, heuristic, weight, capa))
                                i+=1
                        except:
                            pass
                        i += 1
                    i+= 1
            i+= 1
#        print compRatios
        if compRatios:
            plt.clf()
            plt.boxplot(compRatios, positions=x, vert=False)
#                print max([max(m) for m in actualRules])
#                if(max([max(m) for m in actualRules]) >= 750):
#                    plt.axvline(750)
            plt.title("Distribution of compressed rules for " + name)
            plt.yticks(x, label)
            plt.subplots_adjust(left=0.46)
            plt.show()


def showPercentageEnergySavings(nbRules):
    names =["atlanta", "germany50", "ta2", "zib54", "fattree"]
    ruless = ["DefaultPortRules", "WildcardRules"]
#    weights = ["dumb", "metricPlusOne", "metric", "khoa"]
    capas = ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000" ]
    
    for name in names:
        if name == "fattree":
            continue
        print name,
        for rules in ruless:
            for capa in capas:
                print rules,
                try:
                    with open("./results/"+name+"/forceAll_metric_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
                        dataReader = csv.reader(datafile, delimiter=' ')
                        links = dataReader.next()
                        activeLinks = int(links[0])
                        totalLinks = int(links[1])
                        print 100 * (totalLinks - activeLinks) / float(totalLinks),
                except:
                    pass
        print

def plotEnergySavingsAsFuncCharge(name, heuristic, rules, nbRules):
    capas = ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000" ]
    dictCapa = {}
    for capa in capas:
        with open("./results/"+name+"/"+heuristic+"_metricPlusOne_"+rules+"_"+nbRules+"_"+capa+".txt", 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            dictCapa[float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
    plt.clf()
    plt.bar(dictCapa.keys(), dictCapa.values(), width=.3, align="center")
    plt.show()

def showMiniRules(rules):
#    traffMat = ["D1", "D2", "D3", "D4", "D5"]
#    x = [1, 1.5, 2, 2.5, 3]
    for name in ["zib54"]:
        print name, ":",
        miniRules = []
        for capa in ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000"]:
            try:
                with open("./results/{}/forceAll_metricPlusOne_{}_mini_{}.txt".format(name, rules, capa), 'rb') as datafile:
                    dataReader = csv.reader(datafile, delimiter=' ')
                    links = dataReader.next()
                    activeLinks = int(links[0])
                    totalLinks = int(links[1])
                    miniRules.append(int(dataReader.next()[0]))
            except:
                pass
        if not miniRules:
            print
            continue
        print max(miniRules), miniRules

def plotEnergySavingMiniRules(name, nbRules, rules):
    traffMat = ["D1", "D2", "D3", "D4", "D5"]
    x = [1+.15, 1.5+.15, 2+.15, 2.5+.15, 3+.15]

    miniRules = collections.defaultdict(dict)
    
    for capa in ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000"]:
#        try:
            with open("./results/{}/forceAll_metricPlusOne_{}_{}_{}.txt".format(name, rules, nbRules, capa), 'rb') as datafile:
                dataReader = csv.reader(datafile, delimiter=' ')
                links = dataReader.next()
                activeLinks = int(links[0])
                totalLinks = int(links[1])
                miniRules["mini"][float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
            with open("./results/{}/forceAll_metricPlusOne_{}_{}_{}.txt".format(name, rules, 750, capa), 'rb') as datafile:
                dataReader = csv.reader(datafile, delimiter=' ')
                links = dataReader.next()
                activeLinks = int(links[0])
                totalLinks = int(links[1])
                miniRules[750][float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
            with open("./results/{}/forceAll_metricPlusOne_{}_{}_{}.txt".format(name, rules, 1000000, capa), 'rb') as datafile:
                dataReader = csv.reader(datafile, delimiter=' ')
                links = dataReader.next()
                activeLinks = int(links[0])
                totalLinks = int(links[1])
                miniRules[1000000][float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
#        except:
#            pass

    if not miniRules:
        return
    plt.clf()
    plt.bar(miniRules["mini"].keys(), miniRules["mini"].values(), width=.10, color="red", label="Minimum rule space")
    plt.bar([v + .1 for v in miniRules[750].keys()], miniRules[750].values(), width=.10, label="Default rule space")
    plt.bar([v + .2 for v in miniRules[1000000].keys()], miniRules[1000000].values(), width=.1, label="Unlimited rule space", color="green")
    plt.xticks(x, traffMat)
    plt.ylabel("Energy saving (%)")
    plt.legend(loc="best")
#    plt.title(name)
    plt.show()
heuristicsEAR = ["path", "forceAll"]
heuristicsOnce = ["once", "forceOnce"]

def plotCompareDPLC(name):
    traffMat = ["D1", "D2", "D3", "D4", "D5"]
    x = [1+.15, 1.5+.15, 2+.15, 2.5+.15, 3+.15]
    nbRules = 1000000
    rules = collections.defaultdict(dict)
    
    for capa in ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000"]:
        with open("./results/{}/forceAll_metricPlusOne_{}_{}_{}.txt".format(name, "WildcardRules", nbRules, capa), 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            rules["WC"][float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
        with open("./results/{}/forceAll_metricPlusOne_{}_{}_{}.txt".format(name, "DefaultPortRules", nbRules, capa), 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            rules["DP"][float(capa)] = 100 * (totalLinks - activeLinks) / float(totalLinks)
    if not rules:
        return
    plt.clf()
    plt.bar(rules["WC"].keys(), rules["WC"].values(), width=.15, color="red", label="LC")
    plt.bar([v + .15 for v in rules["DP"].keys()], rules["DP"].values(), width=.15, label="DP")
    plt.xticks(x, traffMat)
    plt.ylabel("Energy saving (%)")
    plt.legend(loc="best")
#    plt.title(name)
    plt.show()

def plotRatioCompression(rules, heuristic, nbRules, capa):
    names =["germany50", "ta2", "zib54"]
    compressions = []
    for name in names:
        with open("./results/{}/{}_metricPlusOne_{}_{}_{}.txt".format(name, heuristic, rules, nbRules, capa), 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            # links
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            # nb rules
            filenbRules = int(dataReader.next()[0])
            #
            actualRules = [int(i) for i in dataReader.next()[:-1]]
            #
            compressedRules = [int(i) for i in dataReader.next()[:-1]]
            
            compressionRatio = []
            for i in range(len(actualRules)):
                if compressedRules[i] != 0:
                    compressionRatio.append(compressedRules[i] / float(actualRules[i]))
                else:
                    compressionRatio.append(0)
            compressions.append(compressionRatio)
            
    plt.clf()
    plt.boxplot(compressions, sym='b+')
    plt.xticks(range(1, 5), names)
#    plt.yticks([0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5], ["0%", "5%", "10%", "15%", "20%", "25%", "30%", "35%", "40%", "45%", "50%"])
#    plt.yticks([0, 0.005, 0.01, 0.015, 0.02, 0.025], ["0%", "0.5%", "1.0%", "1.5%", "2.0%", "2.5%"])

    plt.ylabel("Compression ratio")
    plt.show()

def plotRatioCompressionName(rules, heuristic, nbRules, name):
    capas = ["1.000000", "1.500000", "2.000000", "2.500000", "3.000000"]
    capasInt = [1, 1.5, 2, 2.5, 3]
    compressions = []
    for capa in capas:
        with open("./results/{}/{}_metricPlusOneCapa_{}_{}_{}.txt".format(name, heuristic, rules, nbRules, capa), 'rb') as datafile:
            dataReader = csv.reader(datafile, delimiter=' ')
            # links
            links = dataReader.next()
            activeLinks = int(links[0])
            totalLinks = int(links[1])
            # nb rules
            filenbRules = int(dataReader.next()[0])
            #
            actualRules = [int(i) for i in dataReader.next()[:-1]]
            #
            compressedRules = [int(i) for i in dataReader.next()[:-1]]
            
            compressionRatio = []
            for i in range(len(actualRules)):
                if compressedRules[i] != 0:
                    compressionRatio.append(compressedRules[i] / float(actualRules[i]))
                else:
                    compressionRatio.append(0)
            compressions.append(compressionRatio)
            
    plt.clf()
    plt.boxplot(compressions, positions=capasInt, sym='b+')
    plt.xticks(capasInt, ["D1", "D2", "D3", "D4", "D5"])

    plt.ylabel("Compression ratio")
    plt.show()

# Plot the energy savings comparaison for the datas given
def plotEnergyComparaison(datas, labels, colors, markers):
    print datas, labels
    x = [0, 2, 3, 7, 8, 9, 11, 18, 23, 24]
    val = ["2.000000", "1.500000", "1.000000", "1.500000", "2.000000", "2.500000", "3.000000", "2.500000", "2.000000", "2.000000"]
    values = []
    for name, weight, rules, heuristic, nbRules in datas:
        savings = []
        for h, capa in zip(x, val):
            metrics = getMetrics(name, heuristic, weight, rules, nbRules, capa)
            if metrics:
                savings.append((1 - (metrics[0] / float(metrics[1]))) * 100)
                # savings.append(metrics[1] - metrics[0])
            else:
                savings.append(0)
        values.append(savings)

    abs = []
    ords = []
    for _ in xrange(len(values)):
        ords.append([])

    for i in xrange(len(x) - 1):
        abs.append(x[i])
        for ord, saving in zip(ords, values):
            ord.append(saving[i]) 
        abs.append(x[i+1])
        for ord, saving in zip(ords, values):
            ord.append(saving[i]) 

    plt.clf()
    for ord, label, color, marker in zip(ords, labels, colors, markers):
        plt.plot(abs, ord, "b-", marker=marker, markerfacecolor='none', markeredgecolor=color, label=label, c=color)
    plt.legend(loc="upper right").get_frame().set_alpha(0.5)
    plt.xlabel("Hours")
    plt.ylabel("Energy savings (%)")
    plt.xlim(0, 24)
    # plt.ylim(0, 70)
    plt.subplots_adjust(left=0.11, bottom=0.16, right=0.96, top=0.93,
                        wspace=0.33, hspace=0.2)
    plt.savefig("./plots/{}/energy_saving_{}_{}.pdf".format(name, weight, nbRules))
    plt.show()

# Plot the energy savings for the compression and no compression
def plotEnergyComparaisonCompression(name, weight, nbRules):
    plotEnergyComparaison([[name, weight, "WildcardRules", "path", nbRules], \
                        [name, weight, "DefaultPortRules",  "path", nbRules], \
                        [name, weight, "WildcardRules",  "noCompress", nbRules]], \
                        ["WC", "DP", "NC"], 
                        ['red', 'blue', 'green'],
                        ['.', '+', 'x'])

# plotEnergyComparaison("ta2", "path", "750")
# plotEnergyComparaison("zib54", "path", "750")
# plotEnergyComparaison([["zib54", weight, "WildcardRules", "path", "750"] for weight in \
#     ["metricPlusOne", "metricPlusOne75Rules", "metricPlusOne75Capa", "metricPlusOneRules", "metricPlusOne75Rules"]],\
#     ["metricPlusOne", "metricPlusOne75Rules", "metricPlusOne75Capa", "metricPlusOneRules", "metricPlusOne75Rules"], ['r', 'b', 'g', 'c', 'm'])
# plotEnergyComparaisonCompression("zib54", "metricPlusOne", "750")
# plotEnergyComparaison([["germany50", weight, "WildcardRules", "path", "750"] for weight in \
#     ["metricPlusOne", "metricPlusOne75Rules", "metricPlusOne75Capa", "metricPlusOneRules", "metricPlusOne75Rules"]],\
#     ["metricPlusOne", "metricPlusOne75Rules", "metricPlusOne75Capa", "metricPlusOneRules", "metricPlusOne75Rules"], ['r', 'b', 'g', 'c', 'm'])
# plotEnergyComparaisonCompression("germany50", "metricPlusOne", "750")

# plotEnergyComparaisonCompression("zib54", "metricPlusOneRules", "750")
# plotEnergyComparaisonCompression("ta2", "metricPlusOneRules", "750")
# plotEnergyComparaisonCompression("germany50", "metricPlusOneRules", "750")

# plotEnergyComparaisonCompression("zib54", "metricPlusOne", "800")
# plotEnergyComparaisonCompression("ta2", "metricPlusOne", "800")
# plotEnergyComparaisonCompression("germany50", "metricPlusOne", "800")

# for weight in ["metricPlusOne", "metricPlusOne75Rules", "metricPlusOne75Capa", "metricPlusOneRules", "metricPlusOne75Rules"]:
for weight in ["metricPlusOne"]:
    plotEnergyComparaisonCompression("zib54", weight, "750")
    plotEnergyComparaisonCompression("ta2", weight, "750")
    plotEnergyComparaisonCompression("germany50", weight, "750")

