INSTANCES_NAMES = ["atlanta", "germany50", "zib54", "ta2"]

PERIODS = {
	"atlanta":{"D1":1},
	"germany50":{"D1":1, "D2":1.5, "D3":2, "D4":2.5, "D5":3},
	"zib54":{"D1":1, "D2":1.5, "D3":2, "D4":2.5, "D5":3},
	"ta2":{"D1":1, "D2":1.5, "D3":2, "D4":2.5, "D5":3}
}

COMPRESSION_MODULES = ["WildcardRules", "DefaultPortRules", "HeurSB"]#, "CplexRules"
ROUTING_MODULES = ["path"]
TABLE_SIZES = [750]#[100, 750, 1000, 2000]
WEIGHTS = ["metricPlusOne75Capa"]#["dumb", "metric", "metricPlusOne", "metricPlusOneCapa", "metricPlusOneRules", "metricPlusOne75Rules", "metricPlusOne75Capa"]