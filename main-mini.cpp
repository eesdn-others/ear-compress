
template <typename Rules>
void searchMini(const DiGraph& topology, std::size_t _nbServers, std::size_t _nbSwitches,
    const std::vector<Demand>& demands, const std::string& name,
    const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    const auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);

    int minRules = 0;
    int maxRules = std::numeric_limits<int>::max();

    std::cout << "Name : " << name << '\n'
              << "Heurisitc : " << heuristicName << '\n'
              << "Rules : " << _rulesName << '\n'
              << "Weight : " << weightFunctionName << '\n';

    int nbRulesTest = std::numeric_limits<int>::max() / 2;
    std::cout << "Test with " << nbRulesTest << " max mem =>" << std::flush;

    Routing<Rules> solution =
        run(nbRulesTest, topology, _nbServers, _nbSwitches, demands);

    if (solution.getNbDemands() != (int)demands.size()) {
        minRules = nbRulesTest;
        std::cout << " failed. " << solution.getNbDemands() << '/' << demands.size()
                  << '\n';
    } else {
        std::cout << " success.\n";
        maxRules = nbRulesTest;
    }

    while (minRules + 1 < maxRules) {
        nbRulesTest = (maxRules - minRules) / 2 + minRules;
        std::cout << "Test with " << nbRulesTest << " max mem =>" << std::flush;
        auto ear = run(nbRulesTest, topology, _nbServers, _nbSwitches, demands);
        if ((std::size_t)ear.getNbDemands() != demands.size()) {
            minRules = nbRulesTest;
            std::cout << " failed. " << ear.getNbDemands() << '/' << demands.size()
                      << '\n';
        } else {
            std::cout << " success.\n";
            maxRules = nbRulesTest;
            solution = std::move(ear);
        }
    }

    if (solution.getNbDemands() != demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << '\n';
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << " " << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem() << '\n';
    }

#ifdef DYNAMIC
    std::string resultsFolder("results_dynamic");
#else
    std::string resultsFolder("results");
#endif

    for (std::size_t i = 0; i < solution.getRules().size(); ++i) {
        auto& rule = solution.getRules()[i];
#ifdef DEBUG
        if (!rule.isCompressionValid()) {
            std::cout << "Compression not valid for " << i << '\n';
            for (auto& neighbor : solution.getResidual().getNeighbors(i)) {
                std::cout << neighbor << ", ";
            }
            std::cout << '\n';
        }
#endif
        std::cout << i << ": " << rule.getNbCompressedRules() << "/"
                  << rule.getActualNbRules() << '\n';
    }

    solution.saveMetrics("./" + resultsFolder + "/" + name + "_" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_mini_" + std::to_string(percent) + ".txt",
        demands.size());
}