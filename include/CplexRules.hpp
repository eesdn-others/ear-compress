#ifndef CPLEXRULES_H
#define CPLEXRULES_H

#include "CompressedRules.hpp"

class CplexRules : public CompressedRules {
  private:
  public:
    CplexRules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    ~CplexRules() override = default;

    CplexRules(const CplexRules&) = default;
    CplexRules& operator=(const CplexRules&) = default;

    CplexRules(CplexRules&&) = default;
    CplexRules& operator=(CplexRules&&) = default;

    /**
    * Add the rule (_s, _t, _lastHop, _nextHop) to the forwarding table
    */
    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    /**
    * Reduce the size of the forwarding table using the ILP formulation presented in [?]
    */
    void compress() override;

    /**
    * Return the output port for the flow (s, t)
    * Return -1 if no output port is found
    */
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
};
#endif