#ifndef DEFAULTPORTRULES_H
#define DEFAULTPORTRULES_H

#include "CompressedRules.hpp"
#include "EARC.hpp"

class DefaultPortRules : public CompressedRules {
  protected:
    std::size_t m_defaultPort = Rule::Wildstar;

  public:
    DefaultPortRules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    ~DefaultPortRules() override = default;

    DefaultPortRules(const DefaultPortRules&) = default;
    DefaultPortRules& operator=(const DefaultPortRules&) = default;

    DefaultPortRules(DefaultPortRules&&) = default;
    DefaultPortRules& operator=(DefaultPortRules&&) = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    void compress() override;
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
};
#endif