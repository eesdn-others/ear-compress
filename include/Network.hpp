#ifndef NETWORK_H
#define NETWORK_H

#include "DiGraph.hpp"

#include <jni.h>
#include <map>
#include <utility>
#include <vector>

class Network : public DiGraph {

  public:
    explicit Network(const std::vector<jlong>&);
    virtual ~Network() = default;
    void addLink(int64_t _srcNodeID, int64_t _dstNodeID, short _srcPort, short _dstPort,
        double _capacity);
    void deleteLink(int64_t _u, int64_t _v);

    inline const std::pair<jint, jint>& getPort(const int _u, const int _v) const {
        return m_ports[_u * this->getOrder() + _v];
    }

    inline int64_t nodeToId(const int _u) const {
        return m_nodesToId[_u];
    }

    inline int idToNode(const int64_t _id) {
        return m_idToNodes[_id];
    }

  private:
    std::vector<jlong> m_nodesToId;
    std::map<jlong, jint> m_idToNodes;
    std::vector<std::pair<jint, jint>> m_ports;
};

#endif