#ifndef EARC_H
#define EARC_H

#include "DiGraph.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

struct Demand {
    Demand(const Graph::Node _s, const Graph::Node _t, const double _d)
        : s(_s)
        , t(_t)
        , d(_d) {}
    Demand(const Demand&) = default;
    Demand& operator=(const Demand&) = default;
    Demand(Demand&&) = default;
    Demand& operator=(Demand&&) = default;
    ~Demand() = default;

    Graph::Node s; /// Source of the demand
    Graph::Node t; /// Destination of the demand
    double d; /// Charge of the demand
};

struct Instance {
    DiGraph topology;
    std::size_t nbServers;
    std::size_t nbSwitches;
    std::size_t maxMem;
    std::vector<Demand> demands;
};

inline bool operator==(const Demand& _d1, const Demand& _d2) {
    return _d1.s == _d2.s
           && _d1.t == _d2.t
           && _d1.d == _d2.d;
}

std::ostream& operator<<(std::ostream& _out, const Demand& _demand);

inline std::vector<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    }
    Graph::Node s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        std::stringstream lineStream(line);
        if (!line.empty()) {
            lineStream >> s >> t >> d;
            demands.emplace_back(s, t, d * percent);
        }
    }
    return demands;
}

struct Rule {
    constexpr static std::size_t Wildstar = std::numeric_limits<std::size_t>::max();
    bool isValid() const {
        return s != Wildstar
               && t != Wildstar
               && nh != Wildstar
               && lh != Wildstar;
    }

    Rule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop)
        : s(_s)
        , t(_t)
        , lh(_lastHop)
        , nh(_nextHop) {}

    std::size_t s;
    std::size_t t;
    std::size_t lh;
    std::size_t nh;
};

inline std::ostream& operator<<(std::ostream& _out, const Rule& _rule) {
    _out << '(';
    if (_rule.s == Rule::Wildstar) {
        _out << "*, ";
    } else {
        _out << _rule.s << ", ";
    }
    if (_rule.t == Rule::Wildstar) {
        _out << "*) -> ";
    } else {
        _out << _rule.t << ") -> ";
    }
    return _out << _rule.nh;
}

#endif
