#ifndef COMPRESSEDRULES_H
#define COMPRESSEDRULES_H

#include "Rules.hpp"

#include <list>
#include <tuple>

class CompressedRules : public Rules {
  protected:
    std::list<Rule> m_compressedRules{};
    bool m_compressed = false;
#ifdef CHRONO
    std::list<std::tuple<std::size_t, double, std::size_t>> m_compressTimes;
#endif
  public:
    CompressedRules(std::size_t _order, std::size_t _maxSize, std::size_t _outDegree);
    ~CompressedRules() override = default;
    CompressedRules(const CompressedRules&) = default;
    CompressedRules& operator=(const CompressedRules&) = default;

    CompressedRules(CompressedRules&&) = default;
    CompressedRules& operator=(CompressedRules&&) = default;

    std::size_t getNbRules() const;
    bool isCompressed() const;
    bool isCompressable() const;

#ifdef CHRONO
    const std::list<std::tuple<std::size_t, double, std::size_t>>& getCompressTimes() const {
        return m_compressTimes;
    }
#endif
    virtual bool isFull() const {
        return getNbRules() >= m_maxSize;
    }

    bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    virtual bool canAddRule(std::size_t _s, std::size_t _t, std::size_t _p) const;

    std::size_t findCompressedPortInList(std::size_t _s, std::size_t _t) const;
    virtual bool isCompressionValid() const;
    virtual void compress() = 0;
    virtual std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const = 0;

    /** 
  * Return the first non wildcard rule in the list of compressed rules
  */
    Rule topNonStar(std::size_t _s) const;

    virtual std::size_t getActualNbRules() const;
    const std::list<Rule>& getCompressedRules() const;
    std::size_t getNbCompressedRules() const;
    virtual std::list<Rule> getRules() const;

    friend std::ostream& operator<<(std::ostream& _out,
        const CompressedRules& _rules);
};

#endif