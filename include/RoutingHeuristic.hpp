#ifndef ROUTINGHEURISTIC_HPP
#define ROUTINGHEURISTIC_HPP

#include "EarRouting.hpp"

template <typename Rules, typename Weight>
EarRouting<Rules> runPathRestart(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);
    if (!ear.template routeAllDemands<Weight>()) {
        return ear;
    }

    EarRouting<Rules> solution = ear;
    std::set<Graph::Edge> nonRemovableEdge;
    while (true) {
        const auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }

        for (const auto& edge : edges) {
            // const auto restorationPoint = ear.saveState();
            const auto removedDemands = ear.deleteEdge(edge);
            // std::cout << "Try to remove " << edge << '\n';
            if (!ear.template routeAllDemands<Weight>()) {
                nonRemovableEdge.insert(edge);

                // std::cout << "Failed " << edge << '\n';
                // Rollback to previous valid routing
                const std::vector<std::size_t> nbComp = ear.getNbCompression();
                const std::pair<double, double> compTime = ear.getCompressionTimes();
                ear = solution;
                ear.setNbCompression(nbComp);
                ear.setCompressionTimes(compTime);
                break;
            } else {
                solution = ear;
            }
        }
    }
    return ear;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runAtTheEnd(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);

    std::set<Graph::Edge> nonRemovableEdge;

    if (!ear.template routeAllDemands<Weight>()) {
        return ear;
    }

    EarRouting<Rules> solution(ear);

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }

        for (const auto& edge : edges) {
            const auto demandsToRemove = ear.deleteEdge(edge);
            if (ear.template routeAllDemands<Weight>()) {
                solution = ear;
            } else {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    solution.compressAll();
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runPathLink(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);

    std::set<Graph::Edge> nonRemovableLinks;

    if (!ear.template routeAllDemands<Weight>()) {
        return ear;
    }

    EarRouting<Rules> solution{ear};

    while (true) {
        auto links = ear.getRemovableLinks(nonRemovableLinks);
        if (links.empty()) {
            break;
        }

        for (const auto& link : links) {
            auto demandsToRemove = ear.deleteLink(link);
            if (ear.template routeAllDemands<Weight>(demandsToRemove)) {
                solution = ear;
            } else {
                nonRemovableLinks.insert(link);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runNoCompress(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);

    std::set<Graph::Edge> nonRemovableEdge;

    if (!ear.template routeAllDemandsNoCompress<Weight>()) {
        return ear;
    }

    EarRouting<Rules> solution = ear;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }
        for (const auto& edge : edges) {
            auto demandsToRemove = ear.deleteEdge(edge);
            if (ear.template routeAllDemandsNoCompress<Weight>()) {
                solution = ear;
            } else {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runFullRestart(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);

    std::set<Graph::Edge> nonRemovableEdge;

    if (!ear.template routeAllDemands<Weight>()) {
        return ear;
    }

    auto solution = ear;
    EarRouting<Rules> base(_inst);
    std::list<std::pair<int, int>> toRemove;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }
        for (const auto& edge : edges) {
            ear = base;
            ear.deleteEdge(edges.front());
            if (ear.template routeAllDemands<Weight>()) {
                base.deleteEdge(edge);
                std::swap(solution, ear);
                break;
            } else {
                nonRemovableEdge.insert(edges.front());
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runForceDemands(const Instance& _inst) {
    EarRouting<Rules> ear(_inst);

    if (!ear.template forceRouteAllDemands<Weight>()) {
        return ear;
    }
    if (!ear.template cleanFullRules<Weight>()) {
        return ear;
    }
    EarRouting<Rules> solution = ear;
    std::set<Graph::Edge> nonRemovableEdge;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }

        bool good;
        for (const auto& edge : edges) {
            auto removedDemands = ear.deleteEdge(edge);
            good = false;
            if (ear.template forceRouteAllDemands<Weight>()) {
                if (ear.template cleanFullRules<Weight>()) {
                    solution = ear;
                    good = true;
                }
            }
            if (!good) {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

#endif