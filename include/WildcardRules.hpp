#ifndef WILDCARDRULES_H
#define WILDCARDRULES_H

#include <vector>

#include "DefaultPortRules.hpp"
#include "EARC.hpp"

class WildcardRules : public DefaultPortRules {
  public:
    enum CompressionType { NONE,
        LINE,
        COLUMN,
        DEFAULT };

    WildcardRules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    ~WildcardRules() override = default;
    WildcardRules(const WildcardRules&) = default;
    WildcardRules(WildcardRules&&) = default;
    WildcardRules& operator=(const WildcardRules&) = default;
    WildcardRules& operator=(WildcardRules&&) = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    void save();

    void compress() override;
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
    /**
    * \brief Find the rule for the source _s and the destination _t
    */
    Rule findRule(std::size_t _s, std::size_t _t) const;
    Rule findCompressedRule(std::size_t _s, std::size_t _t) const;

  private:
    std::vector<std::size_t> m_defaultPortRow;
    std::vector<std::size_t> m_defaultPortCol;
    std::vector<std::size_t> m_nbValueRow;
    std::vector<std::size_t> m_nbValueCol;
    CompressionType m_compressionType;
    bool m_hasChangedSinceLastComp = true;
};

#endif