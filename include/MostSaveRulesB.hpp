#ifndef MOSTSAVERULESB_H
#define MOSTSAVERULESB_H

#include "CompressedRules.hpp"

class MostSaveRulesB : public CompressedRules {
  private:
  public:
    MostSaveRulesB(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    ~MostSaveRulesB() override = default;

    MostSaveRulesB(const MostSaveRulesB&) = default;
    MostSaveRulesB& operator=(const MostSaveRulesB&) = default;

    MostSaveRulesB(MostSaveRulesB&&) = default;
    MostSaveRulesB& operator=(MostSaveRulesB&&) = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    // bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    void compress() override;
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
};
#endif