#ifndef BIPARRULES_H
#define BIPARRULES_H

#include <vector>

#include "CompressedRules.hpp"

class BiparRules : public CompressedRules {
  public:
    enum CompressionType { NONE,
        LINE,
        COLUMN,
        DEFAULT };

    BiparRules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);

    BiparRules(const BiparRules&) = default;
    BiparRules& operator=(const BiparRules&) = default;
    BiparRules(BiparRules&&) = default;
    BiparRules& operator=(BiparRules&&) = default;
    ~BiparRules() override = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    void compress() override;
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
    Rule findRule(std::size_t _s, std::size_t _t) const;
    Rule findCompressedRule(std::size_t _s, std::size_t _t) const;
};

#endif