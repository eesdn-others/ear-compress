#ifndef ROUTING_HPP
#define ROUTING_HPP

#include <valarray>
#ifdef CHRONO
#include <chrono>
#endif
#include "MyTimer.hpp"
#include <fstream>
#include <functional>
#include <iostream>
#include <set>
#include "DiGraph.hpp"
#include "EARC.hpp"
#include "Matrix.hpp"
#include "ShortestPath.hpp"
#include "json.hpp"

template <typename Rules>
class Routing {
  public:
    Routing(Instance _inst)
        : m_inst(std::move(_inst))
        , m_rules(m_inst.nbSwitches,
            Rules(m_inst.nbServers, m_inst.maxMem, m_inst.topology.getOrder()))
        , m_paths(m_inst.demands.size())
        , m_residual(m_inst.topology)
        , m_shopat(m_residual)
        , m_canCompress(m_inst.topology.getOrder(), true)
        , m_nbCompression(m_inst.topology.getOrder(), 0) {
    }

    Routing(const Routing& _r) = default;
    Routing& operator=(const Routing& _r) = default;
    Routing(Routing&& _r) noexcept = default;
    Routing& operator=(Routing&& _r) noexcept = default;
    virtual ~Routing() = default;

    std::size_t getNbForwardingDevices() const {
        return m_inst.nbSwitches;
    }

    std::size_t getTotalNbRules() const {
        std::size_t nbRules = 0;
        for (const auto& rules : m_rules) {
            nbRules += rules.getNbRules();
        }
        return nbRules;
    }

    const Rules& getRules(const Graph::Node _node) const {
        return m_rules[_node - (m_inst.topology.getOrder() - m_inst.nbSwitches)];
    }

    Rules& getRules(const Graph::Node _node) {
        return m_rules[_node - (m_inst.topology.getOrder() - m_inst.nbSwitches)];
    }

    bool isSwitch(const Graph::Node node) const {
        return node - (m_inst.topology.getOrder() - m_inst.nbSwitches) >= 0;
    }


    double getDensity(const Graph::Node node) const {
        return 100 * getRules(node).getActualNbRules() / double(m_inst.nbServers * m_inst.nbServers);
    }

    /**
    * 
    */

    virtual void addDemand(const std::size_t _demandID, 
        const Graph::Path& _path, std::vector<std::pair<Graph::Node, Rule>>& _rulesList) {
        
        std::size_t lastHop = _path.front();
        _rulesList.reserve(_rulesList.size() + _path.size());

        const auto& [s, t, d] = m_inst.demands[_demandID];
        for (auto ite_u = _path.begin(), ite_v = std::next(ite_u);
             ite_v != _path.end(); ++ite_u, ++ite_v) {
            const Graph::Node u = *ite_u, v = *ite_v;
            if (isSwitch(u)) {
                Rules& rules = getRules(u);
                const Rule rule = rules.addRule(s, t, lastHop, v);
                if (rule.isValid()) {
                    _rulesList.emplace_back(u, rule);
                }
            }
            m_residual.addEdgeWeight(u, v, -d);
            lastHop = u;
        }
        m_paths[_demandID] = _path;
    }

    virtual void addDemand(const std::size_t _demandID, 
        const Graph::Path& _path) {
        
        std::size_t lastHop = _path.front();

        const auto& [s, t, d] = m_inst.demands[_demandID];
        for (auto ite_u = _path.begin(), ite_v = std::next(ite_u);
             ite_v != _path.end(); ++ite_u, ++ite_v) {
            const Graph::Node u = *ite_u, v = *ite_v;
            if (isSwitch(u)) {
                Rules& rules = getRules(u);
                rules.addRule(s, t, lastHop, v);
            }
            m_residual.addEdgeWeight(u, v, -d);
            lastHop = u;
        }
        m_paths[_demandID] = _path;
    }

    virtual void removeDemand(const std::size_t _demandID) {
        assert(_demandID < m_inst.demands.size());
        const auto& [s, t, d] = m_inst.demands[_demandID];
        std::size_t lastHop = Rule::Wildstar;
        for (auto ite_u = m_paths[_demandID].begin(), ite_v = std::next(ite_u);
             ite_v != m_paths[_demandID].end(); ++ite_u, ++ite_v) {
            Graph::Node u = *ite_u, v = *ite_v;
            if (isSwitch(u)) {
                if (getRules(u).removeRule(s, t, lastHop, v)) {
                    m_canCompress[u] = true;
                }
            }
            m_residual.setEdgeWeight(u, v,
                m_residual.getEdgeWeight(u, v) + d);
            lastHop = u;
        }
        m_paths[_demandID].clear();
    }

    template <typename Weight>
    bool cleanFullRules() {
        // std::vector<Demand> demands;
        Graph::Node u = 0;
        for (const auto& rules : m_rules) {
            // Pop non wildcard rule till it's not full
            while (rules.isFull()) {
                Rule toRemove = rules.topNonStar(u);
                if (!toRemove.isValid()) {
                    return false;
                }
                for (std::size_t i = 0; i < m_inst.demands.size(); ++i) {
                    const auto [s, t, d] = m_inst.demands[i];
                    if (toRemove.s == s && toRemove.t == t) {
                        removeDemand(i);
                        // demands.emplace_back(i);
                        break;
                    }
                }
            }
            u++;
        }
        return routeAllDemands<Weight>();
    }

    
    template <typename Weight, bool COMPRESS = true>
    bool routeAllDemands() {
        Time timer;
        for (std::size_t demandID = 0; demandID < m_inst.demands.size(); ++demandID) {
            if(m_paths[demandID].empty()) {
                const Graph::Path path = routeDemand<Weight, Neighbors>(demandID);
                if (path.empty()) {
                    return false;
                }
                if constexpr (COMPRESS) {
                    for (const Graph::Node u : path) {
                        if (isSwitch(u)) {
                            auto& rule = getRules(u);
                            if (rule.isFull()) {
                                std::size_t sizeBefore = rule.getNbRules();
                                if (m_canCompress[u]) {
                                    timer.start();
                                    rule.compress();
                                    const auto pair = timer.get(false);
                                    m_compressionTimes.first += pair.first;
                                    m_compressionTimes.second += pair.second;
                                    ++m_nbCompression[u];
                                    if (rule.getNbRules() == sizeBefore) {
                                        m_canCompress[u] = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    template <typename Weight>
    bool routeAllDemandsNoLimit() {
        for (std::size_t demandID = 0; demandID < m_inst.demands.size(); ++demandID) {
            if (routeDemand<Weight, AllNeighbors>(demandID).empty()) {
                return false;
            }
        }
        return true;
    }

    template <typename Weight>
    bool forceRouteAllDemands() {
        for (std::size_t demandID = 0; demandID < m_inst.demands.size(); ++demandID) {
            if (routeDemand<Weight, AllNeighbors>(demandID).empty()) {
                return false;
            }
        }
        compressAll();
        return true;
    }

    template <typename Weight>
    bool routeAllDemandsNoCompress() {
        for (std::size_t demandID = 0; demandID < m_inst.demands.size(); ++demandID) {
            if (routeDemand<Weight, Neighbors>(demandID).empty()) {
                return false;
            }
        }
        return true;
    }

    /**
    * Compress the forwarding table of \param node if it exists 
    */
    void compress(const Graph::Node node) {
        if (isSwitch(node)) {
            m_rules[node].compress();
        }
    }

    /**
    * \brief Find and assign a path for _demandID. Install all the needed rules.
    */
    template <typename W, typename N>
    Graph::Path
    routeDemand(const std::size_t _demandID,
        std::vector<std::pair<Graph::Node, Rule>>& _rulesList) {
        if (!m_paths[_demandID].empty()) {
            return m_paths[_demandID];
        }
        m_shopat.clear();

        Graph::Path path = m_shopat.getShortestPath(m_inst.demands[_demandID].s, m_inst.demands[_demandID].t,
            N(this, _demandID),
            W(this, _demandID));

        // std::cout << path << '\n';
        if (!path.empty()) {
            addDemand(_demandID, path, _rulesList);
        }
        return path;
    }

    template <typename W, typename N>
    Graph::Path
    routeDemand(const std::size_t _demandID) {
        if (!m_paths[_demandID].empty()) {
            return m_paths[_demandID];
        }
        m_shopat.clear();

        Graph::Path path = m_shopat.getShortestPath(m_inst.demands[_demandID].s, m_inst.demands[_demandID].t,
            N(this, _demandID),
            W(this, _demandID));

        // std::cout << path << '\n';
        if (!path.empty()) {
            addDemand(_demandID, path);
        }
        return path;
    }

    template <typename W, typename N>
    Graph::Path requestRoute(const std::size_t _demandID) {
        m_shopat.clear();
        return m_shopat.getShortestPath(m_inst.demands[_demandID].s, m_inst.demands[_demandID].t,
            N(this, _demandID),
            W(this, _demandID));
    }

    // void addLink(const Graph::Edge& _edge, double _capacity) {
    //     m_residual.addEdge(_edge.first, _edge.second, _capacity);
    // }

    /** Compress all rules */
    void compressAll() {
        for (auto& rules : m_rules) {
            rules.compress();
        }
    }

    /**
    * \brief Save the current solution to _filename    
    */
    void saveMetrics(const std::string& _filename, const std::pair<double, double>& _time) const {
        nlohmann::json resultFile{
            {"CPUTime_total", _time.first},
            {"WallTime_total", _time.second},
            {"CPUTime_comp", m_compressionTimes.first},
            {"WallTime_comp", m_compressionTimes.second},
            {"nbCompression", m_nbCompression}};
        for (const auto& rule : m_rules) {
            resultFile["actualNbRules"].push_back(rule.getActualNbRules());
            resultFile["actualNbCompressedRules"].push_back(rule.getActualNbRules());
        }

        ShortestPath<DiGraph> shopat(m_inst.topology);
        for (std::size_t i = 0; i < m_inst.demands.size(); ++i) {
            const auto [s, t, d] = m_inst.demands[i];

            shopat.clear();
            resultFile["demands"].push_back({{"s", s},
                {"t", t},
                {"d", d},
                {"path", m_paths[i]},
                {"shortest_path", shopat.getShortestPathNbArcs(s, t)}});
        }
        std::ofstream ofs(_filename, std::ofstream::out);
        ofs << resultFile.dump(0);
        std::cout << "Saved to " << _filename << '\n';
    }

    void saveRules(const std::string& _filename) const {
        std::ofstream ofs(_filename);

        ofs << m_inst.topology.getOrder() << '\n';
        for (std::size_t s = 0; s < m_inst.topology.getOrder(); ++s) {
            if (isSwitch(s)) {
                const auto& rules = getRules(s).getUncompressedRules();
                ofs << rules.size() << '\n';
                for (const auto& rule : rules) {
                    ofs << rule.s << "\t" << rule.t << "\t" << rule.d << '\n';
                }
            }
        }
    }

    bool checkCompression() const {
        bool result = true;
        for (const auto& rule : m_rules) {
            result = result && rule.isCompressionValid();
        }
        return result;
    }

    /** Getter */

    std::size_t getNbDemands() const {
        return std::count_if(m_paths.begin(), m_paths.end(), [](auto&& _p){
            return !_p.empty();
        });
    }

    std::size_t getNbActiveLinks() const {
        return m_residual.size();
    }

    const std::vector<Rules>& getRules() const {
        return m_rules;
    }

    std::vector<Rules>& getRules() {
        return m_rules;
    }

    std::vector<Graph::Edge> getEdges() const {
        return m_residual.getEdges();
    }

    std::size_t getMaxMem() const {
        return m_inst.maxMem;
    }

    const DiGraph& getResidual() const {
        return m_residual;
    }

    const std::vector<std::size_t>& getNbCompression() const {
        return m_nbCompression;
    }

    const std::pair<double, double>& getCompressionTimes() const {
        return m_compressionTimes;
    }

    void setNbCompression(const std::vector<std::size_t>& _nbCompression) {
        m_nbCompression = _nbCompression;
    }

    void setCompressionTimes(const std::pair<double, double>& _compressionTimes) {
        m_compressionTimes = _compressionTimes;
    }

    /** Pseudo getter */

    /**
    * \brief Return the energy savings provided by the solution in percentage
    */
    double getEnergySavings() const {
        return 100.0 * (m_inst.topology.size() - m_residual.size()) / double(m_inst.topology.size());
    }

    std::vector<Graph::Edge>
    getRemovableEdges(const std::set<Graph::Edge>& _nonRemovableEdge) const {
        auto edges = m_residual.getEdges();
        edges.erase(std::remove_if(edges.begin(), edges.end(),
                        [&](const Graph::Edge& __edge) {
                            return _nonRemovableEdge.find(__edge) != _nonRemovableEdge.end();
                        }),
            edges.end());
        std::sort(edges.begin(), edges.end(),
            [&](const Graph::Edge& __e1, const Graph::Edge& __e2) {
                return getUsedCapacity(__e1) < getUsedCapacity(__e2);
            });
        return edges;
    }

    std::vector<Graph::Edge>
    getRemovableLinks(const std::set<Graph::Edge>& _nonRemovableLinks) const {
        auto links = m_residual.getEdges();
        // Keep only links
        links.erase(std::remove_if(links.begin(), links.end(),
                        [&](const Graph::Edge& _edge) {
                            return _edge.first < _edge.second;
                        }),
            links.end());
        // Remove unremovable links
        links.erase(std::remove_if(links.begin(), links.end(),
                        [&](const Graph::Edge& _edge) {
                            return _nonRemovableLinks.find(_edge) != _nonRemovableLinks.end();
                        }),
            links.end());
        // Sort by used capacity
        std::sort(
            links.begin(), links.end(),
            [&](const Graph::Edge& _e1, const Graph::Edge& _e2) {
                return getUsedCapacity(_e1) + getUsedCapacity(_e1.second, _e1.first) < getUsedCapacity(_e2) + getUsedCapacity(_e2.second, _e2.first);
            });
        return links;
    }

    double getUsedCapacity(Graph::Edge _edge) const {
        return getUsedCapacity(_edge.first, _edge.second);
    }

    double getUsedCapacity(Graph::Node _u, Graph::Node _v) const {
        return m_inst.topology.getEdgeWeight(_u, _v) - m_residual.getEdgeWeight(_u, _v);
    }

    bool checkMemory() const {
        bool result = true;
        for (std::size_t u = 0; u < m_rules.size(); ++u) {
            const auto& rules = getRules(u);
            if (rules.getNbRules() > rules.getMaxSize()) {
                std::cout << u << " is full: " << rules.getNbRules() << '/'
                          << rules.getMaxSize() << '\n';
                result = false;
            }
        }
        return result;
    }

    /** Weight */

    struct EarDemand {
        EarDemand(Routing* _ear, const std::size_t _demandID)
            : ear(_ear)
            , demandID(_demandID) {}
        EarDemand(const EarDemand&) = default;
        EarDemand& operator=(const EarDemand&) = default;
        EarDemand(EarDemand&&) noexcept = default;
        EarDemand& operator=(EarDemand&&) noexcept = default;
        virtual ~EarDemand() = default;

        Routing* ear;
        std::size_t demandID;
    };

    struct DumbWeight : public EarDemand {
        DumbWeight(Routing* _ear, const std::size_t _demandID)
            : EarDemand(_ear, _demandID) {}
        double operator()(const Graph::Node /*unused*/, const Graph::Node /*unused*/) { return 1; }
    };

    template <std::size_t A, std::size_t B>
    struct MetricWeight : public EarDemand {
        MetricWeight(Routing* _ear, const std::size_t _demandID)
            : EarDemand(_ear, _demandID) {}

        double operator()(const Graph::Node u, const Graph::Node v) {
            const auto [s, t, d] = ear->m_inst.demands[demandID];

            const double wCapa = (ear->getUsedCapacity(u, v) + d) / (ear->m_inst.topology.getEdgeWeight(u, v));
            double wRules = 0;
            if (ear->isSwitch(u) && ear->getRules(u).findCompressedPort(s, t) != v) {
                wRules = ear->getRules(u).getNbRules() / double(ear->getRules(u).getMaxSize());
            }
            return (wCapa * A + wRules * B) / (A + B);
        }
        using EarDemand::demandID;
        using EarDemand::ear;
    };

    template <std::size_t A, std::size_t B>
    struct MetricPlusOneWeight : public MetricWeight<A, B> {

        MetricPlusOneWeight(Routing* _ear, const std::size_t _demandID)
            : MetricWeight<A, B>(_ear, _demandID) {}

        double operator()(const Graph::Node u, const Graph::Node v) {
            return 1 + MetricWeight<A, B>::operator()(u, v);
        }
    };

    /** Neighbors */

    struct Neighbors : public EarDemand {
        Neighbors(Routing* _ear, const std::size_t _demandID)
            : EarDemand(_ear, _demandID) {}
        bool operator()(const Graph::Node u, const Graph::Node v) {
            const auto [s, t, d] = ear->m_inst.demands[demandID];
            // Filter neighbors if not enough capa or cannot add rules
            return !(ear->m_residual.getEdgeWeight(u, v) < d
                     || (ear->isSwitch(u) && !ear->getRules(u).canAddRule(s, t, v)));
        }

        using EarDemand::demandID;
        using EarDemand::ear;
    };

    struct AllNeighbors : public EarDemand {
        AllNeighbors(Routing* _ear, const std::size_t _demandID)
            : EarDemand(_ear, _demandID) {}

        bool operator()(const Graph::Node u, const Graph::Node v) {
            return ear->m_residual.getEdgeWeight(u, v) > ear->m_inst.demands[demandID].d;
        }

        using EarDemand::demandID;
        using EarDemand::ear;
    };


  protected:
    Instance m_inst;
    std::vector<Rules> m_rules;
    std::vector<Graph::Path> m_paths;
    DiGraph m_residual;
    ShortestPath<DiGraph> m_shopat;
    std::vector<bool> m_canCompress;
    std::vector<std::size_t> m_nbCompression;
    std::pair<double, double> m_compressionTimes = {0, 0};
};

std::ostream& operator<<(std::ostream& _out, const Demand& _demand) {
    return _out << '(' << _demand.s << ", " << _demand.t << ") " << _demand.d;
}

#endif