#ifndef EARROUTING_HPP
#define EARROUTING_HPP

#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>

#include <cstdlib>

#include "DiGraph.hpp"

#include "LinkedMatrix.hpp"
#include "Routing.hpp"

template <typename Rules>
class EarRouting : public Routing<Rules> {
  public:
    EarRouting(Instance _inst)
        : Routing<Rules>(_inst)
        , m_demandsOnEdge(m_inst.topology.getOrder(), m_inst.topology.getOrder()) {
        for (const auto& edge : m_inst.topology.getEdges()) {
            m_demandsOnEdge.add(edge.first, edge.second, {});
        }
    }

    EarRouting(const EarRouting& _other) = default;
    EarRouting& operator=(const EarRouting& _other) = default;

    EarRouting(EarRouting&& _other) noexcept = default;
    EarRouting& operator=(EarRouting&& _other) noexcept = default;
    ~EarRouting() override = default;

    /**
  Add the demand on the network
  */
    void addDemand(const std::size_t _demandID, 
        const Graph::Path& _path) override {
        //std::cout << "addDemand("<< _demandID <<", "<<_path << ")\n";
        assert(_demandID < m_inst.demands.size());
        assert(_path.front() == m_inst.demands[_demandID].s);
        assert(_path.back() == m_inst.demands[_demandID].t);

        for (auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end();
             ++iteU, ++iteV) {
            assert(m_demandsOnEdge.find(*iteU, *iteV) != nullptr);
            m_demandsOnEdge.find(*iteU, *iteV)->value.push_back(_demandID);
        }
        Routing<Rules>::addDemand(_demandID, _path);
    }

    /**
  Remove the demand from the network
  Remove all corresponding non aggretation rules
  Invalidate iterators on m_demandsOnEdge lists.
  */
    void removeDemand(const std::size_t _demandID) override {
        //std::cout << "removeDemand("<< _demandID << ")\n";
        assert(_demandID < m_inst.demands.size());

        auto& path = m_paths[_demandID];
        for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end();
             ++iteU, ++iteV) {
            assert(m_demandsOnEdge.find(*iteU, *iteV) != nullptr);
            m_demandsOnEdge.find(*iteU, *iteV)->value.remove(_demandID);
        }
        return Routing<Rules>::removeDemand(_demandID);
    }

    void addEdge(const Graph::Edge& _edge) {
        m_residual.addEdge(_edge.first, _edge.second, m_inst.topology.getEdgeWeight(_edge));
    }

    /**
  Remove _edge from the network and return all the demands that were using it
  */
    std::vector<std::pair<std::size_t, Graph::Path>> deleteEdge(const Graph::Edge& _edge) {
        assert(m_demandsOnEdge.find(_edge.first, _edge.second) != nullptr);
        const auto& demandsOnEdge =
            m_demandsOnEdge.find(_edge.first, _edge.second)->value;

        std::vector<std::pair<std::size_t, Graph::Path>> demands(demandsOnEdge.size());
        
        std::size_t i = 0;
        for (const auto& demand : demandsOnEdge) {
            demands[i] = { demand, m_paths[demand] };
            ++i;
        }

        for (const auto& [demand, path] : demands) {
            removeDemand(demand);
        }        
        m_residual.removeEdge(_edge);
        return demands;
    }

    /**
  Remove both edges of the link between u and v, i.e., (u, v) and (v, u)
  */
    std::vector<Demand> deleteLink(const Graph::Edge& _link) {
        // Get demands on both edges
        auto& demandsOnEdge =
            m_demandsOnEdge.find(_link.first, _link.second)->value;
        std::vector<Demand> demands(demandsOnEdge.begin(), demandsOnEdge.end());
        demandsOnEdge = m_demandsOnEdge.find(_link.second, _link.first)->value;
        demands.assign(demandsOnEdge.begin(), demandsOnEdge.end());

        for (const auto& demand : demands) {
            removeDemand(demand);
        }
        m_residual.removeEdge(_link);
        m_residual.removeEdge(_link.second, _link.first);

        return demands;
    }

    using Routing<Rules>::m_inst;
    using Routing<Rules>::m_paths;
    using Routing<Rules>::m_residual;
    using Routing<Rules>::m_rules;    

  private:
    LinkedMatrix<std::list<std::size_t>> m_demandsOnEdge;
};

#endif