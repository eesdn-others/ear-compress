#ifndef MOSTSAVERULES_H
#define MOSTSAVERULES_H

#include "CompressedRules.hpp"

class MostSaveRules : public CompressedRules {
  private:
  public:
    MostSaveRules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    ~MostSaveRules() override = default;

    MostSaveRules(const MostSaveRules&) = default;
    MostSaveRules& operator=(const MostSaveRules&) = default;

    MostSaveRules(MostSaveRules&&) = default;
    MostSaveRules& operator=(MostSaveRules&&) = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    void compress() override;
    std::size_t findCompressedPort(std::size_t _s, std::size_t _t) const override;
};
#endif