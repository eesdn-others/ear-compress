#ifndef RULES_H
#define RULES_H

#include <chrono>
#include <fstream>
#include <iostream>
#include <list>
#include <random>
#include <sstream>

#include "EARC.hpp"
#include "LinkedMatrix.hpp"

class Rules {
  protected:
    std::size_t m_nbServers;
    std::size_t m_order;
    LinkedMatrix<std::pair<std::size_t, std::size_t>> m_routingMatrix;
    std::size_t m_nbRules = 0;
    std::size_t m_maxSize;

  public:
    Rules(std::size_t _nbServers, std::size_t _maxSize);
    Rules(std::size_t _nbServers, std::size_t _maxSize, std::size_t _order);
    virtual ~Rules() = default;
    Rules(const Rules&) = default;
    Rules& operator=(const Rules&) = default;
    Rules(Rules&&) = default;
    Rules& operator=(Rules&&) = default;

    Rule addRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);
    bool removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop);

    bool canAddRule(std::size_t /*_s*/, std::size_t /*_t*/, std::size_t /*_p*/) const {
        return m_nbRules < m_maxSize;
    }
    bool isFull() const {
        return m_nbRules >= m_maxSize;
    }

    std::list<Rule> getRules() const;
    std::list<Rule> getUncompressedRules() const;

    void saveRules(const std::string& _filename) const;

    std::size_t getNbRules() const {
        return m_nbRules;
    }

    std::size_t getMaxSize() const {
        return m_maxSize;
    }
    void setMaxSize(std::size_t _size) {
        m_maxSize = _size;
    }

    /** 
    * Return the most occuring port of the table
    */
    std::size_t getMaxOccuringPort() const; 

    friend std::ostream& operator<<(std::ostream& _out, const Rules& _rules);
};

std::ostream& operator<<(std::ostream& _out, const Rule& _rule);

#endif