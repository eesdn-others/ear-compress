#include "net_beaconcontroller_routing_Compression.hpp"

#include <vector>

#include "EarRouting.hpp"
#include "Network.h"

typedef WildcardRules SDNRules;
Network* network = nullptr;
EarRouting<SDNRules>* ear = nullptr;
std::vector<jlong>* nodes = nullptr;
int tableSize = 0;
enum Priority { WILDCARD = 1,
    NORMAL = 0,
    DEFAULT = 2 };
jclass ruleClass;
jmethodID ruleCons;

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    setTableLimit
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_net_beaconcontroller_routing_Compression_setTableLimit(JNIEnv* _env,
    jobject _object,
    jint _tableSize) {
    tableSize = _tableSize;
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    addNodes
 * Signature: ([J)V
 */
JNIEXPORT void JNICALL Java_net_beaconcontroller_routing_Compression_addNodes(
    JNIEnv* _env, jobject _object, jlongArray _array) {
    if (network != nullptr)
        return;
    jlong* cArr = (*_env).GetLongArrayElements(_array, NULL);
    int size = (*_env).GetArrayLength(_array);

    nodes = new std::vector<jlong>(cArr, cArr + size);
    network = new Network(*nodes);
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    addLink
 * Signature: (JJSSD)V
 */
JNIEXPORT void JNICALL Java_net_beaconcontroller_routing_Compression_addLink(
    JNIEnv* _env, jobject _object, jlong _u, jlong _v, jshort _up, jshort _vp,
    jdouble _c) {
    network->addLink(_u, _v, _up, _vp, _c);
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_net_beaconcontroller_routing_Compression_init(
    JNIEnv* _env, jobject _object) {
    if (ear != nullptr) {
        return;
    }
    ruleClass = (*_env).FindClass("net/beaconcontroller/routing/Rule");
    ruleCons = (*_env).GetMethodID(ruleClass, "<init>", "(JJJSSJI)V");
    ear = new EarRouting<SDNRules>(*network, tableSize);
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    calculateRoute
 * Signature: (JJD)[Lnet/beaconcontroller/routing/Rule;
 * Returns an array of rules to install
 */
JNIEXPORT jobjectArray JNICALL
Java_net_beaconcontroller_routing_Compression_calculateRoute(JNIEnv* _env,
    jobject _object,
    jlong _idSrc,
    jlong _idDest,
    jdouble _charge) {
    // ruleClass = (*_env).FindClass("net/beaconcontroller/routing/Rule");

    std::list<std::pair<int, Rule>> rules;
    auto idSrc = network->idToNode(_s), idDest = network->idToNode(_t);

    // Check if demands already in system
    if (ear->isDemandIn(idSrc, idDest)) {
        auto path = ear->getPath(Demand(idSrc, idDest, _charge));
        // Create and fill the Java array
        jobjectArray rulesArray =
            (*_env).NewObjectArray(path.size() - 1, ruleClass, nullptr);
        auto ite = path.begin();
        for (int i = 0; i < (int)path.size() - 1; ++i, ++ite) {
            auto node = *ite;
            auto id = network->nodeToId(node);
            (*_env).SetObjectArrayElement(
                rulesArray, i,
                RuleToJNI(_env, ear->getRules()[node].findRule(idSrc, idDest), id));
        }
        return rulesArray;
    }
    if (!ear->routeDemand<EarRouting<SDNRules>::MetricPlusOneWeight<1, 1>,
            EarRouting<SDNRules>::Neighbors>(
            Demand(idSrc, idDest, _charge), &SDNRules::addRule, &rules)) {
        // std::cout << "No route found!" << std::endl;
        return nullptr;
    }
    // Create and fill the Java array
    jobjectArray rulesArray =
        (*_env).NewObjectArray(rules.size(), ruleClass, nullptr);
    int i = 0;
    for (auto pair : rules) {
        const Rule& rule = pair.second;
        int node = pair.first;
        //        std::cout << ear->getRules()[node].ge÷\tActualNbRules() <<
        //        ear->getRules()[node].getCompressedRules().size() << std::endl;
        long id = network->nodeToId(node);
        (*_env).SetObjectArrayElement(rulesArray, i, RuleToJNI(_env, rule, id));
        ++i;
    }
    return rulesArray;
}

JNIEXPORT jobjectArray JNICALL
Java_net_beaconcontroller_routing_Compression_requestRoute(JNIEnv* _env,
    jobject _object,
    jlong _idSrc,
    jlong _idDest,
    jdouble _charge) {
    // ruleClass = (*_env).FindClass("net/beaconcontroller/routing/Rule");

    // Get 'IP' for source and destination
    auto ipSrc = m_ID_IP[_idSrc], ipDest = m_IP_ID[_ipDest];
    // Get gateway of servers
    auto gatewaySrc = m_gateway[ipSrc], gatewayDest = m_gateway[ipDest];
    ear->requestRoute<EarRouting<SDNRules>::MetricPlusOneWeight<1, 1>,
        EarRouting<SDNRules>::Neighbors>(gatewaySrc, gatewayDest);
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    isInDemand
 * Signature: (JJD)[Lnet/beaconcontroller/routing/Rule;
 */
JNIEXPORT jboolean JNICALL
Java_net_beaconcontroller_routing_Compression_isDemandIn(JNIEnv* _env,
    jobject _object,
    jlong _s, jlong _t) {
    return ear->isDemandIn(network->idToNode(_s), network->idToNode(_t));
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    turnOffLinks
 * Signature: ()V;
 */
JNIEXPORT void JNICALL
Java_net_beaconcontroller_routing_Compression_turnOffLinks(JNIEnv* _env,
    jobject _object) {
    ear->turnOffLinks<EarRouting<SDNRules>::MetricPlusOneWeight<1, 1>>();
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    getRules
 * Signature: (J)[Lnet/beaconcontroller/routing/Rule;
 */
JNIEXPORT jobjectArray JNICALL
Java_net_beaconcontroller_routing_Compression_getRules(JNIEnv* _env,
    jobject _object,
    jlong _id) {
    jclass ruleClass = (*_env).FindClass("net/beaconcontroller/routing/Rule");
    jmethodID ruleCons = (*_env).GetMethodID(ruleClass, "<init>", "(JJJSSJI)V");
    int node = network->idToNode(_id);

    // Create and fill the Java array
    const SDNRules& rules = ear->getRules()[node];
    //    std::cout << "# rules: " << rules.getCompressedRules().size() <<
    //    std::endl;

    jobjectArray rulesArray =
        (*_env).NewObjectArray(rules.getRules().size(), ruleClass, nullptr);
    int i = 0;
    for (const Rule& rule : rules.getRules()) {
        //        std::cout << rule << std::endl;
        int s = std::get<0>(rule), t = std::get<1>(rule), lh = std::get<2>(rule),
            nh = std::get<3>(rule);

        jint priority = Priority::NORMAL;
        if (s == -1 && t == -1) {
            priority = Priority::DEFAULT;
        } else if (s == -1 || t == -1) {
            priority = Priority::WILDCARD;
        }
        int srcPort = lh == -1 ? -1 : network->getPort(lh, node).second;
        int dstPort = network->getPort(node, nh).first;
        long idS = s == -1 ? -1 : network->nodeToId(s);
        long idT = t == -1 ? -1 : network->nodeToId(t);
        (*_env).SetObjectArrayElement(
            rulesArray, i,
            (*_env).NewObject(ruleClass, ruleCons, _id, idS, idT, srcPort, dstPort,
                network->nodeToId(nh), priority));
        ++i;
    }
    return rulesArray;
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    exit
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_net_beaconcontroller_routing_Compression_exit(
    JNIEnv* _env, jobject _object) {
    delete network;
    delete ear;
    delete nodes;
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    isFull
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL Java_net_beaconcontroller_routing_Compression_isFull(
    JNIEnv* _env, jobject _object, jlong _id) {
    return ear->getRules()[network->idToNode(_id)].isFull();
}

/*
 * Class:     net_beaconcontroller_routing_Compression
 * Method:    compress
 * Signature: (J)Z
 */
JNIEXPORT void JNICALL Java_net_beaconcontroller_routing_Compression_compress(
    JNIEnv* _env, jobject _object, jlong _id) {
    ear->compress(network->idToNode(_id));
}

/*
 * Class:     Java_net_beaconcontroller_routing_Compression
 * Method:    getNbRules
 * Signature: (J)J
 */
JNIEXPORT jint JNICALL Java_net_beaconcontroller_routing_Compression_getNbRules(
    JNIEnv* _env, jobject _object, jlong _id) {
    return ear->getRules()[network->idToNode(_id)].getNbRules();
}

/*
 * Class:     Java_net_beaconcontroller_routing_Compression
 * Method:    getNbRules
 * Signature: (J)J
 */
JNIEXPORT jint JNICALL
Java_net_beaconcontroller_routing_Compression_getActualNbRules(JNIEnv* _env,
    jobject _object,
    jlong _id) {
    return ear->getRules()[network->idToNode(_id)].getActualNbRules();
}

jobject RuleToJNI(JNIEnv* _env, const Rule& _rule, long id) {
    int node = network->idToNode(id);
    int s = std::get<0>(_rule), t = std::get<1>(_rule), lh = std::get<2>(_rule),
        nh = std::get<3>(_rule);

    jint priority = Priority::NORMAL;
    if (s == -1 && t == -1) {
        priority = Priority::DEFAULT;
    } else if (s == -1 || t == -1) {
        priority = Priority::WILDCARD;
    }
    int srcPort = lh == -1 ? -1 : network->getPort(lh, node).second;
    int dstPort = network->getPort(node, nh).first;

    long idS = s == -1 ? -1 : network->nodeToId(s);
    long idT = t == -1 ? -1 : network->nodeToId(t);
    return (*_env).NewObject(ruleClass, ruleCons, id, idS, idT, srcPort, dstPort,
        network->nodeToId(nh), priority);
}