#include "Rules.hpp"

Rules::Rules(const std::size_t _nbServers, const std::size_t _maxSize, const std::size_t _order)
    : m_nbServers(_nbServers)
    , m_order(_order)
    , m_routingMatrix(_nbServers, _nbServers)
    , m_maxSize(_maxSize) {}

Rules::Rules(const std::size_t _nbServers, const std::size_t _maxSize)
    : Rules(_nbServers, _maxSize, _nbServers) {}

Rule Rules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    m_routingMatrix.add(_s, _t, std::make_pair(_lastHop, _nextHop));
    ++m_nbRules;
    return {_s, _t, _lastHop, _nextHop};
}

bool Rules::removeRule(const std::size_t _s, const std::size_t _t, const std::size_t /*_lastHop*/, const std::size_t /*_nextHop*/) {
    m_routingMatrix.remove(_s, _t);
    --m_nbRules;
    return true;
}

std::list<Rule> Rules::getRules() const {
    return getUncompressedRules();
}

std::size_t Rules::getMaxOccuringPort() const {
    std::vector<std::size_t> count(m_order, 0);
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            ++count[cell->value.second];
        }
    }

    // Add non default port rules
    return std::distance(count.begin(),
        std::max_element(count.begin(), count.end()));
}

std::list<Rule> Rules::getUncompressedRules() const {
    std::list<Rule> rules;
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            auto t = cell->getColumn();
            rules.emplace_back(s, t, cell->value.first, cell->value.second);
        }
    }
    return rules;
}

void Rules::saveRules(const std::string& _filename) const {
    std::ofstream ofs(_filename);
    ofs << m_order << '\t' << m_order << '\n';
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            auto t = cell->getColumn();
            ofs << s << '\t' << t << '\t' << cell->value.second << '\n';
        }
    }
}