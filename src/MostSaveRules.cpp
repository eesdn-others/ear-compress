#include "MostSaveRules.hpp"

#include <algorithm>
#include <iostream>
#include <tuple>

#include <functional>
#include <map>
#include <set>
#include <utility>

#include <BinaryHeap.hpp>

MostSaveRules::MostSaveRules(const std::size_t _nbServers, const std::size_t _maxSize, const std::size_t _order)
    : CompressedRules(_nbServers, _maxSize, _order) {}

Rule MostSaveRules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    auto rule = Rules::addRule(_s, _t, _lastHop, _nextHop);

    if (m_compressed) {
        if (_nextHop != findCompressedPort(_s, _t)) {
            m_compressedRules.emplace_front(_s, _t, _lastHop, _nextHop);
            return m_compressedRules.front();
        }
        return {Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, Rule::Wildstar};
    }
    return rule;
}

void MostSaveRules::compress() {
    std::vector<std::vector<std::size_t>> count(2 * m_nbServers,
        std::vector<std::size_t>(m_order, 0));
    std::vector<std::size_t> defaultPort(2 * m_nbServers, Rule::Wildstar);
    std::vector<std::size_t> nbRules(2 * m_nbServers);

    // Check default port per row
    for (std::size_t s = 0; s < m_nbServers; ++s) {
        if (m_routingMatrix.begin_row(s) != nullptr) {
            for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                 cell = cell->getNextOnRow()) {
                ++count[s][cell->value.second];
                ++nbRules[s];
            }

            // Set default port
            std::size_t port = 0;
            for (std::size_t i = 1; i < m_order; ++i) {
                if (count[s][port] < count[s][i]) {
                    port = i;
                }
            }
            defaultPort[s] = port;
        }
    }

    // Check default port per column
    for (std::size_t t = 0; t < m_nbServers; ++t) {
        if (m_routingMatrix.begin_column(t) != nullptr) {
            for (auto cell = m_routingMatrix.begin_column(t); cell != nullptr;
                 cell = cell->getNextOnCol()) {
                ++count[t + m_nbServers][cell->value.second];
                ++nbRules[t + m_nbServers];
            }

            // Set default port
            std::size_t port = 0;
            for (std::size_t i = 1; i < m_order; ++i) {
                if (count[t + m_nbServers][port] < count[t + m_nbServers][i]) {
                    port = i;
                }
            }
            defaultPort[t + m_nbServers] = port;
        }
    }

    using Heap = BinaryHeap<std::size_t, std::function<bool(std::size_t, std::size_t)>>;
    Heap heap(
        m_nbServers * 2, [&](std::size_t u, std::size_t v) {
            return count[u][defaultPort[u]] > count[v][defaultPort[v]];
        });
    std::vector<Heap::Handle*> handles(m_nbServers * 2, nullptr);
    std::vector<std::list<std::size_t>> parent(m_nbServers * 2);

    m_compressedRules.clear();
    for (std::size_t i = 0; i < 2 * m_nbServers; ++i) {
        handles[i] = heap.push(i);
    }

    std::vector<bool> compressed(2 * m_nbServers, false);

    while (!heap.empty()) {
        auto i = heap.top();
        heap.pop();
        compressed[i] = true;
        if (nbRules[i] > 0) {
            if (i < m_nbServers) {
                // Compress row
                std::size_t s = i;
                for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                     cell = cell->getNextOnRow()) {
                    auto t = cell->getColumn();
                    std::size_t p = cell->value.second;
                    --nbRules[t + m_nbServers];
                    --count[t + m_nbServers][p];

                    // Update default port
                    std::size_t port = 0;
                    for (std::size_t j = 1; j < m_order; ++j) {
                        if (count[t + m_nbServers][port] < count[t + m_nbServers][j]) {
                            port = j;
                        }
                    }
                    defaultPort[t + m_nbServers] = port;
                    heap.increase(handles[t + m_nbServers]);

                    if (p != defaultPort[s] && !compressed[t + m_nbServers]) {
                        m_compressedRules.emplace_back(s, t, Rule::Wildstar, p);
                    }
                }
                m_compressedRules.emplace_back(s, Rule::Wildstar, Rule::Wildstar, defaultPort[s]);
            } else {
                // Compress col
                std::size_t t = i - m_nbServers;
                for (auto cell = m_routingMatrix.begin_column(t); cell != nullptr;
                     cell = cell->getNextOnCol()) {
                    auto s = cell->getRow();
                    auto p = cell->value.second;
                    --nbRules[s];
                    --count[s][p];

                    // Update default port
                    std::size_t port = 0;
                    for (std::size_t j = 1; j < m_order; ++j) {
                        if (count[s][port] < count[s][j]) {
                            port = j;
                        }
                    }
                    defaultPort[s] = port;
                    heap.increase(handles[s]);

                    if (p != defaultPort[i] && !compressed[s]) {
                        m_compressedRules.emplace_back(s, t, Rule::Wildstar, p);
                    }
                }
                m_compressedRules.emplace_back(Rule::Wildstar, t, Rule::Wildstar, defaultPort[i]);
            }
        }
    }

    const auto dp = m_compressedRules.back().nh;
    while (!m_compressedRules.empty() && m_compressedRules.back().nh == dp) {
        m_compressedRules.pop_back();
    }
    m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, dp);
    m_compressed = true;
}

std::size_t MostSaveRules::findCompressedPort(std::size_t _s, std::size_t _t) const {
    for (auto& rule : m_compressedRules) {
        std::size_t s = rule.s, t = rule.t;
        if ((s == Rule::Wildstar || s == _s) && (t == Rule::Wildstar || t == _t)) {
            return rule.nh;
        }
    }
    return Rule::Wildstar;
}

// void save(const std::strin& compMode,