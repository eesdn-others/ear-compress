#include "WildcardRules.hpp"

#include "EARC.hpp"
#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <random>
#include <utility>

#ifdef CHRONO
#include <chrono>
#endif

WildcardRules::WildcardRules(const std::size_t _nbServers, const std::size_t _maxSize,
    const std::size_t _order)
    : DefaultPortRules(_nbServers, _maxSize, _order)
    , m_defaultPortRow(m_nbServers, Rule::Wildstar)
    , m_defaultPortCol(m_nbServers, Rule::Wildstar)
    , m_compressionType(CompressionType::NONE) {
}

Rule WildcardRules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    const auto rule = Rules::addRule(_s, _t, _lastHop, _nextHop);

    if (isCompressed()) {
        const auto compRule = findCompressedRule(_s, _t);
        
        // Only add the rule if no wildcard rules already exists
        if (_nextHop != compRule.nh) {
            m_compressedRules.emplace_front(_s, _t, _lastHop, _nextHop);
            m_hasChangedSinceLastComp = true;
            // assert(isCompressionValid());
            return m_compressedRules.front();
        }
        // Otherwise, return the corresponding wildcard rule
        // assert(isCompressionValid());
        return compRule;
    }
    // assert(isCompressionValid());
    m_hasChangedSinceLastComp = true;
    return rule;
}

void WildcardRules::save() {
    static std::size_t i = 0;
    std::ofstream ofs("dump num " + std::to_string(i));

    // Dump uncompressed rules
    const auto& rules = getUncompressedRules();
    ofs << rules.size() << std::endl;
    for (const auto& rule : rules) {
        ofs << rule.s << "\t" << rule.t << "\t" << rule.nh << std::endl;
    }
    ofs << m_compressedRules.size() << std::endl;
    for (const auto& rule : m_compressedRules) {
        ofs << rule.s << "\t" << rule.t << "\t" << rule.nh << std::endl;
    }
    ofs.close();
    ++i;
}

bool WildcardRules::removeRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop,
    const std::size_t _nextHop) {
    m_hasChangedSinceLastComp = CompressedRules::removeRule(_s, _t, _lastHop, _nextHop);
    if (m_nbRules == 0) {
        m_compressionType = CompressionType::NONE;
        m_defaultPort = Rule::Wildstar;
        m_compressedRules.clear();
    }    
    // assert(isCompressionValid());
    return m_hasChangedSinceLastComp;
}

void WildcardRules::compress() {
    std::cout << "comp" << '\n';
    if (!m_hasChangedSinceLastComp) {
        return;
    }
#ifdef CHRONO
    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();
#endif
    // std::cout << "WildcardRules::compress()" << std::endl;
    // std::cout << __FILE__ << ":"<< __LINE__ <<std::endl;
    if (m_nbRules == 0) {
        return;
    }

    std::vector<std::size_t> allCount(m_order, 0); // Count occurence of ports
    std::vector<std::size_t> count(m_order, 0);           // Count occurence of ports per row/column
    std::vector<std::size_t> starCount(m_order, 0); // Count occurence of port per wildcard rules
    //////////////////////////
    //                      //
    //                      //
    //                      //
    //   Compress by row    //
    //                      //
    //                      //
    //                      //
    //////////////////////////

    // Compress by row
    std::size_t savedRow = 0;
    std::vector<std::size_t> defaultPortsRow(m_nbServers, Rule::Wildstar);

    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        // Count for each row the dominating port
        std::fill(count.begin(), count.end(), 0);
        // Count value of the row
        auto cell = m_routingMatrix.begin_row(s);
        if (cell == nullptr) {
            continue;
        }
        for (; cell != nullptr; cell = cell->getNextOnRow()) {
            const std::size_t p = cell->value.second;
            ++count[p];
            ++allCount[p];
        }

        defaultPortsRow[s] = std::distance(
            count.begin(), std::max_element(count.begin(), count.end()));
        savedRow += count[defaultPortsRow[s]] - 1;
        ++starCount[defaultPortsRow[s]];
    }
    std::size_t defaultPortRow = std::distance(
        starCount.begin(), std::max_element(starCount.begin(), starCount.end()));
    savedRow += starCount[defaultPortRow] - 1;

    //////////////////////////
    //                      //
    //                      //
    //                      //
    //  Compress by column  //
    //                      //
    //                      //
    //                      //
    //////////////////////////
    std::fill(starCount.begin(), starCount.end(), 0);

    std::size_t savedColumn = 0;
    std::vector<std::size_t> defaultPortsCol(m_nbServers, Rule::Wildstar);

    for (std::size_t t = 0; t < m_routingMatrix.columnSize(); ++t) {
        // Count for each row the dominating port
        std::fill(count.begin(), count.end(), 0);
        // Count value of the row
        auto cell = m_routingMatrix.begin_column(t);
        if (cell == nullptr) {
            defaultPortsCol[t] = Rule::Wildstar;
            continue;
        }
        for (; cell != nullptr; cell = cell->getNextOnCol()) {
            std::size_t p = cell->value.second;
            ++count[p];
        }

        defaultPortsCol[t] = std::distance(
            count.begin(), std::max_element(count.begin(), count.end()));
        savedColumn += count[defaultPortsCol[t]] - 1;
        ++starCount[defaultPortsCol[t]];
    }
    std::size_t defaultPortColumn = std::distance(
        starCount.begin(), std::max_element(starCount.begin(), starCount.end()));
    savedColumn += starCount[defaultPortColumn] - 1;

    //////////////////////////
    //                      //
    //                      //
    //                      //
    // Compress by default  //
    //                      //
    //                      //
    //                      //
    //////////////////////////

    const std::size_t defaultPort = std::distance(
        allCount.begin(), std::max_element(allCount.begin(), allCount.end()));
    const std::size_t savedDefault = allCount[defaultPort] - 1;

    // Check if compression save more rules than current compression
    if (m_compressionType != CompressionType::NONE) {
        std::size_t saved = m_nbRules - m_compressedRules.size();
        if (saved > savedDefault && saved > savedRow && saved > savedColumn) {
            m_hasChangedSinceLastComp = false;
            return;
        }
    }

    if (savedDefault >= savedRow && savedDefault >= savedColumn) {
        // Use default port
        m_compressedRules.clear();

        for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
            for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                 cell = cell->getNextOnRow()) {
                const std::size_t t = cell->getColumn();
                const std::size_t p = cell->value.second;
                if (p != defaultPort) {
                    m_compressedRules.emplace_back(s, t, cell->value.first, p);
                }
            }
        }
        m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar,
            Rule::Wildstar, defaultPort);
        m_defaultPort = defaultPort;
        m_compressionType = CompressionType::DEFAULT;
        m_compressed = true;
    } else if (savedColumn > savedRow) {
        // Compress by column
        m_compressedRules.clear();
        m_defaultPortCol = std::move(defaultPortsCol);

        for (std::size_t t = 0; t < m_routingMatrix.columnSize(); ++t) {
            for (auto cell = m_routingMatrix.begin_column(t); cell != nullptr;
                 cell = cell->getNextOnCol()) {
                auto s = cell->getRow();
                std::size_t p = cell->value.second;

                if (p != m_defaultPortCol[t]) {
                    m_compressedRules.emplace_back(s, t, cell->value.first, p);
                }
            }
            // Add wildcard if not dominating wildcard port
            if (m_defaultPortCol[t] != Rule::Wildstar
                && m_defaultPortCol[t] != defaultPortColumn) {
                m_compressedRules.emplace_back(Rule::Wildstar, t, Rule::Wildstar,
                    m_defaultPortCol[t]);
            }
        }
        m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar,
            Rule::Wildstar, defaultPortColumn);
        m_defaultPort = defaultPortColumn;
        m_compressionType = CompressionType::COLUMN;
        m_compressed = true;
    } else {
        // Compress by row
        m_compressedRules.clear();
        m_defaultPortRow = std::move(defaultPortsRow);

        for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
            for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                 cell = cell->getNextOnRow()) {
                auto t = cell->getColumn();
                auto& p = cell->value.second;
                if (p != m_defaultPortRow[s]) {
                    m_compressedRules.emplace_back(s, t, cell->value.first, p);
                }
            }
            // Add wildcard if not dominating wildcard port
            if (m_defaultPortRow[s] != Rule::Wildstar
                && m_defaultPortRow[s] != defaultPortRow) {
                m_compressedRules.emplace_back(s, Rule::Wildstar, Rule::Wildstar,
                    m_defaultPortRow[s]);
            }
        }
        m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar,
            Rule::Wildstar, defaultPortRow);
        m_defaultPort = defaultPortRow;
        m_compressionType = CompressionType::LINE;
        m_compressed = true;
    }
    m_hasChangedSinceLastComp = false;
#ifdef CHRONO
    m_compressTimes.emplace_back(
        getActualNbRules(),
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count(),
        m_nbCompressedRules);
#endif
}

std::size_t WildcardRules::findCompressedPort(const std::size_t _s, const std::size_t _t) const {
    if (m_compressionType == CompressionType::COLUMN && m_defaultPortCol[_t] != Rule::Wildstar) {
        return m_defaultPortCol[_t];
    }
    if (m_compressionType == CompressionType::LINE && m_defaultPortRow[_s] != Rule::Wildstar) {
        return m_defaultPortRow[_s];
    }
    if (m_compressed) {
        return m_defaultPort;
    }
    return Rule::Wildstar;
}

Rule WildcardRules::findCompressedRule(const std::size_t _s, const std::size_t _t) const {
    if (m_compressionType == CompressionType::COLUMN && m_defaultPortCol[_t] != Rule::Wildstar) {
        return { Rule::Wildstar, _t, Rule::Wildstar, m_defaultPortCol[_t] };
    }
    if (m_compressionType == CompressionType::LINE && m_defaultPortRow[_s] != Rule::Wildstar) {
        return { _s, Rule::Wildstar, Rule::Wildstar, m_defaultPortRow[_s] };
    }
    return {Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, m_defaultPort};
}

Rule WildcardRules::findRule(const std::size_t _s, const std::size_t _t) const {
    if (m_compressed) {
        for (const auto rule : m_compressedRules) {
            if ((rule.s == _s || rule.s == Rule::Wildstar) && (rule.t == _t || rule.t == Rule::Wildstar)) {
                return rule;
            }
        }
    }
    auto& value = m_routingMatrix.find(_s, _t)->value;
    return Rule{_s, _t, value.first, value.second};
}