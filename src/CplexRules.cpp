#include "CplexRules.hpp"

#include <algorithm>
#include <iostream>
#include <tuple>

#include <map>
#include <set>
#include <utility>

#include <DiGraph.hpp>
#include <cstring>
#include <ilcplex/ilocplex.h>
#include <vector>

CplexRules::CplexRules(const std::size_t _nbServers, const std::size_t _maxSize,
    const std::size_t _order)
    : CompressedRules(_nbServers, _maxSize, _order) {}

Rule CplexRules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop,
    const std::size_t _nextHop) {
    auto rule = Rules::addRule(_s, _t, _lastHop, _nextHop);

    if (m_compressed) {
        if (_nextHop != findCompressedPort(_s, _t)) {
            return m_compressedRules.emplace_front(_s, _t, _lastHop, _nextHop);
        }
        return {Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, Rule::Wildstar};
    }
    return rule;
}

void CplexRules::compress() {
    IloEnv env;
    IloModel model(env);

    std::set<std::size_t> sources, destinations, ports;
    std::vector<std::set<std::size_t>> portCol(m_routingMatrix.rowSize()),
        portRow(m_routingMatrix.rowSize());

    std::map<std::tuple<std::size_t, std::size_t, std::size_t>, IloBoolVar> r;
    std::map<std::pair<std::size_t, std::size_t>, IloBoolVar> gs;
    std::map<std::pair<std::size_t, std::size_t>, IloBoolVar> gd;
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            auto t = cell->getColumn();
            auto& p = cell->value.second;

            sources.emplace(s);
            destinations.emplace(t);
            ports.emplace(p);

            portCol[t].emplace(p);
            portRow[s].emplace(p);

            r.emplace(std::make_tuple(s, t, p),
                IloBoolVar(env, std::string("r(" + std::to_string(s) + "," + std::to_string(t) + "," + std::to_string(p) + ")")
                                    .c_str()));
            gs.emplace(std::make_pair(s, p),
                IloBoolVar(env, std::string("gs(" + std::to_string(s) + "," + std::to_string(p) + ")")
                                    .c_str()));
            gd.emplace(std::make_pair(t, p),
                IloBoolVar(env, std::string("gd(" + std::to_string(t) + "," + std::to_string(p) + ")")
                                    .c_str()));
        }
    }

    std::map<std::size_t, IloBoolVar> dp;
    for (auto& p : ports) {
        dp.emplace(
            p,
            IloBoolVar(env, std::string("d[" + std::to_string(p) + "]").c_str()));
    }

    // Objective function
    IloExpr objectiveFunc(env);
    for (auto& v : r) {
        objectiveFunc += v.second;
    }

    for (auto& v : gd) {
        objectiveFunc += v.second;
    }
    for (auto& v : gs) {
        objectiveFunc += v.second;
    }
    objectiveFunc += 1;

    model.add(IloMinimize(env, objectiveFunc));

    // One default port
    IloExpr sumPortExpr(env);
    for (auto& pair : dp) {
        sumPortExpr += pair.second;
    }
    model.add(sumPortExpr == 1);

    // One default port per source
    for (auto s : sources) {
        IloExpr defaultSrcExpr(env);
        for (auto p : portRow[s]) {
            auto ite = gs.find(std::make_pair(s, p));
            if (ite != gs.end()) {
                defaultSrcExpr += ite->second;
            }
        }
        model.add(defaultSrcExpr <= 1);
    }

    // One default port per destination
    for (auto& t : destinations) {
        IloExpr defaultDestExpr(env);
        for (auto& p : portCol[t]) {
            auto ite = gd.find(std::make_pair(t, p));
            if (ite != gd.end()) {
                defaultDestExpr += ite->second;
            }
        }
        model.add(defaultDestExpr <= 1);
    }

    // At least one rule for an original rule in the forwarding table
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            auto t = cell->getColumn();
            auto& p = cell->value.second;
            model.add(r[std::make_tuple(s, t, p)] + gs[std::make_pair(s, p)] + gd[std::make_pair(t, p)] + dp[p] >= 1);
        }
    }

    std::map<std::tuple<std::size_t, std::size_t>, IloBoolVar> order;

    // Order inverse
    for (auto& s : sources) {
        for (auto& t : destinations) {
            order.emplace(std::make_tuple(s, t),
                IloBoolVar(env, std::string("o(" + std::to_string(s) + ", " + std::to_string(t) + ")")
                                    .c_str()));
        }
    }

    // Correct order
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            auto t = cell->getColumn();
            auto& p1 = cell->value.second;
            // Forall (s, t) -> p1
            for (auto& p2 : portCol[t]) {
                if (p1 != p2) {
                    auto iteGD = gd.find(std::make_pair(t, p2));

                    if (iteGD == gs.end()) {
                        std::cerr << "Error ->" << __FILE__ << ":" << __LINE__ << std::endl;
                    }
                    model.add(r[std::make_tuple(s, t, p1)] + order[std::make_tuple(s, t)] >= iteGD->second);
                    model.add(r[std::make_tuple(s, t, p1)] + gs[std::make_pair(s, p1)] >= iteGD->second);
                }
            }

            for (auto& p2 : portRow[s]) {
                if (p1 != p2) {
                    auto iteGS = gs.find(std::make_pair(s, p2));
                    if (iteGS == gs.end()) {
                        std::cerr << "Error ->" << __FILE__ << ":" << __LINE__ << std::endl;
                    }
                    model.add(r[std::make_tuple(s, t, p1)] + 1 - order[std::make_tuple(s, t)] >= iteGS->second);
                    model.add(r[std::make_tuple(s, t, p1)] + gd[std::make_pair(t, p1)] >= iteGS->second);
                }
            }
        }
    }

    // Square cycle

    for (auto& s1 : sources) {
        for (auto& s2 : sources) {
            if (s1 != s2) {
                for (auto& t1 : destinations) {
                    for (auto& t2 : destinations) {
                        if (t1 != t2) {
                            model.add(1 <= order[std::make_tuple(s1, t1)] - order[std::make_tuple(s2, t1)] + order[std::make_tuple(s2, t2)] - order[std::make_tuple(s1, t2)] + 2 <= 3);
                        }
                    }
                }
            }
        }
    }

    IloCplex cplex(model);
    cplex.setOut(env.getNullStream());
    cplex.setParam(IloCplex::TiLim, 60 * 60);

    try {
        if (cplex.solve() == IloTrue) {
            std::vector<std::pair<std::size_t, std::size_t>> sRules;
            for (const auto& ps : gs) {
                if (cplex.getValue(ps.second) != 0.0) {
                    sRules.push_back(ps.first);
                }
            }

            std::vector<std::pair<std::size_t, std::size_t>> dRules;
            for (auto& ps : gd) {
                if (cplex.getValue(ps.second) != 0.0) {
                    dRules.push_back(ps.first);
                }
            }

            if (sRules.size() + dRules.size() > 0) {
                DiGraph g(sRules.size() + dRules.size());

                std::size_t i = 0;
                for (auto& sRule : sRules) {
                    std::size_t j = 0;
                    for (auto& dRule : dRules) {
                        if (cplex.getValue(order[std::make_tuple(sRule.first, dRule.first)]) != 0.0) {
                            g.addEdge(i, sRules.size() + j, 0);
                        } else {
                            g.addEdge(sRules.size() + j, i, 0);
                        }
                        ++j;
                    }
                    ++i;
                }

                const auto ruleOrder = getTopologicalOrder(g);

                m_compressedRules.clear();
                for (auto& pair : r) {
                    if (cplex.getValue(pair.second) > 0) {
                        m_compressedRules.emplace_back(std::get<0>(pair.first),
                            std::get<1>(pair.first), Rule::Wildstar,
                            std::get<2>(pair.first));
                    }
                }

                for (const auto& rule : ruleOrder) {
                    if (rule < sRules.size()) {
                        const auto& pair = sRules[rule];
                        m_compressedRules.emplace_back(pair.first, Rule::Wildstar, Rule::Wildstar, pair.second);
                    } else {
                        const auto& pair = dRules[rule - sRules.size()];
                        m_compressedRules.emplace_back(Rule::Wildstar, pair.first, Rule::Wildstar, pair.second);
                    }
                }
            }

            for (auto& port : dp) {
                if (cplex.getValue(port.second) > 0) {
                    m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, port.first);
                    break;
                }
            }
            m_compressed = true;
            env.end();
        }
    } catch (const IloCplex::Exception& e) {
        std::cout << e.getMessage() << std::endl;
    }
}

std::size_t CplexRules::findCompressedPort(const std::size_t _s, const std::size_t _t) const {
    // std::cout << __FILE__ << ":"<< __LINE__ <<std::endl;
    for (const auto& rule : m_compressedRules) {
        const std::size_t s = rule.s, t = rule.t;
        if ((s == Rule::Wildstar || s == _s) && (t == Rule::Wildstar || t == _t)) {
            return rule.nh;
        }
    }
    return -1;
}