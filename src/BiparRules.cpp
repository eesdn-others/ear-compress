#include "BiparRules.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <utility>

#include "BinaryHeap.hpp"
#include "utility.hpp"

#ifdef CHRONO
#include <chrono>
#endif

BiparRules::BiparRules(const std::size_t _nbServers, const std::size_t _maxSize,
    const std::size_t _order)
    : CompressedRules(_nbServers, _maxSize, _order) {}

Rule BiparRules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop,
    const std::size_t _nextHop) {
    auto rule = Rules::addRule(_s, _t, _lastHop, _nextHop);

    if (isCompressed()) {
        if (_nextHop != findCompressedPort(_s, _t)) { // Only add the rule if no wildcard rules already exists
            m_compressedRules.emplace_front(_s, _t, _lastHop, _nextHop);
            // m_hasChanged = true;
            return m_compressedRules.front();
        }
        return findCompressedRule(_s, _t);
    }
    return rule;
}

bool BiparRules::removeRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    Rules::removeRule(_s, _t, _lastHop, _nextHop);
    if (m_nbRules == 0) {
        m_compressedRules.clear();
    }

    auto toRemoveIte = std::find_if(m_compressedRules.begin(), m_compressedRules.end(),
        [&](const Rule& __r) {
            return __r.s == _s && __r.t == _t;
        });
    if (toRemoveIte != m_compressedRules.end()) {
        m_compressedRules.erase(toRemoveIte);
        return true;
    }
    return true;
}

void BiparRules::compress() {
#ifdef CHRONO
    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();
#endif
    if (m_nbRules == 0) {

        return;
    }
    // Check default port per row
    std::size_t rowToComp = 0;
    std::vector<std::size_t> defaultPortRow(m_nbServers, Rule::Wildstar);
    std::vector<std::size_t> nbRulesRow(m_nbServers, 0);
    for (std::size_t s = 0; s < m_nbServers; ++s) {
        if (m_routingMatrix.begin_row(s) != nullptr) { // @TODO: Implement row iterator for LinkedMatrix
            ++rowToComp;
            std::vector<std::size_t> count(m_order, 0);
            for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                 cell = cell->getNextOnRow()) {
                ++count[cell->value.second];
                ++nbRulesRow[s];
            }
            std::size_t port = 0;
            for (std::size_t i = 1; i < count.size(); ++i) {
                if (count[port] < count[i]) {
                    port = i;
                }
            }
            defaultPortRow[s] = port;
        }
    }

    // Check default port per column
    std::vector<std::size_t> defaultPortCol(m_nbServers, Rule::Wildstar);
    std::vector<std::size_t> nbRulesCol(m_nbServers, 0);
    std::size_t colToComp = 0;
    for (std::size_t t = 0; t < m_nbServers; ++t) {
        if (m_routingMatrix.begin_column(t) != nullptr) {
            ++colToComp;
            std::vector<std::size_t> count(m_order, 0);
            for (auto cell = m_routingMatrix.begin_column(t); cell != nullptr;
                 cell = cell->getNextOnCol()) {
                ++count[cell->value.second];
                ++nbRulesCol[t];
            }
            std::size_t port = 0;
            for (std::size_t i = 1; i < count.size(); ++i) {
                if (count[port] < count[i]) {
                    port = i;
                }
            }
            defaultPortCol[t] = port;
        }
    }

    std::vector<std::size_t> inDegree(m_nbServers * 2, 0),
        outDegree(m_nbServers * 2, 0);
    std::vector<bool> compressed(m_nbServers * 2, false);
    std::vector<std::vector<std::size_t>> depFrom(
        m_nbServers * 2); // depFrom[i] := vector of column/row i depends from
    std::vector<std::vector<std::size_t>> shareWith(
        m_nbServers * 2); // depFrom[i] := vector of column/row i shares compressable with
    std::vector<std::vector<std::size_t>> slaveOf(
        m_nbServers * 2); // slaveOf[i] := vector of column/row that can't be compressed before i
    std::vector<bool> isLeaf(m_nbServers * 2, true);

    m_compressedRules.clear();
    for (std::size_t s = 0; s < m_nbServers; ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            std::size_t t = cell->getColumn();
            std::size_t p = cell->value.second;

            if (p != defaultPortRow[s] && p != defaultPortCol[t]) {
                m_compressedRules.emplace_back(s, t, Rule::Wildstar, p);
                --nbRulesCol[t];
                --nbRulesRow[s];
            } else {
                if (p != defaultPortRow[s]) { // Col t must be compressed before row s
                    ++outDegree[s];
                    depFrom[s].push_back(m_nbServers + t);

                    ++inDegree[m_nbServers + t];
                    slaveOf[m_nbServers + t].push_back(s);
                    // --nbRulesRow[s];
                } else if (p != defaultPortCol[t]) { // Col t must be compressed before row s
                    ++outDegree[m_nbServers + t];
                    depFrom[m_nbServers + t].push_back(s);

                    ++inDegree[s];
                    slaveOf[s].push_back(m_nbServers + t);
                    // --nbRulesCol[t];
                } else {
                    shareWith[s].push_back(m_nbServers + t);
                }
            }
        }
    }

    std::vector<std::vector<std::size_t>> parent(m_nbServers * 2);

    using Heap = BinaryHeap<std::size_t, std::function<bool(std::size_t, std::size_t)>>;
    Heap heap(
        m_nbServers * 2, [&](const std::size_t u, const std::size_t v) {
            return (outDegree[u] == 0 && outDegree[v] > 0) ||                                                                                                                                                                     // u has no dependencies but v does
                   (outDegree[u] == 0 && outDegree[v] == 0 && inDegree[u] > inDegree[v]) ||                                                                                                                                       // Both u and v have no
                                                                                                                                                                                                                                  // dependencies but more nodes
                                                                                                                                                                                                                                  // depends from u
                   (outDegree[u] == 0 && outDegree[v] == 0 && inDegree[u] == inDegree[v] && (u < m_nbServers ? nbRulesRow[u] : nbRulesCol[u - m_nbServers]) > (v < m_nbServers ? nbRulesRow[v] : nbRulesCol[v - m_nbServers])) || // Both u and v have
                                                                                                                                                                                                                                  // no dependencies
                                                                                                                                                                                                                                  // and same # nodes
                                                                                                                                                                                                                                  // depending from
                                                                                                                                                                                                                                  // them
                   // (outDegree[u] == 0 && outDegree[v] == 0 && inDegree[u] ==
                   // inDegree[v] && parentSameDP[u] > parentSameDP[v]) || // Both u
                   // and v have no dependencies and same # nodes depending from
                   // them
                   (outDegree[u] == outDegree[v] && inDegree[u] > inDegree[v]) || (outDegree[u] == outDegree[v] && inDegree[u] == inDegree[v] && (u < m_nbServers ? nbRulesRow[u] : nbRulesCol[u - m_nbServers]) > (v < m_nbServers ? nbRulesRow[v] : nbRulesCol[v - m_nbServers])) || (outDegree[u] < outDegree[v]);
        });
    std::vector<Heap::Handle*> handles(
        m_nbServers * 2, nullptr);

    for (std::size_t i = 0; i < m_nbServers * 2; ++i) {
        if (i < m_nbServers) {
            if (m_routingMatrix.begin_row(i) != nullptr) { // If rules from i
                handles[i] = heap.push(i);
            }
        } else {
            if (m_routingMatrix.begin_column(i - m_nbServers) != nullptr) { // If rules to i
                handles[i] = heap.push(i);
            }
        }
    }

    std::vector<std::list<Rule>> rules(m_nbServers * 2);
    while (!heap.empty()) {
        const auto i = heap.top();
        heap.pop();
        bool empty =
            i < m_nbServers ? nbRulesRow[i] == 0 : nbRulesCol[i - m_nbServers] == 0;
        if (!compressed[i] && !empty) {
            compressed[i] = true;
            for (auto& s : slaveOf[i]) {
                --outDegree[s];
                heap.decrease(handles[s]);
            }
            // Add parent that are compressed (parent[i] = {j in depFrom[i] |
            // compressed[j])
            for (auto& p : depFrom[i]) {
                if (compressed[p]) {
                    isLeaf[p] = false;
                    parent[i].push_back(p);
                }
            }

            if (outDegree[i] == 0) {
                if (i < m_nbServers) {
                    int s = i;
                    --rowToComp;
                    // std::cout << "Compress row " << s << std::endl;
                    for (auto j : shareWith[i]) {
                        int t = j - m_nbServers;
                        --nbRulesCol[t];
                    }
                    rules[i].emplace_back(s, Rule::Wildstar, Rule::Wildstar, defaultPortRow[s]);
                } else {
                    int t = i - m_nbServers;
                    --colToComp;
                    // std::cout << "Compress column " << t << std::endl;
                    for (auto s : shareWith[i]) {
                        --nbRulesRow[s];
                    }
                    rules[i].emplace_back(Rule::Wildstar, t, Rule::Wildstar, defaultPortCol[t]);
                }
            } else {
                if (i < m_nbServers) {
                    int s = i;
                    --rowToComp;
                    // std::cout << "Compress row " << s << std::endl;
                    for (auto j : shareWith[i]) {
                        int t = j - m_nbServers;
                        --nbRulesCol[t];
                    }

                    for (auto& j : depFrom[i]) {
                        auto t = j - m_nbServers;
                        if (!compressed[j]) {
                            auto p = m_routingMatrix.find(s, t)->value.second;
                            rules[i].emplace_back(s, t, Rule::Wildstar, p);
                            --nbRulesCol[t];
                        }
                    }
                    rules[i].emplace_back(s, Rule::Wildstar, Rule::Wildstar, defaultPortRow[s]);
                } else {
                    int t = i - m_nbServers;
                    --colToComp;
                    // std::cout << "Compress column " << t << std::endl;
                    for (auto s : shareWith[i]) {
                        --nbRulesRow[s];
                    }

                    for (auto& s : depFrom[i]) {
                        if (!compressed[s]) {
                            auto p = m_routingMatrix.find(s, t)->value.second;
                            rules[i].emplace_back(s, t, Rule::Wildstar, p);
                            --nbRulesRow[s];
                        }
                    }
                    rules[i].emplace_back(Rule::Wildstar, t, Rule::Wildstar, defaultPortCol[t]);
                }
            }
        }
    }

    std::vector<int> nbPort(m_nbServers);
    std::vector<bool> aggregatable(2 * m_nbServers, false);
    // std::cout << isLeaf << std::endl;
    // Determine default port
    for (std::size_t i = 0; i < m_nbServers * 2; ++i) {
        if (isLeaf[i] && compressed[i]) {
            int dp =
                i < m_nbServers ? defaultPortRow[i] : defaultPortCol[i - m_nbServers];
            ++nbPort[dp];
            aggregatable[i] = true;
            std::list<int> l{i};
            while (!l.empty()) {
                auto n = l.front();
                l.pop_front();
                for (auto& j : parent[n]) {
                    int p = j < m_nbServers ? defaultPortRow[j]
                                            : defaultPortCol[j - m_nbServers];
                    if (p == dp) {
                        bool allIn = true;
                        // Check all sons are aggregatable
                        for (auto& son : slaveOf[j]) {
                            int pSon = son < m_nbServers ? defaultPortRow[son]
                                                         : defaultPortCol[son - m_nbServers];
                            if (compressed[son] && !aggregatable[son] && pSon == dp) {
                                allIn = false;
                                break;
                            }
                        }
                        if (allIn) {
                            l.emplace_back(j);
                            ++nbPort[dp];
                            aggregatable[j] = true;
                        }
                    }
                }
            }
        }
    }

    // std::cout << nbPort << std::endl;
    // std::cout << aggregatable << std::endl;

    std::size_t dp = 0;
    for (std::size_t i = 1; i < m_nbServers; ++i) {
        if (nbPort[i] > nbPort[dp]) {
            dp = i;
        }
    }

    std::vector<bool> in(2 * m_nbServers, false);
    for (std::size_t p = 0; p < m_nbServers * 2; ++p) {
        if (parent[p].empty() && compressed[p]) {
            std::list<Rule> rulesvector;
            std::list<std::size_t> l{p};
            in[p] = true;
            while (!l.empty()) {
                auto n = l.front();
                l.pop_front();
                // std::cout << "add rules of " << n << std::endl;
                std::size_t port = n < m_nbServers ? defaultPortRow[n]
                                                   : defaultPortCol[n - m_nbServers];
                if (!aggregatable[n] || dp != port) {
                    rulesvector.splice(rulesvector.end(), rules[n]);
                } else {
                    rulesvector.splice(rulesvector.end(), rules[n], rules[n].begin(),
                        std::prev(rules[n].end()));
                }
                // std::cout << "sons: ";
                for (auto& j : slaveOf[n]) {
                    if (compressed[j] && !in[j]) {
                        // Check all parent in
                        bool allIn = true;
                        for (auto& pp : parent[j]) {
                            if (compressed[pp] && !in[pp]) {
                                allIn = false;
                                break;
                            }
                        }
                        if (allIn) {
                            l.push_back(j);
                            in[j] = true;
                            // std::cout << j << ", ";
                        }
                    }
                }
                // std::cout << std::endl;
                // for(auto& r : rulesvector) {
                // std::cout << r << ", ";
                // }
                // std::cout << std::endl;
            }
            m_compressedRules.splice(m_compressedRules.end(), rulesvector);
            // for(auto& r : m_compressedRules) {
            // std::cout << r << ", ";
            // }
            // std::cout << std::endl;
        }
    }

    // DOT

    std::ofstream ofs("arbo.dot");
    ofs << "digraph G {" << std::endl;
    for (std::size_t i = 0; i < 2 * m_nbServers; ++i) {
        if (aggregatable[i]) {
            ofs << "n" << i << "_"
                << (i < m_nbServers ? defaultPortRow[i]
                                    : defaultPortCol[i - m_nbServers])
                << " [color=blue];" << std::endl;
        }
        for (auto& j : slaveOf[i]) {
            if (compressed[j]) {
                ofs << "n" << i << "_"
                    << (i < m_nbServers ? defaultPortRow[i]
                                        : defaultPortCol[i - m_nbServers])
                    << " -> "
                    << "n" << j << "_"
                    << (j < m_nbServers ? defaultPortRow[j]
                                        : defaultPortCol[j - m_nbServers])
                    << ';' << std::endl;
            }
        }
    }
    ofs << "}" << std::endl;

    m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, dp);

    m_compressed = true;
#ifdef CHRONO
    m_compressTimes.emplace_back(
        getActualNbRules(),
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count(),
        m_compressedRules.size());
#endif
}

std::size_t BiparRules::findCompressedPort(const std::size_t _s, const std::size_t _t) const {
    return findCompressedPortInList(_s, _t);
}

Rule BiparRules::findCompressedRule(const std::size_t _s, const std::size_t _t) const {
    return findRule(_s, _t);
}

Rule BiparRules::findRule(const std::size_t _s, const std::size_t _t) const {
    if (m_compressed) {
        for (const auto& rule : m_compressedRules) {
            if ((rule.s == _s || rule.s == Rule::Wildstar) && (rule.t == _t || rule.t == Rule::Wildstar)) {
                return rule;
            }
        }
    }
    const auto& value = m_routingMatrix.find(_s, _t)->value;
    return {_s, _t, value.first, value.second};
}