#include "CompressedRules.hpp"

CompressedRules::CompressedRules(std::size_t _order, std::size_t _maxSize, std::size_t _outDegree)
    : Rules(_order, _maxSize, _outDegree) {
}

Rule CompressedRules::topNonStar(const std::size_t _s) const {
    for (const auto& rule : m_compressedRules) {
        if (rule.s != _s && rule.s != Rule::Wildstar && rule.t != Rule::Wildstar) {
            return rule;
        }
    }
    return {Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, Rule::Wildstar};
}

/**
* \brief Check if the compressesd table has the same semantics as the original table
* Complexity: O(n_s * n_t), where n_s is the number of sources and n_t is the number of destination 
*/
bool CompressedRules::isCompressionValid() const {
    if (!m_compressed) {
        return true;
    }

    bool valid = true;
    for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
        for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            std::size_t t = cell->getColumn();
            std::size_t p = cell->value.second;
            std::size_t inListPort = findCompressedPortInList(s, t);
            if (inListPort != p) {
                std::cout << '(' << s << ", " << t << ") :" << inListPort << " instead of " << p
                          << std::endl;
                valid = false;
            }
        }
    }
    return valid;
}

std::size_t CompressedRules::getNbRules() const {
    return m_compressed ? m_compressedRules.size() : m_nbRules;
}

bool CompressedRules::isCompressed() const {
    return m_compressed;
}

bool CompressedRules::isCompressable() const {
    return true;
}

std::size_t CompressedRules::getActualNbRules() const {
    return m_nbRules;
}

const std::list<Rule>& CompressedRules::getCompressedRules() const {
    return m_compressedRules;
}

std::size_t CompressedRules::getNbCompressedRules() const {
    return m_compressedRules.size();
}

bool CompressedRules::removeRule(std::size_t _s, std::size_t _t, std::size_t _lastHop, std::size_t _nextHop) {
    Rules::removeRule(_s, _t, _lastHop, _nextHop);
    auto toRemoveIte = std::find_if(m_compressedRules.begin(), m_compressedRules.end(),
        [&](const Rule& __r) {
            return __r.s == _s && __r.t == _t;
        });
    if (toRemoveIte != m_compressedRules.end()) {
        m_compressedRules.erase(toRemoveIte);
        
        return true;
    }
    return false;
}

std::size_t CompressedRules::findCompressedPortInList(const std::size_t _s, const std::size_t _t) const {
    for (const auto& rule : m_compressedRules) {
        if ((rule.s == Rule::Wildstar || rule.s == _s) && (rule.t == Rule::Wildstar || rule.t == _t)) {
            return rule.nh;
        }
    }
    return Rule::Wildstar;
}

std::list<Rule> CompressedRules::getRules() const {
    if (m_compressed) {
        return getCompressedRules();
    }
    return Rules::getRules();
}

bool CompressedRules::canAddRule(const std::size_t _s, const std::size_t _t, const std::size_t _p) const {
    return !this->isFull() or this->findCompressedPort(_s, _t) == _p;
}

std::ostream& operator<<(std::ostream& _out, const CompressedRules& _rules) {
    for (std::size_t s = 0; s < _rules.m_routingMatrix.rowSize(); ++s) {
        _out << '[';
        for (auto cell = _rules.m_routingMatrix.begin_row(s); cell != nullptr;
             cell = cell->getNextOnRow()) {
            _out << '(' << cell->getRow() << ", " << cell->getColumn() << ") ->"
                 << cell->value.second << '\t';
        }
        _out << ']' << std::endl;
    }
    for (const auto& rule : _rules.m_compressedRules) {
        _out << rule << ", ";
    }
    return _out;
}