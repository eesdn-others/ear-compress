#include "Network.hpp"

Network::Network(const std::vector<jlong>& _vect)
    : DiGraph(_vect.size())
    , m_nodesToId(_vect)
    , m_idToNodes()
    , m_ports(_vect.size() * _vect.size(), std::make_pair(-1, -1)) {
    // std::cout << _vect.size() << std::endl;
    for (int i = 0; i < (int)_vect.size(); ++i) {
        // std::cout << "m_idToNodes["<<_vect[i]<<"] = "<<i<<std::endl;
        m_idToNodes[_vect[i]] = i;
    }
}

void Network::addLink(const int64_t _srcNodeID, const int64_t _dstNodeID, const short _srcPort,
    const short _dstPort, const double _capacity) {
    int u = m_idToNodes[_srcNodeID];
    int v = m_idToNodes[_dstNodeID];

    DiGraph::addEdge(u, v, _capacity);
    auto& pair = m_ports[u * this->getOrder() + v];
    pair.first = _srcPort;
    pair.second = _dstPort;
}

void Network::deleteLink(int64_t _u, int64_t _v) {
    DiGraph::removeEdge(_u, _v);
}