#include "DefaultPortRules.hpp"

#include <algorithm>
#include <iostream>
#include <tuple>

DefaultPortRules::DefaultPortRules(const std::size_t _nbServers, const std::size_t _maxSize, const std::size_t _order)
    : CompressedRules(_nbServers, _maxSize, _order) {}

Rule DefaultPortRules::addRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    // std::cout << __FILE__ << ":"<< __LINE__ <<std::endl;
    const auto rule = Rules::addRule(_s, _t, _lastHop, _nextHop);

    if (m_compressed) {
        if (_nextHop != findCompressedPort(_s, _t)) {
            m_compressedRules.emplace_front(_s, _t, _lastHop, _nextHop);
            return m_compressedRules.front();
        }
        return {Rule::Wildstar, Rule::Wildstar, Rule::Wildstar, Rule::Wildstar};
    }
    return rule;
}

bool DefaultPortRules::removeRule(const std::size_t _s, const std::size_t _t, const std::size_t _lastHop, const std::size_t _nextHop) {
    if (m_nbRules == 0) {
        m_defaultPort = Rule::Wildstar;
    }
    return CompressedRules::removeRule(_s, _t, _lastHop, _nextHop);
}

void DefaultPortRules::compress() {
    m_compressedRules.clear();
    if (m_nbRules > 0) {
        // Count dominating port
        m_defaultPort = getMaxOccuringPort();
        for (std::size_t s = 0; s < m_routingMatrix.rowSize(); ++s) {
            for (auto cell = m_routingMatrix.begin_row(s); cell != nullptr;
                 cell = cell->getNextOnRow()) {
                const auto t = cell->getColumn();
                const std::size_t p = cell->value.second;
                if (p != m_defaultPort) {
                    m_compressedRules.emplace_back(s, t, cell->value.first, p);
                }
            }
        }

        m_compressedRules.emplace_back(Rule::Wildstar, Rule::Wildstar,
            Rule::Wildstar, m_defaultPort);
        m_compressed = true;
    }
}

std::size_t DefaultPortRules::findCompressedPort(std::size_t /*_s*/, std::size_t /*_t*/) const {
    return m_defaultPort;
}