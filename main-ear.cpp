#include <iostream>

#include "tclap/CmdLine.hpp"
#include "EarRouting.hpp"
#include "BiparRules.hpp"
#include "CplexRules.hpp"
#include "DefaultPortRules.hpp"
#include "DiGraph.hpp"
#include "MostSaveRulesB.hpp"
#include "RoutingHeuristic.hpp"
#include "WildcardRules.hpp"
#include "MyTimer.hpp"

struct Param {
    std::string compModule;
    std::string routingModule;
    std::string energyModule;
    std::string name;
    double factor;
    std::size_t numberRules;
    std::size_t energyCons;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the EARC problem", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> cCons(
        {"WildcardRules", "HeurB", "HeurSB", "DefaultPortRules", "CplexRules"});
    TCLAP::ValueArg<std::string> compModule("m", "comp",
        "Specify the compression module",
        false, "WildcardRules", &cCons);
    cmd.add(&compModule);

    TCLAP::ValuesConstraint<std::string> rCons(
        {"full", "path", "noCompress", "atTheEnd", "forceAll"});
    TCLAP::ValueArg<std::string> routingModule(
        "r", "rout", "Specify the routing module", false, "path", &rCons);
    cmd.add(&routingModule);

    TCLAP::ValuesConstraint<std::string> nCons(
        {"dumb", "metric", "metricPlusOne", "metricPlusOneCapa",
            "metricPlusOneRules", "metricPlusOne75Rules", "metricPlusOne75Capa"});
    TCLAP::ValueArg<std::string> energyModule("n", "rout",
        "Specify the routing module", false,
        "metricPlusOne", &nCons);
    cmd.add(&energyModule);

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor(
        "d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> numberRules(
        "l", "numberRules", "Specify the number of rules maximum per switch",
        false, 750, "int");
    cmd.add(&numberRules);

    TCLAP::ValueArg<int> energyCons(
        "e", "energyCons", "Specify the index for the energy consumption file",
        false, 1, "int");
    cmd.add(&energyCons);

    cmd.parse(argc, argv);

    return {compModule.getValue(), routingModule.getValue(),
        energyModule.getValue(), networkName.getValue(),
        demandFactor.getValue(), numberRules.getValue(),
        energyCons.getValue()};
}

template <typename Rules>
using HeurisitcFunction = EarRouting<Rules> (*)(const Instance&);

template <typename Rules>
HeurisitcFunction<Rules>
getHeuristic(const std::string& heuristicName,
    const std::string& weightFunctionName) {
#define findHeuristic(name, func)                                                                \
    if (heuristicName == #name) {                                                                \
        if (weightFunctionName == "dumb") {                                                      \
            return &func<Rules, typename EarRouting<Rules>::DumbWeight>;                         \
        } else if (weightFunctionName == "metric") {                                             \
            return &func<Rules, typename EarRouting<Rules>::template MetricWeight<1, 1>>;        \
        } else if (weightFunctionName == "metricPlusOne") {                                      \
            return &func<Rules, typename EarRouting<Rules>::template MetricPlusOneWeight<1, 1>>; \
        } else if (weightFunctionName == "metricPlusOneCapa") {                                  \
            return &func<Rules, typename EarRouting<Rules>::template MetricWeight<1, 0>>;        \
        } else if (weightFunctionName == "metricPlusOneRules") {                                 \
            return &func<Rules, typename EarRouting<Rules>::template MetricWeight<0, 1>>;        \
        } else if (weightFunctionName == "metricPlusOne75Rules") {                               \
            return &func<Rules, typename EarRouting<Rules>::template MetricWeight<1, 3>>;        \
        } else if (weightFunctionName == "metricPlusOne75Capa") {                                \
            return &func<Rules, typename EarRouting<Rules>::template MetricWeight<3, 1>>;        \
        }                                                                                        \
    }
    findHeuristic(full, runFullRestart) else findHeuristic(path, runPathRestart) else findHeuristic(noCompress, runNoCompress) else findHeuristic(atTheEnd, runAtTheEnd) else findHeuristic(forceAll, runForceDemands);
    return nullptr;
}

template <typename Rules>
void launch(const Instance& _inst,
    const std::string& name, const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    std::cout << "Name : " << name << '\n'
              << "Heurisitc : " << heuristicName << '\n'
              << "Rules : " << _rulesName << '\n'
              << "Weight : " << weightFunctionName << '\n'
              << "Percent: " << percent << '\n'
              << "# rules: " << _inst.maxMem << '\n';

    Time timer;
    timer.start();
    const auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);
    const auto solution = run(_inst);
    const auto time = timer.get(true);

    if (solution.getNbDemands() != _inst.demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << _inst.demands.size() << ')' << '\n';
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << "\t" << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem() << '\n';

        for (Graph::Node i = 0; i < _inst.topology.getOrder(); ++i) {
            if (solution.isSwitch(i)) {
                const auto& rule = solution.getRules(i);
                if (!rule.isCompressionValid()) {
                    std::cout << "Compression not valid for " << i << '\n';
                }
                if(rule.getNbRules() > _inst.maxMem) {
                	std::cerr << i << " is above the max limit.\n";
                }
                std::cout << i << ": " << rule.getNbCompressedRules() << "/"
                          << rule.getActualNbRules() << '\t'
                          << 100 * (rule.getNbCompressedRules() / (double)rule.getActualNbRules())
                          << '\t' << solution.getDensity(i) << '\t'
                          << solution.getResidual().getOutDegree(i) << '\t';
                std::cout << '\n';
            }
        }

#ifdef DYNAMIC
        std::string resultsFolder("results_dynamic");
#else
        std::string resultsFolder("results");
#endif
        const auto filename = "./" + resultsFolder + "/" + name + "_" + heuristicName
                              + "_" + weightFunctionName + "_" + _rulesName + "_" + std::to_string(_inst.maxMem) + "_" + std::to_string(percent) + ".txt";
        solution.saveMetrics(filename, time);
    }
}

int main(int argc, char** argv) {
    const auto params = getParams(argc, argv);

    auto demands = loadDemandsFromFile(
        std::string("./instances/" + params.name + "_demand.txt"), params.factor);
    auto [topology, nbServers, nbSwitches] = DiGraph::loadFromFile(
        std::string("./instances/" + params.name + "_topo.txt"));

    using launchFunction = void (*)(const Instance&, const std::string&,
        const std::string&, const std::string&, const std::string&, double);

    const std::map<std::string, launchFunction> whatToCall{{{"WildcardRules", &launch<WildcardRules>},
        {"HeurB", &launch<BiparRules>},
        {"HeurSB", &launch<MostSaveRulesB>},
        {"DefaultPortRules", &launch<DefaultPortRules>},
        {"CplexRules", &launch<CplexRules>}}};

    whatToCall.at(params.compModule)(Instance{topology, nbServers, nbSwitches, params.numberRules, demands},
        params.name, params.compModule, params.energyModule,
        params.routingModule, params.factor);
    return 0;
}
