
#include <utility>

#include <algorithm>
#include <iostream>
#include <tuple>

#include <chrono>
#include <map>
#include <set>
#include <utility>

#include "BiparRules.hpp"
#include "CplexRules.hpp"
#include "Matrix.hpp"
#include "MostSaveRules.hpp"
#include "MostSaveRulesB.hpp"
#include "WildcardRules.hpp"

void fromRandom(std::string compMod, int nbRow, int nbPort, double density);
void fromFile(std::string compMod, std::string filename);
template <typename Rules>
void save(const Rules& table, const std::string& _filename);

int main(int argc, char** argv) {
    std::string compMod{argv[1]};
    std::string type{argv[2]};
    if (type == "-f") {
        std::string filename{argv[3]};
        fromFile(compMod, filename);
    } else if (type == "-r") {
        int nbRow = std::stoi(argv[3]);
        int nbPort = std::stoi(argv[4]);
        double density = std::stof(argv[5]);
        fromRandom(compMod, nbRow, nbPort, density);
    }
    return 0;
}

std::pair<int, Matrix<int>> fromFile(const std::string& _filename) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    int nbRules = 0;
    int s, t, p;
    int row, column;
    std::string line;
    std::getline(ifs, line);
    // std::cout << line << std::endl;
    std::stringstream lineStream(line);

    lineStream >> row >> column;
    Matrix<int> m(row, row, -1);
    // std::cout << r << ' ' << c << std::endl;
    // std::cout << s << ", " << t << ", "<< d << std::endl;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        std::stringstream lineStream(line);
        lineStream >> s >> t >> p;
        m(s, t) = p;
        ++nbRules;
    }
    return std::make_pair(nbRules, m);
}

std::pair<int, Matrix<int>> fromRandom(int _nbRow, int _nbPort,
    double _density) {
    Matrix<int> m(_nbRow, _nbRow, -1);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);
    std::uniform_real_distribution<double> realDistr(0.0, 1.0);
    std::uniform_int_distribution<int> intDistr(0, _nbPort - 1);
    int nbRules = 0;
    for (int s = 0; s < _nbRow; ++s) {
        for (int t = 0; t < _nbRow; ++t) {
            if (realDistr(gen) <= _density) {
                m(s, t) = intDistr(gen);
                ++nbRules;
            }
        }
    }
    return std::make_pair(nbRules, m);
}

void loadTable(Rules& _r, const Matrix<int>& _m) {
    for (int i = 0; i < _m.size1(); ++i) {
        for (int j = 0; j < _m.size1(); ++j) {
            if (_m(i, j) != -1) {
                _r.addRule(i, j, -1, _m(i, j));
            }
        }
    }
}

void fromFile(std::string compMod, std::string filename) {
    std::string name = filename.substr(6, filename.size() - 12);
    std::cout << name << std::endl;

    std::pair<int, Matrix<int>> pair = fromFile(filename);
    int& nbRules = pair.first;
    Matrix<int>& oTable = pair.second;

    std::cout << "rules/file_" + compMod + "_" + name + ".results" << std::endl;
    std::ofstream ofs("rules/file_" + compMod + "_" + name + ".results",
        std::ofstream::out);
    ofs << nbRules;
    // for(int i = 0; i < oTable.size1(); ++i) {
    // 	std::cout << "[";
    // 	for(int j = 0; j < oTable.size1(); ++j) {
    // 		int p = oTable(i, j);
    // 		if(p == -1) {
    // 			std::cout << " *";
    // 		} else {
    // 			std::cout << " " << p;
    // 		}
    // 	}
    // 	std::cout << " ]" << std::endl;
    // }

    if (compMod == "Heur-LCDP" or compMod == "all" or compMod == "allHeur") {
        WildcardRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);
        table.compress();
        std::cout << "Heur-LCDP: " << table.getNbRules()
                  << ", valid: " << table.isCompressionValid() << std::endl;
        if (table.isCompressionValid()) {
            ofs << '\t' << table.getNbCompressedRules();
        } else {
            ofs << "\t -1";
        }
        for (int i = 0; i < oTable.size1(); ++i) {
            std::cout << "[";
            for (int j = 0; j < oTable.size1(); ++j) {
                int p = oTable(i, j);
                if (p == -1) {
                    std::cout << " *";
                } else {
                    std::cout << " " << p;
                }
            }
            std::cout << " ]" << std::endl;
        }
        for (auto& r : table.getCompressedRules()) {
            std::cout << r << std::endl;
        }
        // }
    }
    // if (compMod == "HeurB-LCDP" or compMod == "all" or compMod == "allHeur") {
    // 	BiparRules table ( oTable.size1(), -1,  oTable.size1());
    // 	loadTable(table, oTable);
    // 	table.compress();
    // 	std::cout << "HeurB-LCDP: " << table.getNbRules()  << ", valid: " <<
    // table.isCompressionValid() << std::endl; 	if(table.isCompressionValid()) {
    // 		ofs << '\t' << table.getNbCompressedRules();
    // 	} else {
    // 		ofs << "\t -1";
    // 		for(int i = 0; i < oTable.size1(); ++i) {
    // 			std::cout << "[";
    // 			for(int j = 0; j < oTable.size1(); ++j) {
    // 				int p = oTable(i, j);
    // 				if(p == -1) {
    // 					std::cout << " *";
    // 				} else {
    // 					std::cout << " " << p;
    // 				}
    // 			}
    // 			std::cout << " ]" << std::endl;
    // 		}
    // 	}
    // }

    ofs << "\t -1";
    // ofsTime << "\t -1";
    // if (compMod == "HeurS-LCDP" or compMod == "all" or compMod == "allHeur") {
    // 	MostSaveRules table ( oTable.size1(), -1,  oTable.size1());
    // 	loadTable(table, oTable);
    // 	table.compress();
    // 	std::cout << "HeurS-LCDP: " << table.getNbRules()  << ", valid: " <<
    // table.isCompressionValid() << std::endl; 	if(table.isCompressionValid()) {
    // 		ofs << '\t' << table.getNbCompressedRules();
    // 	} else {
    // 		ofs << "\t -1";
    // 		for(int i = 0; i < oTable.size1(); ++i) {
    // 			std::cout << "[";
    // 			for(int j = 0; j < oTable.size1(); ++j) {
    // 				int p = oTable(i, j);
    // 				if(p == -1) {
    // 					std::cout << " *";
    // 				} else {
    // 					std::cout << " " << p;
    // 				}

    // 			}
    // 			std::cout << " ]" << std::endl;
    // 		}
    // 	}
    // }
    ofs << "\t -1";
    // ofsTime << "\t -1";

    if (compMod == "HeurSB-LCDP" or compMod == "all" or compMod == "allHeur") {
        MostSaveRulesB table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);
        table.compress();
        std::cout << "HeurSB-LCDP: " << table.getNbRules()
                  << ", valid: " << table.isCompressionValid() << std::endl;
        if (table.isCompressionValid()) {
            ofs << '\t' << table.getNbCompressedRules();
        } else {
            ofs << "\t -1";
            for (int i = 0; i < oTable.size1(); ++i) {
                std::cout << "[";
                for (int j = 0; j < oTable.size1(); ++j) {
                    int p = oTable(i, j);
                    if (p == -1) {
                        std::cout << " *";
                    } else {
                        std::cout << " " << p;
                    }
                }
                std::cout << " ]" << std::endl;
            }
        }
    }

    if (compMod == "DP" or compMod == "all" or compMod == "allHeur") {
        DefaultPortRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);
        table.compress();
        std::cout << "DP: " << table.getNbRules()
                  << ", valid: " << table.isCompressionValid() << std::endl;
        if (table.isCompressionValid()) {
            ofs << '\t' << table.getNbCompressedRules();
        } else {
            ofs << "\t -1";
            for (int i = 0; i < oTable.size1(); ++i) {
                std::cout << "[";
                for (int j = 0; j < oTable.size1(); ++j) {
                    int p = oTable(i, j);
                    if (p == -1) {
                        std::cout << " *";
                    } else {
                        std::cout << " " << p;
                    }
                }
                std::cout << " ]" << std::endl;
            }
        }
    }

    if (compMod == "LP-LCDP" or compMod == "all") {
        CplexRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);
        table.compress();
        std::cout << "LP-LCDP: " << table.getNbRules()
                  << ", valid: " << table.isCompressionValid() << std::endl;
        if (table.isCompressionValid()) {
            ofs << '\t' << table.getNbCompressedRules();
        } else {
            ofs << "\t -1";
            for (int i = 0; i < oTable.size1(); ++i) {
                std::cout << "[";
                for (int j = 0; j < oTable.size1(); ++j) {
                    int p = oTable(i, j);
                    if (p == -1) {
                        std::cout << " *";
                    } else {
                        std::cout << " " << p;
                    }
                }
                std::cout << " ]" << std::endl;
            }
        }
    }

    ofs.close();
}

void fromRandom(std::string compMod, int nbRow, int nbPort, double density) {
    std::cout << "Random -> nbRow: " << nbRow << ", nbPort: " << nbPort
              << ", density: " << density << std::endl;

    std::pair<int, Matrix<int>> pair = fromRandom(nbRow, nbPort, density);
    int& nbRules = pair.first;
    Matrix<int>& oTable = pair.second;

    std::string filename = "rules/rand_" + compMod + "_" + std::to_string(nbRow) + "_" + std::to_string(nbPort) + "_" + std::to_string(density) + ".results";
    std::string filenameTime =
        "rules/rand_" + compMod + "_" + std::to_string(nbRow) + "_" + std::to_string(nbPort) + "_" + std::to_string(density) + ".time";

    std::ofstream ofs(filename, std::ofstream::out | std::ofstream::app);
    std::ofstream ofsTime(filenameTime, std::ofstream::out | std::ofstream::app);
    std::cout << "Table generated!" << std::endl;
    ofs << nbRules;
    ofsTime << nbRules;

    if (compMod == "Heur-LCDP" or compMod == "all" or compMod == "allHeur") {
        WildcardRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);

        std::chrono::high_resolution_clock::time_point t1 =
            std::chrono::high_resolution_clock::now();
        table.compress();
        int duration =
            std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
                std::chrono::high_resolution_clock::now() - t1)
                .count();

        std::cout
            << "Heur-LCDP: "
            << table.getNbRules() /*<< ", valid: " << table.isCompressionValid() */
            << std::endl;
        // if(table.isCompressionValid()) {
        ofs << '\t' << table.getNbCompressedRules();
        ofsTime << '\t' << duration;
        // } else {
        // 	ofs << "\t -1";
        // 	for(int i = 0; i < oTable.size1(); ++i) {
        // 		std::cout << "[";
        // 		for(int j = 0; j < oTable.size1(); ++j) {
        // 			int p = oTable(i, j);
        // 			if(p == -1) {
        // 				std::cout << " *";
        // 			} else {
        // 				std::cout << " " << p;
        // 			}

        // 		}
        // 		std::cout << " ]" << std::endl;
        // 	}
        // }
    }

    // if (compMod == "HeurB-LCDP" or compMod == "all" or compMod == "allHeur") {
    // 	BiparRules table ( oTable.size1(), -1,  oTable.size1());
    // 	loadTable(table, oTable);

    // 	std::chrono::high_resolution_clock::time_point t1 =
    // std::chrono::high_resolution_clock::now(); 	table.compress();
    // 	int duration = std::chrono::duration_cast<std::chrono::duration<double,
    // std::milli>>(std::chrono::high_resolution_clock::now()-t1).count();

    // 	std::cout << "HeurB-LCDP: " << table.getNbRules()  << ", valid: " <<
    // table.isCompressionValid() << std::endl; 	if(table.isCompressionValid()) {
    // 		ofs << '\t' << table.getNbCompressedRules();
    // 		ofsTime << '\t' << duration;
    // 	} else {
    // 		ofs << "\t -1";
    // 		for(int i = 0; i < oTable.size1(); ++i) {
    // 			std::cout << "[";
    // 			for(int j = 0; j < oTable.size1(); ++j) {
    // 				int p = oTable(i, j);
    // 				if(p == -1) {
    // 					std::cout << " *";
    // 				} else {
    // 					std::cout << " " << p;
    // 				}

    // 			}
    // 			std::cout << " ]" << std::endl;
    // 		}
    // 	}
    // }
    ofs << "\t -1";
    ofsTime << "\t -1";

    // if (compMod == "HeurS-LCDP" or compMod == "all" or compMod == "allHeur") {
    // 		MostSaveRules table ( oTable.size1(), -1,  oTable.size1());
    // 	loadTable(table, oTable);

    // 	std::chrono::high_resolution_clock::time_point t1 =
    // std::chrono::high_resolution_clock::now(); 	table.compress();
    // 	int duration = std::chrono::duration_cast<std::chrono::duration<double,
    // std::milli>>(std::chrono::high_resolution_clock::now()-t1).count();

    // 	std::cout << "HeurS-LCDP: " << table.getNbRules()  << ", valid: " <<
    // table.isCompressionValid() << std::endl; 	if(table.isCompressionValid()) {
    // 		ofs << '\t' << table.getNbCompressedRules();
    // 		ofsTime << '\t' << duration;
    // 	} else {
    // 		ofs << "\t -1";
    // 		ofsTime << "\t -1";
    // 		for(int i = 0; i < oTable.size1(); ++i) {
    // 			std::cout << "[";
    // 			for(int j = 0; j < oTable.size1(); ++j) {
    // 				int p = oTable(i, j);
    // 				if(p == -1) {
    // 					std::cout << " *";
    // 				} else {
    // 					std::cout << " " << p;
    // 				}

    // 			}
    // 			std::cout << " ]" << std::endl;
    // 		}
    // 	}
    // }
    ofs << "\t -1";
    ofsTime << "\t -1";

    if (compMod == "HeurSB-LCDP" or compMod == "all" or compMod == "allHeur") {
        MostSaveRulesB table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);

        std::chrono::high_resolution_clock::time_point t1 =
            std::chrono::high_resolution_clock::now();
        table.compress();
        int duration =
            std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
                std::chrono::high_resolution_clock::now() - t1)
                .count();

        std::cout
            << "HeurSB-LCDP: "
            << table.getNbRules() /*<< ", valid: " << table.isCompressionValid() */
            << std::endl;
        // if(table.isCompressionValid()) {
        ofs << '\t' << table.getNbCompressedRules();
        ofsTime << '\t' << duration;
        // } else {
        // 	ofs << "\t -1";
        // 	ofsTime << "\t -1";
        // 	for(int i = 0; i < oTable.size1(); ++i) {
        // 		std::cout << "[";
        // 		for(int j = 0; j < oTable.size1(); ++j) {
        // 			int p = oTable(i, j);
        // 			if(p == -1) {
        // 				std::cout << " *";
        // 			} else {
        // 				std::cout << " " << p;
        // 			}

        // 		}
        // 		std::cout << " ]" << std::endl;
        // 	}
        // }
    }

    if (compMod == "DP" or compMod == "all" or compMod == "allHeur") {
        DefaultPortRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);

        std::chrono::high_resolution_clock::time_point t1 =
            std::chrono::high_resolution_clock::now();
        table.compress();
        int duration =
            std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
                std::chrono::high_resolution_clock::now() - t1)
                .count();

        std::cout
            << "DP: "
            << table.getNbRules() /*<< ", valid: " << table.isCompressionValid() */
            << std::endl;
        // if(table.isCompressionValid()) {
        ofs << '\t' << table.getNbCompressedRules();
        ofsTime << '\t' << duration;
        // } else {
        // 	ofs << "\t -1";
        // 	ofsTime << "\t -1";
        // 	for(int i = 0; i < oTable.size1(); ++i) {
        // 		std::cout << "[";
        // 		for(int j = 0; j < oTable.size1(); ++j) {
        // 			int p = oTable(i, j);
        // 			if(p == -1) {
        // 				std::cout << " *";
        // 			} else {
        // 				std::cout << " " << p;
        // 			}

        // 		}
        // 		std::cout << " ]" << std::endl;
        // 	}
        // }
    }

    if (compMod == "LP-LCDP" or compMod == "all") {
        CplexRules table(oTable.size1(), -1, oTable.size1());
        loadTable(table, oTable);

        std::chrono::high_resolution_clock::time_point t1 =
            std::chrono::high_resolution_clock::now();
        table.compress();
        int duration =
            std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
                std::chrono::high_resolution_clock::now() - t1)
                .count();

        std::cout
            << "LP-LCDP: "
            << table.getNbRules() /*<< ", valid: " << table.isCompressionValid() */
            << std::endl;
        // if(table.isCompressionValid()) {
        ofs << '\t' << table.getNbCompressedRules();
        ofsTime << '\t' << duration;
        // } else {
        // 	ofs << "\t -1";
        // 	ofsTime << "\t -1";
        // 	for(int i = 0; i < oTable.size1(); ++i) {
        // 		std::cout << "[";
        // 		for(int j = 0; j < oTable.size1(); ++j) {
        // 			int p = oTable(i, j);
        // 			if(p == -1) {
        // 				std::cout << " *";
        // 			}
        // 			else {
        // 				std::cout << " " << p;
        // 			}

        // 		}
        // 		std::cout << " ]" << std::endl;
        // 	}
        // }
    }
    ofs << std::endl;
    ofsTime << std::endl;
}

template <typename Rules>
void save(const Rules& table, const std::string& _filename) {
    std::ofstream ofs(_filename, std::ofstream::out | std::ofstream::app);
    ofs << table.getActualNbRules() << '\t' << table.getNbCompressedRules()
        << std::endl;
    ofs.close();
    return;
}