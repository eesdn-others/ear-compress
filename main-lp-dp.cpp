#include <algorithm>
#include <iostream>
#include <tuple>

#include "cplex_utility.hpp"
#include "utility.hpp"
#include <map>
#include <set>
#include <utility>

#include <cstring>
#include <ilcplex/ilocplex.h>
#include <vector>

#include "DiGraph.hpp"
#include "MyTimer.hpp"

struct Demand {
    Demand(int _s, int _t, double _d)
        : s(_s)
        , t(_t)
        , d(_d) {}
    int s{};
    int t{};
    double d{};
};
std::ostream& operator<<(std::ostream& _out, const Demand& _d) {
    return _out << '(' << _d.s << ", " << _d.t << ", " << _d.d << ')';
}

struct Rule {
    Rule(int _s, int _t, int _p)
        : s(_s)
        , t(_t)
        , p(_p) {}
    int s;
    int t;
    int p;
    constexpr static int Wildcard{-1};
};
constexpr int Rule::Wildcard;
std::ostream& operator<<(std::ostream& _out, const Rule& _r) {
    _out << '(';
    if (_r.s == Rule::Wildcard) {
        _out << '*';
    } else {
        _out << _r.s;
    }
    _out << ", ";
    if (_r.t == Rule::Wildcard) {
        _out << '*';
    } else {
        _out << _r.t;
    }
    return _out << ", " << _r.p << ')';
}

bool isMatch(const Rule& _r, const Demand& _d) {
    return (_r.s == Rule::Wildcard || _r.s == _d.s) && (_r.t == Rule::Wildcard || _r.t == _d.t);
}

std::vector<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    }
    int s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        // std::cout << line << '\n';
        std::stringstream lineStream(line);
        if (line != "") {
            lineStream >> s >> t >> d;
            // std::cout << s << ", " << t << ", "<< d << '\n';
            demands.emplace_back(s, t, d * percent);
        }
    }
    return demands;
}

DiGraph getMinSpanningSubgraph(const DiGraph& graph) {
    IloEnv env;
    IloModel model(env);
    Matrix<int> edgeID(graph.getOrder(), graph.getOrder(), -1);
    const auto edges = graph.getEdges();
    for (int e = 0; e < graph.size(); ++e) {
        edgeID(edges[e].first, edges[e].second) = e;
    }
    IloNumVarArray x(env, graph.size(), 0.0, 1.0, ILOBOOL);
    model.add(IloMinimize(env, IloSum(x)));
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloNumVarArray outNeighbors(env);
        IloNumVarArray inNeighbors(env);
        for (auto v : graph.getNeighbors(u)) {
            outNeighbors.add(x[edgeID(u, v)]);
            inNeighbors.add(x[edgeID(v, u)]);
        }
        model.add(IloRange(env, 1.0, IloSum(outNeighbors), IloInfinity));
        model.add(IloRange(env, 1.0, IloSum(inNeighbors), IloInfinity));
    }
    IloCplex solver(model);
    DiGraph spanning(graph.getOrder());
    if (solver.solve()) {
        IloNumArray xVal(env);
        solver.getValues(xVal, x);
        for (int e = 0; e < graph.size(); ++e) {
            if (xVal[e]) {
                spanning.addEdge(edges[e].first, edges[e].second, 0.0);
            }
        }
    }
    return spanning;
}

void compressDP(const DiGraph& graph, const std::vector<Demand>& demands,
    int nbRules, double percent, std::string _filename) {
    Time timer;
    timer.start();
    IloEnv env;
    IloModel model(env);

    std::map<std::pair<int, int>, IloBoolVar> x;
    std::map<std::pair<int, int>, IloBoolVar> dp;

    std::map<std::tuple<int, int, int, int>, IloBoolVar> f;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> g;

    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        x.emplace(std::make_pair(u, v), IloBoolVar(env));
        dp.emplace(std::make_pair(u, v), IloBoolVar(env));

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            f.emplace(std::make_tuple(s, t, u, v), IloBoolVar(env));
            g.emplace(std::make_tuple(s, t, u, v), IloBoolVar(env));
        }
    }

    // Minimize number of active links
    IloExpr objectiveFunc(env);
    for (auto& pair : x) {
        objectiveFunc += pair.second;
    }
    model.add(IloMinimize(env, objectiveFunc));

    // Flow constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            int val = 0;
            if (u == s) {
                val = -1;
            } else if (u == t) {
                val = 1;
            }
            IloExpr flowCons(env);
            for (auto& v : graph.getNeighbors(u)) {
                flowCons += f[std::make_tuple(s, t, v, u)];
                flowCons += g[std::make_tuple(s, t, v, u)];
                flowCons -= f[std::make_tuple(s, t, u, v)];
                flowCons -= g[std::make_tuple(s, t, u, v)];
            }
            model.add(flowCons == val);
        }
    }

    // Either forwarded through default or normal
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            model.add(
                f[std::make_tuple(s, t, u, v)] + g[std::make_tuple(s, t, u, v)] <= 1);
        }
    }

    // Link capa constraints
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        IloExpr linkCapa(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            auto& d = demand.d;

            linkCapa +=
                (f[std::make_tuple(s, t, u, v)] + g[std::make_tuple(s, t, u, v)]) * d;
        }
        model.add(linkCapa <= x[std::make_pair(u, v)] * graph.getEdgeWeight(edge));
    }

    // Table size constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr tableSize(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            for (auto& v : graph.getNeighbors(u)) {
                tableSize += f[std::make_tuple(s, t, u, v)];
            }
        }
        model.add(tableSize <= nbRules - 1);
    }

    // One default port per table
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr defaultPort(env);
        for (auto& v : graph.getNeighbors(u)) {
            defaultPort += dp[std::make_pair(u, v)];
        }
        model.add(defaultPort <= 1);
    }

    // Default port
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            model.add(g[std::make_tuple(s, t, u, v)] <= dp[std::make_pair(u, v)]);
        }
    }

    IloCplex cplex(model);
    // cplex.setOut(env.getNullStream());
    // cplex.setParam(IloCplex::Threads, 8);
    cplex.setParam(IloCplex::TiLim, 60 * 60);
    // cplex.exportModel("lp.lp");
    cplex.solve();
    auto times = timer.show();
    std::ofstream ofs{_filename};
    ofs << times.first << '\t' << times.second << '\t' << cplex.getObjValue()
        << '\t' << cplex.getMIPRelativeGap() << '\n';
}

void compressLCDP(const DiGraph& graph, const std::vector<Demand>& demands,
    const int nbRules, const double percent,
    std::string _filename) {
    Time timer;
    timer.start();
    IloEnv env;
    IloModel model(env);
    IloCplex solver(env);

    Matrix<int> edgeID(graph.getOrder(), graph.getOrder(), -1);
    const auto edges = graph.getEdges();
    for (int e = 0; e < graph.size(); ++e) {
        edgeID(edges[e].first, edges[e].second) = e;
    }
    IloNumVarArray x(env, graph.size(), 0.0, 1.0, ILOBOOL);
    // std::map<std::tuple<int, int>, IloBoolVar> x;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> F;

    std::map<std::tuple<int, int, int, int>, IloBoolVar> r;
    std::map<std::tuple<int, int, int>, IloBoolVar> gs;
    std::map<std::tuple<int, int, int>, IloBoolVar> gd;
    std::map<std::tuple<int, int>, IloBoolVar> dp;

    std::map<std::tuple<int, int, int>, IloBoolVar> order;

    std::set<int> sources, destinations;

    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        // x.emplace(std::make_pair(u, v), IloBoolVar(env,
        // std::string("x[" + std::to_string(u) + ", " +
        // std::to_string(v) + ", " +  "]").c_str()));

        dp.emplace(std::make_pair(u, v),
            IloBoolVar(env, std::string("dp[" + std::to_string(u) + ", " + std::to_string(v) + ", " + "]")
                                .c_str()));

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;

            F.emplace(std::make_tuple(s, t, u, v),
                IloBoolVar(env, std::string("F[" + std::to_string(s) + ", " + std::to_string(t) + ", " + std::to_string(u) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            r.emplace(std::make_tuple(u, s, t, v),
                IloBoolVar(env, std::string("r[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(t) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            gd.emplace(std::make_tuple(u, s, v),
                IloBoolVar(env, std::string("gd[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            gs.emplace(std::make_tuple(u, t, v),
                IloBoolVar(env, std::string("gs[" + std::to_string(u) + ", " + std::to_string(t) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            sources.insert(s);
            destinations.insert(t);
        }
    }

    for (auto& u : graph.getVertices()) {
        for (auto& s : sources) {
            for (auto& t : destinations) {
                order.emplace(
                    std::make_tuple(u, s, t),
                    IloBoolVar(env, std::string("order[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(t) + "]")
                                        .c_str()));
            }
        }
    }

    for (auto& pair_tupVar : order) {
        model.add(pair_tupVar.second);
    }

    // Minimize number of active links
    model.add(IloMinimize(env, IloSum(x)));

    // Flow constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            int val = 0;
            if (u == s) {
                val = -1;
            } else if (u == t) {
                val = 1;
            }
            IloExpr flowCons(env);
            for (auto& v : graph.getNeighbors(u)) {
                flowCons += F[std::make_tuple(s, t, v, u)];
                flowCons -= F[std::make_tuple(s, t, u, v)];
            }
            IloConstraint cs{flowCons == val};
            cs.setName("flow cons");
            model.add(cs);
        }
    }

    // Link capa constraints
    for (int e = 0; e < graph.size(); ++e) {
        const auto& edge = edges[e];
        int u = edge.first, v = edge.second;

        IloExpr linkCapa(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            auto& c = demand.d;

            linkCapa += F[std::make_tuple(s, t, u, v)] * c;
        }
        IloConstraint cs{linkCapa <= x[e] * graph.getEdgeWeight(edge)};
        cs.setName("link cons");
        model.add(cs);
    }

    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr nbRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            nbRulesExpr += dp[std::make_tuple(u, v)];
            for (auto& demand : demands) {
                const auto &s = demand.s, t = demand.t;
                nbRulesExpr += r[std::make_tuple(u, s, t, v)];
            }
            for (auto& t : destinations) {
                nbRulesExpr += gs[std::make_tuple(u, t, v)];
            }
            for (auto& s : sources) {
                nbRulesExpr += gd[std::make_tuple(u, s, v)];
            }
        }

        IloConstraint cs1{nbRulesExpr < nbRules};
        cs1.setName("rules cons");
        model.add(cs1);

        IloExpr srcRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            for (auto& t : destinations) {
                srcRulesExpr += gs[std::make_tuple(u, t, v)];
            }
        }
        IloConstraint cs2{srcRulesExpr <= 1};
        cs2.setName("src rules cons");
        model.add(cs2);

        IloExpr destRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            for (auto& s : sources) {
                destRulesExpr += gd[std::make_tuple(u, s, v)];
            }
        }
        IloConstraint cs3{destRulesExpr <= 1};
        cs3.setName("dest rules cons");
        model.add(cs3);

        IloExpr defaultRuleExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            defaultRuleExpr += dp[std::make_tuple(u, v)];
        }
        IloConstraint cs4{defaultRuleExpr == 1};
        cs4.setName(std::string("default rules cons " + std::to_string(u)).c_str());
        model.add(cs4);

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            for (auto& v1 : graph.getNeighbors(u)) {
                for (auto& v2 : graph.getNeighbors(u)) {
                    if (v1 != v2) {
                        IloConstraint cs5{r[std::make_tuple(u, s, t, v1)] + (gs[std::make_tuple(u, t, v1)] + (1 - order[std::make_tuple(u, s, t)])) / 2 >= gd[std::make_tuple(u, s, v2)] - 1 + F[std::make_tuple(s, t, u, v1)]};
                        cs5.setName("order 1");
                        model.add(cs5);

                        IloConstraint cs6{r[std::make_tuple(u, s, t, v1)] + (gd[std::make_tuple(u, s, v1)] + order[std::make_tuple(u, s, t)]) / 2 >= gs[std::make_tuple(u, t, v2)]};
                        cs6.setName("order 2");
                        model.add(cs6);
                    }
                }
            }
        }
    }

    // std::cout << "Getting lazy constraints..." << std::flush;
    IloRangeArray lazyCons(env);
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (const int s1 : sources) {
            for (const int s2 : sources) {
                if (s1 != s2) {
                    for (auto& t1 : destinations) {
                        for (auto& t2 : destinations) {
                            if (t1 != t2) {
                                // IloRange cs7 { -1 <=

                                //     <= 1 };
                                lazyCons.add(
                                    IloRange(env, -1,
                                        IloNumExpr(order[std::make_tuple(u, s1, t1)] - order[std::make_tuple(u, s2, t1)] + order[std::make_tuple(u, s2, t2)] - order[std::make_tuple(u, s1, t2)]),
                                        1));
                            }
                        }
                    }
                }
            }
        }
    }
    // std::cout << "done\n";
    // Getting neighbors cuts
    IloRangeArray userCuts(env);
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloNumVarArray outNeighbors(env);
        IloNumVarArray inNeighbors(env);
        for (auto v : graph.getNeighbors(u)) {
            outNeighbors.add(x[edgeID(u, v)]);
            inNeighbors.add(x[edgeID(v, u)]);
        }
        userCuts.add(IloRange(env, 1.0, IloSum(outNeighbors), IloInfinity));
        userCuts.add(IloRange(env, 1.0, IloSum(inNeighbors), IloInfinity));
        outNeighbors.end();
        inNeighbors.end();
    }

    auto spanning = getMinSpanningSubgraph(graph);
    std::cout << "Size of spanning :" << spanning.size() << '\n';
    userCuts.add(IloRange(env, spanning.size(), IloSum(x), graph.size()));

    // At least one rule to forward the flow
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const int s = demand.s, t = demand.t;
            IloConstraint cs{
                r[std::make_tuple(u, s, t, v)] + gs[std::make_tuple(u, t, v)] + gd[std::make_tuple(u, s, v)] + dp[std::make_tuple(u, v)] >= F[std::make_tuple(s, t, u, v)]};
            cs.setName("one rules to fwd");
            model.add(cs);
        }
    }

    solver.extract(model);
    // solver.setOut(env.getNullStream());
    // solver.setParam(IloCplex::Threads, 8);
    // solver.setParam(IloCplex::TiLim, 60*60);
    solver.addLazyConstraints(lazyCons);
    solver.addUserCuts(userCuts);
    // solver.exportModel("lp.lp");
    solver.solve();
    auto times = timer.show();
    std::ofstream ofs{_filename};
    ofs << times.first << '\t' << times.second << '\t' << solver.getObjValue()
        << '\t' << solver.getMIPRelativeGap() << '\n';
}

void compressLCDP_old(const DiGraph& graph, const std::vector<Demand>& demands,
    const int nbRules, const double percent,
    std::string _filename) {
    Time timer;
    timer.start();
    IloEnv env;
    IloModel model(env);
    IloCplex solver(env);

    std::set<int> sources;
    std::set<int> destinations;

    for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
        sources.insert(demands[i].s);
        destinations.insert(demands[i].t);
    }

    Matrix<int> edgeID(graph.getOrder(), graph.getOrder(), -1);
    const auto edges = graph.getEdges();
    for (int e = 0; e < graph.size(); ++e) {
        edgeID(edges[e].first, edges[e].second) = e;
    }

    IloNumVarArray x(env, graph.size(), 0.0, 1.0, ILOBOOL);
    for (int e = 0; e < graph.size(); ++e) {
        setIloName(x[e], "x(" + toString(e) + ')');
    }
    IloNumVarArray f(env, graph.size() * demands.size(), 0.0, 1.0, ILOBOOL);
    for (int e = 0; e < graph.size(); ++e) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            setIloName(f[e + graph.size() * i],
                "f(" + toString(std::make_pair(edges[e], demands[i])) + ')');
        }
    }
    IloNumVarArray r(env, graph.size() * graph.getOrder() * graph.getOrder(), 0.0,
        1.0, ILOBOOL);
    auto rIndex = [&](const int __e, const int __s, const int __t) {
        return __e + __s * graph.size() + __t * graph.size() * graph.getOrder();
    };
    for (int e = 0; e < graph.size(); ++e) {
        for (const int s : sources) {
            for (const int t : destinations) {
                setIloName(r[rIndex(e, s, t)],
                    "r(" + toString(std::make_tuple(edges[e].first, s, t, edges[e].second)) + ')');
            }
        }
    }
    IloNumVarArray gs(env, graph.size() * graph.getOrder(), 0.0, 1.0, ILOBOOL);
    for (int e = 0; e < graph.size(); ++e) {
        for (const int t : destinations) {
            setIloName(gs[e + graph.size() * t],
                "gs(" + toString(std::make_tuple(edges[e].first, '*', t, edges[e].second)) + ')');
        }
    }
    IloNumVarArray gd(env, graph.size() * graph.getOrder(), 0.0, 1.0, ILOBOOL);
    for (int e = 0; e < graph.size(); ++e) {
        for (const int s : sources) {
            setIloName(gd[e + graph.size() * s],
                "gd(" + toString(std::make_tuple(edges[e].first, s, '*', edges[e].second)) + ')');
        }
    }
    IloNumVarArray dp(env, graph.size(), 0.0, 1.0, ILOBOOL);
    for (int e = 0; e < graph.size(); ++e) {
        setIloName(dp[e], "dp(" + toString(std::make_tuple(edges[e].first, '*', '*', edges[e].second)) + ')');
    }

    IloNumVarArray order(env,
        graph.getOrder() * graph.getOrder() * graph.getOrder(),
        0.0, 1.0, ILOBOOL);
    auto oIndex = [&](const int __u, const int __s, const int __t) {
        return __u + __s * graph.getOrder() + __t * graph.getOrder() * graph.getOrder();
    };

    for (int u = 0; u < graph.getOrder(); ++u) {
        for (const int s : sources) {
            for (const int t : destinations) {
                setIloName(order[oIndex(u, s, t)],
                    "order(" + toString(std::make_tuple(u, s, t)) + ')');
            }
        }
    }

    model.add(x);
    model.add(f);
    model.add(r);
    model.add(gs);
    model.add(gd);
    model.add(dp);
    model.add(order);

    // Minimize number of active links
    model.add(IloMinimize(env, IloSum(x)));

    // Flow constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            int val = u == demands[i].s ? -1 : (u == demands[i].t ? 1 : 0);
            IloExpr flowCons(env);
            for (const Graph::Node v : graph.getNeighbors(u)) {
                flowCons += f[edgeID(v, u) + graph.size() * i];
                flowCons -= f[edgeID(u, v) + graph.size() * i];
            }
            model.add(IloRange(env, val, flowCons, val));
            flowCons.end();
        }
    }
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            IloExpr outgoing(env);
            IloExpr incoming(env);
            for (const Graph::Node v : graph.getNeighbors(u)) {
                outgoing += f[edgeID(v, u) + graph.size() * i];
                incoming += f[edgeID(u, v) + graph.size() * i];
            }
            model.add(IloRange(env, 0.0, outgoing, 1.0));
            model.add(IloRange(env, 0.0, incoming, 1.0));
        }
    }

    // Link capa constraints
    for (int e = 0; e < graph.size(); ++e) {
        IloExpr linkCapa(env);
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            linkCapa -= f[e + graph.size() * i] * demands[i].d;
        }
        linkCapa += x[e] * graph.getEdgeWeight(edges[e]);
        model.add(linkCapa >= 0.0);
        linkCapa.end();
    }

    // Node capa constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr nbRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            const int e = edgeID(u, v);
            nbRulesExpr += dp[e];
            for (auto& demand : demands) {
                nbRulesExpr += r[rIndex(e, demand.s, demand.t)];
            }
            for (const int t : destinations) {
                nbRulesExpr += gs[e + graph.size() * t];
            }
            for (const int s : sources) {
                nbRulesExpr += gd[e + graph.size() * s];
            }
        }
        model.add(nbRulesExpr <= nbRules);
        nbRulesExpr.end();
    }

    // \forall u \in V,
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            IloExpr nbUnAggRules(env);
            for (const Graph::Node v : graph.getNeighbors(u)) {
                nbUnAggRules += r[rIndex(edgeID(u, v), demands[i].s, demands[i].t)];
            }
            model.add(nbUnAggRules <= 1.0);
            nbUnAggRules.end();
        }
    }
    // \forall u \in V, t \in \mathcal{T}, \sum_{v \in N^+(u)}{s^u_{tv}}
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (const int t : destinations) {
            IloExpr srcRulesExpr(env);
            for (const Graph::Node v : graph.getNeighbors(u)) {
                srcRulesExpr += gs[edgeID(u, v) + graph.size() * t];
            }
            model.add(srcRulesExpr <= 1.0);
            srcRulesExpr.end();
        }
    }

    // \forall u \in V, s \in \mathcal{S}, \sum_{v \in N^+(u)}{t^u_{sv}}
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (auto& s : sources) {
            IloExpr destRulesExpr(env);
            for (const Graph::Node v : graph.getNeighbors(u)) {
                destRulesExpr += gd[edgeID(u, v) + graph.size() * s];
            }
            model.add(destRulesExpr <= 1.0);
            destRulesExpr.end();
        }
    }

    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr defaultRuleExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            defaultRuleExpr += dp[edgeID(u, v)];
        }
        model.add(defaultRuleExpr <= 1.0);
        defaultRuleExpr.end();
    }

    for (int u = 0; u < graph.getOrder(); ++u) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            const int s = demands[i].s, t = demands[i].t;
            for (const Graph::Node v1 : graph.getNeighbors(u)) {
                for (const Graph::Node v2 : graph.getNeighbors(u)) {
                    if (v1 != v2) {
                        model.add(r[rIndex(edgeID(u, v1), s, t)] + gs[edgeID(u, v1) + graph.size() * t] >= gd[edgeID(u, v2) + graph.size() * s] - 1 + f[edgeID(u, v1) + graph.size() * i]);
                        model.add(r[rIndex(edgeID(u, v1), s, t)] + (1 - order[oIndex(u, s, t)]) >= gd[edgeID(u, v2) + graph.size() * s] - 1 + f[edgeID(u, v1) + graph.size() * i]);

                        model.add(r[rIndex(edgeID(u, v1), s, t)] + gd[edgeID(u, v1) + graph.size() * s] >= gs[edgeID(u, v2) + graph.size() * t] - 1 + f[edgeID(u, v1) + graph.size() * i]);
                        model.add(r[rIndex(edgeID(u, v1), s, t)] + order[oIndex(u, s, t)] >= gs[edgeID(u, v2) + graph.size() * t] - 1 + f[edgeID(u, v1) + graph.size() * i]);
                    }
                }
            }
        }
    }

    IloRangeArray lazyCons(env);
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (const int s1 : sources) {
            for (const int s2 : sources) {
                if (s1 != s2) {
                    for (const int t1 : destinations) {
                        for (const int t2 : destinations) {
                            if (t1 != t2) {
                                model.add(IloRange(env, -1,
                                    IloNumExpr(order[oIndex(u, s1, t1)] - order[oIndex(u, s2, t1)] + order[oIndex(u, s2, t2)] - order[oIndex(u, s1, t2)]),
                                    1));
                            }
                        }
                    }
                }
            }
        }
    }

    // At least one rule to forward the flow
    for (int e = 0; e < graph.size(); ++e) {
        for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
            const int s = demands[i].s, t = demands[i].t;
            model.add(IloConstraint(r[rIndex(e, s, t)] + gs[e + graph.size() * t] + gd[e + graph.size() * s] + dp[e] >= f[e + graph.size() * i]));
            model.add(IloConstraint(r[rIndex(e, s, t)] <= f[e + graph.size() * i]));
        }
    }

    solver.extract(model);
    solver.solve();
    auto times = timer.show();

    // Check solution
    // Get paths
    std::vector<Graph::Path> paths(demands.size());
    IloNumArray fVal(env);
    solver.getValues(fVal, f);
    for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
        paths[i].emplace_back(demands[i].s);
        while (paths[i].back() != demands[i].t) {
            for (const Graph::Node v : graph.getNeighbors(paths[i].back())) {
                if (fVal[edgeID(paths[i].back(), v) + graph.size() * i]) {
                    fVal[edgeID(paths[i].back(), v) + graph.size() * i] = 0;
                    paths[i].emplace_back(v);
                    break;
                }
            }
        }
    }
    // Get rules
    std::vector<std::vector<Rule>> rules(graph.getOrder());
    for (Graph::Node u = 0; u < graph.getOrder(); ++u) {
        // Add non aggregated rules
        for (const Graph::Node v : graph.getNeighbors(u)) {
            for (const auto& demand : demands) {
                if (solver.getValue(r[rIndex(edgeID(u, v), demand.s, demand.t)])) {
                    rules[u].emplace_back(demand.s, demand.t, v);
                }
            }
        }

        int nbUnAggRules = rules[u].size();
        // Add wildcard rules
        for (const int t : destinations) {
            for (const Graph::Node v : graph.getNeighbors(u)) {
                if (solver.getValue(gs[edgeID(u, v) + graph.size() * t])) {
                    rules[u].emplace_back(Rule::Wildcard, t, v);
                    break;
                }
            }
        }
        for (const int s : destinations) {
            for (const Graph::Node v : graph.getNeighbors(u)) {
                if (solver.getValue(gd[edgeID(u, v) + graph.size() * s])) {
                    rules[u].emplace_back(s, Rule::Wildcard, v);
                    break;
                }
            }
        }
        std::sort(rules[u].begin() + nbUnAggRules, rules[u].end(),
            [&](const Rule& __r1, const Rule& __r2) {
                if ((__r1.s == Rule::Wildcard && __r2.s == Rule::Wildcard) || (__r1.t == Rule::Wildcard && __r2.t == Rule::Wildcard)) { // Both rules of the same type
                    return true;
                }
                const int s = __r1.s != Rule::Wildcard ? __r1.s : __r2.s;
                const int t = __r1.t != Rule::Wildcard ? __r1.t : __r2.t;
                return bool(solver.getValue(order[oIndex(u, s, t)]));
            });
        // Add default port
        for (const Graph::Node v : graph.getNeighbors(u)) {
            if (solver.getValue(dp[edgeID(u, v)])) {
                rules[u].emplace_back(Rule::Wildcard, Rule::Wildcard, v);
            }
        }
        if (static_cast<int>(rules[u].size()) > nbRules) {
            std::cerr << "Number of rules exceeded on " << u << " -> "
                      << rules[u].size() << '\n';
        }
    }

    // Check link capa
    std::vector<double> linkUsage(graph.size());
    for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
        for (auto iteU = paths[i].begin(), iteV = std::next(iteU);
             iteV != paths[i].end(); ++iteU, ++iteV) {
            linkUsage[edgeID(*iteU, *iteV)] += demands[i].d;
            if (linkUsage[edgeID(*iteU, *iteV)] > graph.getEdgeWeight(*iteU, *iteV)) {
                std::cerr << "Link capacity exceeded for (" << *iteU << ", " << *iteV
                          << "): " << linkUsage[edgeID(*iteU, *iteV)] << " > "
                          << graph.getEdgeWeight(*iteU, *iteV) << '\n';
            }
        }
    }

    // Check rules
    for (int i = 0; i < static_cast<int>(demands.size()); ++i) {
        for (auto iteU = paths[i].begin(), iteV = std::next(iteU);
             iteV != paths[i].end(); ++iteU, ++iteV) {

            for (const auto& rule : rules[*iteU]) {
                if (isMatch(rule, demands[i])) {
                    if (rule.p != *iteV) {
                        std::cout << "Wrong rule for demand " << demands[i] << " on "
                                  << *iteU << " -> " << rule << " instead of " << *iteV
                                  << '\n';
                        if (rule.s == -1) {
                            displayValue(
                                solver,
                                gs[edgeID(*iteU, rule.p) + graph.size() * demands[i].t]);
                            std::cout << '\n';
                        } else if (rule.t == -1) {
                            displayValue(
                                solver,
                                gd[edgeID(*iteU, rule.p) + graph.size() * demands[i].s]);
                            std::cout << '\n';
                        }
                        displayValue(solver, f[edgeID(*iteU, *iteV) + graph.size() * i]);
                        std::cout << '\n';
                        displayValue(
                            solver,
                            r[rIndex(edgeID(*iteU, *iteV), demands[i].s, demands[i].t)]);
                        std::cout << '\n';
                        displayValue(
                            solver, gs[edgeID(*iteU, *iteV) + graph.size() * demands[i].t]);
                        std::cout << '\n';
                        displayValue(
                            solver, gd[edgeID(*iteU, *iteV) + graph.size() * demands[i].s]);
                        std::cout << '\n';
                        std::cout << rules[*iteU] << '\n';
                        exit(-1);
                    }
                    break;
                }
            }
        }
    }

    std::ofstream ofs{_filename};
    std::cout << "Found solution in " << times << ", with obj value of "
              << solver.getObjValue() << " (gap of " << solver.getMIPRelativeGap()
              << ")\n";
    ofs << times.first << '\t' << times.second << '\t' << solver.getObjValue()
        << '\t' << solver.getMIPRelativeGap() << '\n';
}

// Minimize the number of rules in the network using only the default port
void minDP(const DiGraph& graph, int nbLinks,
    const std::vector<Demand>& demands, double percent,
    std::string _filename) {
    Time timer;
    timer.start();
    IloEnv env;
    IloModel model(env);

    std::map<std::pair<int, int>, IloBoolVar> x;
    std::map<std::pair<int, int>, IloBoolVar> dp;

    std::map<std::tuple<int, int, int, int>, IloBoolVar> f;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> g;

    IloIntVar nbRules{env, 1, (int)demands.size()};

    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        x.emplace(std::make_pair(u, v), IloBoolVar(env));
        dp.emplace(std::make_pair(u, v), IloBoolVar(env));

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;

            f.emplace(std::make_tuple(s, t, u, v), IloBoolVar(env));
            g.emplace(std::make_tuple(s, t, u, v), IloBoolVar(env));
        }
    }

    // Minimize number of max rules
    model.add(IloMinimize(env, nbRules));

    IloExpr linkNb(env);
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        linkNb += x[std::make_pair(u, v)];
    }
    IloConstraint csLn{linkNb == nbLinks};
    csLn.setName("# links");
    model.add(csLn);

    // Flow constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            int val = 0;
            if (u == s) {
                val = -1;
            } else if (u == t) {
                val = 1;
            }
            IloExpr flowCons(env);
            for (auto& v : graph.getNeighbors(u)) {
                flowCons += f[std::make_tuple(s, t, v, u)];
                flowCons += g[std::make_tuple(s, t, v, u)];
                flowCons -= f[std::make_tuple(s, t, u, v)];
                flowCons -= g[std::make_tuple(s, t, u, v)];
            }
            model.add(flowCons == val);
        }
    }

    // Either forwarded through default or normal
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            model.add(
                f[std::make_tuple(s, t, u, v)] + g[std::make_tuple(s, t, u, v)] <= 1);
        }
    }

    // Link capa constraints
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        IloExpr linkCapa(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            auto& c = demand.d;

            linkCapa +=
                (f[std::make_tuple(s, t, u, v)] + g[std::make_tuple(s, t, u, v)]) * c;
        }
        model.add(linkCapa <= x[std::make_pair(u, v)] * graph.getEdgeWeight(edge));
    }

    // Table size constraints
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr tableSize(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            for (auto& v : graph.getNeighbors(u)) {
                tableSize += f[std::make_tuple(s, t, u, v)];
            }
        }
        model.add(tableSize <= nbRules - 1);
    }

    // One default port per table
    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr defaultPort(env);
        for (auto& v : graph.getNeighbors(u)) {
            defaultPort += dp[std::make_pair(u, v)];
        }
        model.add(defaultPort <= 1);
    }

    // Default port
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            model.add(g[std::make_tuple(s, t, u, v)] <= dp[std::make_pair(u, v)]);
        }
    }

    IloCplex cplex(model);
    // cplex.setOut(env.getNullStream());
    // cplex.setParam(IloCplex::Threads, 8);
    // cplex.setParam(IloCplex::TiLim, 60*60);
    // cplex.exportModel("lp.lp");
    cplex.solve();
    auto times = timer.show();
    std::ofstream ofs{_filename};
    ofs << times.first << '\t' << times.second << '\t' << cplex.getObjValue()
        << '\t' << cplex.getMIPRelativeGap() << '\n';
}

// Minimize the number of rules in the network using wildcard rules
void minLCDP(const DiGraph& graph, int nbLinks,
    const std::vector<Demand>& demands, double percent,
    std::string _filename) {
    Time timer;
    timer.start();
    IloEnv env;
    IloModel model(env);

    std::map<std::pair<int, int>, IloBoolVar> x;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> F;

    std::map<std::tuple<int, int, int, int>, IloBoolVar> r;
    std::map<std::tuple<int, int, int>, IloBoolVar> gs;
    std::map<std::tuple<int, int, int>, IloBoolVar> gd;
    std::map<std::tuple<int, int>, IloBoolVar> dp;

    std::map<std::tuple<int, int, int>, IloBoolVar> order;
    IloIntVar nbRules{env, 0, (int)demands.size()};

    std::set<int> sources, destinations;

    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        x.emplace(std::make_pair(u, v),
            IloBoolVar(env, std::string("x[" + std::to_string(u) + ", " + std::to_string(v) + ", " + "]")
                                .c_str()));

        dp.emplace(std::make_pair(u, v),
            IloBoolVar(env, std::string("dp[" + std::to_string(u) + ", " + std::to_string(v) + ", " + "]")
                                .c_str()));

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;

            F.emplace(std::make_tuple(s, t, u, v),
                IloBoolVar(env, std::string("F[" + std::to_string(s) + ", " + std::to_string(t) + ", " + std::to_string(u) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            r.emplace(std::make_tuple(u, s, t, v),
                IloBoolVar(env, std::string("r[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(t) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            gd.emplace(std::make_tuple(u, s, v),
                IloBoolVar(env, std::string("gd[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            gs.emplace(std::make_tuple(u, t, v),
                IloBoolVar(env, std::string("gs[" + std::to_string(u) + ", " + std::to_string(t) + ", " + std::to_string(v) + "]")
                                    .c_str()));

            sources.insert(s);
            destinations.insert(t);
        }
    }

    for (auto& u : graph.getVertices()) {
        for (auto& s : sources) {
            for (auto& t : destinations) {
                order.emplace(
                    std::make_tuple(u, s, t),
                    IloBoolVar(env, std::string("order[" + std::to_string(u) + ", " + std::to_string(s) + ", " + std::to_string(t) + "]")
                                        .c_str()));
            }
        }
    }

    // Minimize number of active links
    // IloExpr objectiveFunc(env);
    // for(auto& pair : x) {
    //     objectiveFunc += pair.second;
    // }
    model.add(IloMinimize(env, nbRules));

    IloExpr linkNb(env);
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        linkNb += x[std::make_pair(u, v)];
    }
    IloConstraint csLn{linkNb == nbLinks};
    csLn.setName("# links");
    model.add(csLn);

    // Flow constraints
    for (auto& u : graph.getVertices()) {
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            int val = 0;
            if (u == s) {
                val = -1;
            } else if (u == t) {
                val = 1;
            }
            IloExpr flowCons(env);
            for (auto& v : graph.getNeighbors(u)) {
                flowCons += F[std::make_tuple(s, t, v, u)];
                flowCons -= F[std::make_tuple(s, t, u, v)];
            }
            IloConstraint cs{flowCons == val};
            cs.setName("flow cons");
            model.add(cs);
        }
    }

    // Link capa constraints
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        IloExpr linkCapa(env);
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            auto& c = demand.d;

            linkCapa += F[std::make_tuple(s, t, u, v)] * c;
        }
        IloConstraint cs{linkCapa <= x[std::make_pair(u, v)] * graph.getEdgeWeight(edge)};
        cs.setName("link cons");
        model.add(cs);
    }

    for (int u = 0; u < graph.getOrder(); ++u) {
        IloExpr nbRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            nbRulesExpr += dp[std::make_tuple(u, v)];
            for (auto& demand : demands) {
                const auto &s = demand.s, t = demand.t;
                nbRulesExpr += r[std::make_tuple(u, s, t, v)];
            }
            for (auto& t : destinations) {
                nbRulesExpr += gs[std::make_tuple(u, t, v)];
            }
            for (auto& s : sources) {
                nbRulesExpr += gd[std::make_tuple(u, s, v)];
            }
        }

        IloConstraint cs1{nbRulesExpr <= nbRules};
        cs1.setName("rules cons");
        model.add(cs1);

        IloExpr srcRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            for (auto& t : destinations) {
                srcRulesExpr += gs[std::make_tuple(u, t, v)];
            }
        }
        IloConstraint cs2{srcRulesExpr <= 1};
        cs2.setName("src rules cons");
        model.add(cs2);

        IloExpr destRulesExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            for (auto& s : sources) {
                destRulesExpr += gd[std::make_tuple(u, s, v)];
            }
        }
        IloConstraint cs3{destRulesExpr <= 1};
        cs3.setName("dest rules cons");
        model.add(cs3);

        IloExpr defaultRuleExpr(env);
        for (const Graph::Node v : graph.getNeighbors(u)) {
            defaultRuleExpr += dp[std::make_tuple(u, v)];
        }
        IloConstraint cs4{defaultRuleExpr == 1};
        cs4.setName(std::string("default rules cons " + std::to_string(u)).c_str());
        model.add(cs4);

        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            for (auto& v1 : graph.getNeighbors(u)) {
                for (auto& v2 : graph.getNeighbors(u)) {
                    if (v1 != v2) {
                        IloConstraint cs5{r[std::make_tuple(u, s, t, v1)] + (gs[std::make_tuple(u, t, v1)] + (1 - order[std::make_tuple(u, s, t)])) / 2 >= gd[std::make_tuple(u, s, v2)]};
                        cs5.setName("order 1");
                        model.add(cs5);

                        IloConstraint cs6{r[std::make_tuple(u, s, t, v1)] + (gd[std::make_tuple(u, s, v1)] + order[std::make_tuple(u, s, t)]) / 2 >= gs[std::make_tuple(u, t, v2)]};
                        cs6.setName("order 2");
                        model.add(cs6);
                    }
                }
            }
        }

        for (const int s1 : sources) {
            for (const int s2 : sources) {
                if (s1 != s2) {
                    for (auto& t1 : destinations) {
                        for (auto& t2 : destinations) {
                            if (t1 != t2) {
                                IloConstraint cs7{1 <= order[std::make_tuple(u, s1, t1)] - order[std::make_tuple(u, s2, t1)] + order[std::make_tuple(u, s2, t2)] - order[std::make_tuple(u, s1, t2)] + 2 <= 3};
                                cs7.setName("triangular");
                                model.add(cs7);
                            }
                        }
                    }
                }
            }
        }
    }

    // At least one rule to forward the flow
    for (auto& edge : graph.getEdges()) {
        int u = edge.first, v = edge.second;
        for (auto& demand : demands) {
            const auto &s = demand.s, t = demand.t;
            IloConstraint cs{
                r[std::make_tuple(u, s, t, v)] + gs[std::make_tuple(u, t, v)] + gd[std::make_tuple(u, s, v)] + dp[std::make_tuple(u, v)] >= F[std::make_tuple(s, t, u, v)]};
            cs.setName("one rules to fwd");
            model.add(cs);
        }
    }

    IloCplex cplex(model);
    cplex.solve();
    auto times = timer.show();
    std::ofstream ofs{_filename};
    ofs << times.first << '\t' << times.second << '\t' << cplex.getObjValue()
        << '\t' << cplex.getMIPRelativeGap() << '\n';
}

void findSolution(int argc, char** argv) {
    std::string name(argv[2]);
    int nbRules = std::stoi(argv[3]);
    double percent = std::stod(argv[4]);

    auto demands = loadDemandsFromFile(
        std::string("./instances/" + name + "_demand.txt"), percent);
    auto topology =
        DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));
    auto graph = std::get<0>(topology);

    std::string type{argv[1]};

    if (type == "DP") {
        compressDP(graph, demands, nbRules, percent,
            "./results/DP_" + name + "_" + std::to_string(nbRules));
    } else if (type == "LCDP") {
        compressLCDP(graph, demands, nbRules, percent,
            "./results/LCDP_" + name + "_" + std::to_string(nbRules));
    } else if (type == "LCDP_old") {
        compressLCDP_old(graph, demands, nbRules, percent,
            "./results/LCDP_" + name + "_" + std::to_string(nbRules));
    } else if (type == "minLCDP") {
        minLCDP(graph, nbRules, demands, percent,
            "./results/minLCDP_" + name + "_" + std::to_string(nbRules));
    } else if (type == "minDP") {
        minDP(graph, nbRules, demands, percent,
            "./results/minDP_" + name + "_" + std::to_string(nbRules));
    } else {
        std::cerr << "No type specified: " << type << '\n';
    }
}

int main(int argc, char** argv) {
    findSolution(argc, argv);
    return 0;
}