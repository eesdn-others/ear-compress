# Adding clang-tidy target if executable is found
find_program(CLANG_TIDY "clang-tidy")
find_program(CLANG_TIDY_PYTHON "run-clang-tidy.py")
if(CLANG_TIDY)
    add_custom_target(
        clang-tidy
        run-clang-tidy.py -header-filter='.*' 
            -extra-arg="-std=c++17"            
    )
    add_custom_target(
        clang-tidy-fix
        run-clang-tidy-6.0.py -fix -header-filter='.*' 
            -extra-arg="-std=c++17" -fix
    )
endif()

# additional target to perform clang-format run, requires clang-format

# get all project files
file(GLOB
    ALL_CXX_SOURCE_FILES
    ${PROJECT_SOURCE_DIR}/*/*.[chi]pp
    *.[chi]pp
)

find_program(CLANG_FORMAT "clang-format")
if(CLANG_FORMAT)
add_custom_target(
    clang-format
    COMMAND clang-format
    -i
    -style=file
    ${ALL_CXX_SOURCE_FILES}
)
endif()