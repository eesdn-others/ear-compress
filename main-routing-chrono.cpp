#include <cassert>
#include <chrono>
#include <fstream> //loadDemandsFromFile
#include <functional>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <utility> //loadDemandsFromFile
#include <vector>  //loadDemandsFromFile

#include "DefaultPortRules.hpp"
#include "WildcardRules.hpp"
// #include "DestinationRules.hpp"
#include "CplexRules.hpp"

#include "DiGraph.hpp"
#include "Routing.hpp"

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

struct Parameters {
    std::string network{};
    std::string rules{};
    std::string heuristic{};
    std::string weight{};
    int nbRules = 0;
    double percent = 0;

} parameters;

std::vector<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    int s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        // std::cout << line << std::endl;
        std::stringstream lineStream(line);
        if (line != "") {
            lineStream >> s >> t >> d;
            // std::cout << s << ", " << t << ", "<< d << std::endl;
            demands.emplace_back(s, t, d * percent);
        }
    }
    return demands;
}

template <typename Rules, typename Weight>
std::tuple<Routing<Rules>, double>
runOnce(int _maxRule, const DiGraph& topology, int _nbServers, int _nbSwitches,
    const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();

    ear.template routeAllDemands<Weight>(demands);

    return std::make_tuple(
        ear,
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count());
}

template <typename Rules, typename Weight>
std::tuple<Routing<Rules>, double>
runOnceEnd(int _maxRule, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();

    ear.template routeAllDemands<Weight>(demands);

    return std::make_tuple(
        ear,
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count());
}

template <typename Rules, typename Weight>
std::tuple<Routing<Rules>, double>
runOnceFinalNoLimit(int _maxRule, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();

    ear.template routeAllDemandsNoLimit<Weight>(demands, false);

    double pathFindingTime =
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count();

    for (auto& rules : ear.getRules()) {
        rules.compress();
    }
    return std::make_tuple(ear, pathFindingTime);
}

template <typename Rules>
std::function<std::tuple<Routing<Rules>, double>(int, const DiGraph&, int, int,
    const std::vector<Demand>&)>
getHeuristic(const std::string& heuristicName,
    const std::string& weightFunctionName) {

    typedef std::function<std::tuple<Routing<Rules>, double>(
        int, const DiGraph&, int, int, const std::vector<Demand>&)>
        HeuristicFunction;

#define findHeuristic(name, func)                                                    \
    if (heuristicName.compare(#name) == 0) {                                         \
        if (weightFunctionName.compare("dumb") == 0) {                               \
            run = func<Rules, typename Routing<Rules>::DumbWeight>;                  \
        } else if (weightFunctionName.compare("metric") == 0) {                      \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 1>>; \
        } else if (weightFunctionName.compare("metricPlusOne") == 0) {               \
            run = func<Rules,                                                        \
                typename Routing<Rules>::template MetricPlusOneWeight<1, 1>>;        \
        } else if (weightFunctionName.compare("metricPlusOneCapa") == 0) {           \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 0>>; \
        } else if (weightFunctionName.compare("metricPlusOneRules") == 0) {          \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<0, 1>>; \
        } else if (weightFunctionName.compare("metricPlusOne75Rules") == 0) {        \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<1, 3>>; \
        } else if (weightFunctionName.compare("metricPlusOne75Capa") == 0) {         \
            run = func<Rules, typename Routing<Rules>::template MetricWeight<3, 1>>; \
        }                                                                            \
    }

    HeuristicFunction run;

    findHeuristic(once, runOnce) else findHeuristic(onceFinalNoLimit,
        runOnceFinalNoLimit) else {
        std::cout << "No heuristic given!" << std::endl;
        exit(-1);
    }
    return run;
}

template <typename Rules>
void launch(int _nbRules, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands,
    const std::string& name, const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl
              << "Percent: " << percent << std::endl;

    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);
    auto triplet = run(_nbRules, topology, _nbServers, _nbSwitches, demands);
    auto solution = std::get<0>(triplet);
    auto pathFindingTime = std::get<1>(triplet);

    std::cout << pathFindingTime << "ms. " << std::endl;

    if (solution.getNbDemands() < (int)demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << "\t" << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem()
                  << std::endl;
    }

// Save metrics results...
#ifdef DYNAMIC
    std::string resultsFolder("results_dynamic");
#else
    std::string resultsFolder("results");
#endif

    mkdir(std::string("./" + resultsFolder + "/" + name).c_str(), 0700);
    solution.saveMetrics("./" + resultsFolder + "/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_" + std::to_string(_nbRules) + "_" + std::to_string(percent) + ".txt",
        demands.size());

//... and time results.
#ifdef CHRONO
    std::ofstream ofs("./" + resultsFolder + "/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_" + std::to_string(_nbRules) + "_" + std::to_string(percent) + ".time");

    ofs << pathFindingTime << std::endl;
    // ofs << compressTimes.size() << std::endl;

    for (auto& r : solution.getRules()) {
        for (auto& t : r.getCompressTimes()) {
            ofs << std::get<1>(t) << '\t';
        }
    }

    ofs << std::endl;

    for (auto& r : solution.getRules()) {
        for (auto& t : r.getCompressTimes()) {
            ofs << std::get<0>(t) << '\t';
        }
    }
    ofs << std::endl;

    for (auto& r : solution.getRules()) {
        for (auto& t : r.getCompressTimes()) {
            ofs << std::get<2>(t) << '\t';
        }
    }
    ofs << std::endl;

    ofs.close();
#endif

    for (auto& i : solution.getVertices()) {
        if (solution.isSwitch(i)) {
            auto& rule = solution.getRules(i);
#ifdef DEBUG
            if (!rule.isCompressionValid()) {
                std::cout << "Compression not valid for " << i << std::endl;
            }
#endif
            std::cout << i << ':' << rule.getCompressedRules().size() << "/"
                      << rule.getActualNbRules() << '\t'
                      << 100 * (rule.getCompressedRules().size() / (double)rule.getActualNbRules())
                      << '\t' << solution.getDensity(i) << '\t'
                      << solution.getResidual().getOutDegree(i) << '\t';
            if (rule.getCompressedRules().size() <= 3) {
                for (auto& r : rule.getCompressedRules()) {
                    std::cout << r << ", ";
                }
            }
            std::cout << std::endl;
        }
    }
}

void findSolution(int argc, char** argv) {
    std::string name(argv[1]);
    auto demands = loadDemandsFromFile(
        std::string("./instances/" + name + "_demand.txt"), parameters.percent);
    auto topology =
        DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));

    std::string strNbRules(argv[5]);

    int nbRules;

    if (strNbRules.compare("max") == 0) {
        nbRules = std::numeric_limits<int>::max();
    } else {
        nbRules = std::stoi(strNbRules);
    }

    if (std::string(argv[2]) == std::string("WC")) {
        launch<WildcardRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "WildcardRules",
            argv[4], argv[3], parameters.percent);
    } else if (std::string(argv[2]) == std::string("DP")) {
        launch<DefaultPortRules>(nbRules, std::get<0>(topology),
            std::get<1>(topology), std::get<2>(topology),
            demands, name, "DefaultPortRules", argv[4],
            argv[3], parameters.percent);
    } else if (std::string(argv[2]) == std::string("CPLEX")) {
        launch<CplexRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "CplexRules",
            argv[4], argv[3], parameters.percent);
    }
}

int main(int argc, char** argv) {
    std::string network = std::string(argv[1]);
    parameters.rules = std::string(argv[2]);
    parameters.heuristic = std::string(argv[3]);
    parameters.weight = std::string(argv[4]);
    parameters.nbRules = 0;
    parameters.percent = std::stod(argv[6]);
    findSolution(argc, argv);
    return 0;
}