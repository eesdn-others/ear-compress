#include <cassert>
#include <chrono>
#include <fstream> //loadDemandsFromFile
#include <functional>
#include <iostream>
#include <list>
#include <random>
#include <set>
#include <sstream>
#include <string>
#include <utility> //loadDemandsFromFile
#include <vector>  //loadDemandsFromFile

// #include "AllWildcardRules.hpp"
#include "DiGraph.hpp"
#include "EarRouting.hpp"

struct Parameters {
    std::string network;
    std::string rules;
    std::string heuristic;
    std::string weight;
    int nbRules;
    double percent;
} parameters;

std::set<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::set<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    int s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        std::stringstream lineStream(line);

        lineStream >> s >> t >> d;
        demands.insert(std::make_tuple(s, t, d * percent));
    }
    return demands;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runPathRestart(int _maxRule, const DiGraph& topology,
    const std::set<Demand>& demands) {

    EarRouting<Rules> ear(topology, _maxRule);

    std::set<Edge> nonRemovableEdge;

    if (!ear.template routeAllDemands<Weight>(demands)) {
        return ear;
    }

    EarRouting<Rules> solution = ear;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }

        for (const auto& edge : edges) {
            auto demands = ear.deleteLink(edge);
            if (ear.template routeAllDemands<Weight>(demands)) {
                solution = ear;
            } else {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runNoCompress(int _maxRule, const DiGraph& topology,
    const std::set<Demand>& demands) {

    EarRouting<Rules> ear(topology, _maxRule);

    std::set<Edge> nonRemovableEdge;

    if (!ear.template routeAllDemandsNoCompress<Weight>(demands)) {
        return ear;
    }

    EarRouting<Rules> solution = ear;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        //        std::cout << nonRemovableEdge.size() << std::endl;
        if (edges.empty()) {
            break;
        }

        for (const auto& edge : edges) {
            auto demands = ear.deleteLink(edge);
            if (ear.template routeAllDemandsNoCompress<Weight>(demands)) {
                solution = ear;
            } else {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runFullRestart(int _maxRule, const DiGraph& topology,
    const std::set<Demand>& demands) {

    EarRouting<Rules> ear(topology, _maxRule);

    std::set<Edge> nonRemovableEdge;

    if (!ear.template routeAllDemands<Weight>(demands)) {
        return ear;
    }

    auto solution = ear;
    EarRouting<Rules> base(topology, _maxRule);
    std::list<std::pair<int, int>> toRemove;
    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }
        for (const auto& edge : edges) {
            ear = base;
            ear.deleteLink(edges.front());
            if (ear.template routeAllDemands<Weight>(demands)) {
                base.deleteLink(edge);
                std::swap(solution, ear);
                break;
            } else {
                nonRemovableEdge.insert(edges.front());
                break;
            }
        }
    }
    return solution;
}

template <typename Rules, typename Weight>
EarRouting<Rules> runForceDemands(int _maxRule, const DiGraph& topology,
    const std::set<Demand>& demands) {

    EarRouting<Rules> ear(topology, _maxRule);

    if (!ear.template forceRouteAllDemands<Weight>(demands)) {
        return ear;
    }
    if (!ear.template cleanFullRules<Weight>()) {
        return ear;
    }
    EarRouting<Rules> solution = ear;
    std::set<Edge> nonRemovableEdge;

    while (true) {
        auto edges = ear.getRemovableEdges(nonRemovableEdge);
        if (edges.empty()) {
            break;
        }

        bool good;
        for (const auto& edge : edges) {
            auto removedDemands = ear.deleteLink(edge);
            good = false;
            if (ear.template forceRouteAllDemands<Weight>(removedDemands)) {
                if (ear.template cleanFullRules<Weight>()) {
                    solution = ear;
                    good = true;
                }
            }
            if (!good) {
                nonRemovableEdge.insert(edge);
                ear = solution;
                break;
            }
        }
    }
    return solution;
}

template <typename Rules>
std::function<EarRouting<Rules>(int, const DiGraph&, const std::set<Demand>&)>
getHeuristic(const std::string& heuristicName,
    const std::string& weightFunctionName) {

    typedef std::function<EarRouting<Rules>(int, const DiGraph&,
        const std::set<Demand>&)>
        HeuristicFunction;
#define findHeuristic(name, func)                                              \
    if (heuristicName.compare(#name) == 0) {                                   \
        if (weightFunctionName.compare("dumb") == 0) {                         \
            run = func<Rules, typename EarRouting<Rules>::DumbWeight>;         \
        } else if (weightFunctionName.compare("metric") == 0) {                \
            run = func<Rules,                                                  \
                typename EarRouting<Rules>::template MetricWeight<1, 1>>;      \
        } else if (weightFunctionName.compare("metricPlusOne") == 0) {         \
            run = func<Rules, typename EarRouting<                             \
                                  Rules>::template MetricPlusOneWeight<1, 1>>; \
        } else if (weightFunctionName.compare("khoa") == 0) {                  \
            run = func<Rules, typename EarRouting<Rules>::KhoaWeight>;         \
        } else if (weightFunctionName.compare("metricPlusOneCapa") == 0) {     \
            run = func<Rules,                                                  \
                typename EarRouting<Rules>::template MetricWeight<1, 0>>;      \
        } else if (weightFunctionName.compare("metricPlusOneRules") == 0) {    \
            run = func<Rules,                                                  \
                typename EarRouting<Rules>::template MetricWeight<0, 1>>;      \
        } else if (weightFunctionName.compare("metricPlusOne75Rules") == 0) {  \
            run = func<Rules,                                                  \
                typename EarRouting<Rules>::template MetricWeight<1, 3>>;      \
        } else if (weightFunctionName.compare("metricPlusOne75Capa") == 0) {   \
            run = func<Rules,                                                  \
                typename EarRouting<Rules>::template MetricWeight<3, 1>>;      \
        }                                                                      \
    }

    HeuristicFunction run;

    findHeuristic(full, runFullRestart) else findHeuristic(
        path, runPathRestart) else findHeuristic(forceAll, runForceDemands) else {
        std::cout << "No heuristic given!" << std::endl;
        exit(-1);
    }
    return run;
}

template <typename Rules>
void launch(int _nbRules, const DiGraph& topology,
    const std::set<Demand>& demands, const std::string& name,
    const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl
              << "Percent: " << percent << std::endl;

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();
    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);
    // auto solution = routeDemandsSpanning<Rules>(_nbRules, topology, demands);
    auto solution = run(_nbRules, topology, demands);

    std::cout
        << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
               std::chrono::high_resolution_clock::now() - t1)
               .count()
        << "ms. " << std::endl;
    if (heuristicName.compare("forceOnce") != 0 and (std::size_t) solution.getNbDemands() != demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
        for (auto& edge : solution.getResidual().getEdges()) {
            auto resi = solution.getResidual().getEdgeWeight(edge);
            std::cout << edge << " " << resi << std::endl;
        }
        for (std::size_t i = 0; i < solution.getRules().size(); ++i) {
            auto& rule = solution.getRules()[i];
            if (!rule.isCompressionValid()) {
                std::cout << "Compression not valid for " << i << std::endl;
            }
            std::cout << i << ":" << rule.getCompressedRules().size() << "/"
                      << rule.getActualNbRules() << std::endl;
        }
        return;
    }

    solution.saveMetrics("./results/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_" + std::to_string(_nbRules) + "_" + std::to_string(percent) + ".txt");

    std::cout << "Found solution with " << solution.getNbActiveLinks()
              << " active links."
              << "\t" << solution.getEnergySavings()
              << "% energy savings. Max mem : " << solution.getMaxMem()
              << std::endl;

    for (std::size_t i = 0; i < solution.getRules().size(); ++i) {
        auto& rule = solution.getRules()[i];
        if (!rule.isCompressionValid()) {
            std::cout << "Compression not valid for " << i << std::endl;
        }
        std::cout << i << ':' << rule.getCompressedRules().size() << "/"
                  << rule.getActualNbRules() << '\t'
                  << 100 * (rule.getCompressedRules().size() / (double)rule.getActualNbRules())
                  << '\t' << solution.getResidual().getOutDegree(i) << '\t';
        if (rule.getCompressedRules().size() <= 3) {
            for (auto& r : rule.getCompressedRules()) {
                std::cout << r << ", ";
            }
        }
        std::cout << std::endl;
    }
}

template <typename Rules>
void searchMini(const DiGraph& topology, const std::set<Demand>& demands,
    const std::string& name, const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);

    int minRules = 0;
    int maxRules = 10000;

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl;

    int nbRulesTest, nbRules;
    EarRouting<Rules> solution(topology, 0);

    while (minRules + 1 < maxRules) {
        nbRulesTest = (minRules + maxRules) / 2;
        std::cout << "Test with " << nbRulesTest << " max mem =>" << std::flush;
        auto ear = run(nbRulesTest, topology, demands);
        if ((std::size_t)ear.getNbDemands() != demands.size()) {
            minRules = nbRulesTest;
            std::cout << " failed. " << ear.getNbDemands() << '/' << demands.size()
                      << std::endl;
        } else {
            std::cout << " success." << std::endl;
            maxRules = nbRulesTest;
            nbRules = nbRulesTest;
            std::swap(solution, ear);
        }
    }
    if ((std::size_t)solution.getNbDemands() != demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
        return;
    }

    solution.saveMetrics("./results/" + name + "/" + heuristicName + "_" + weightFunctionName + "_" + _rulesName + "_mini_" + std::to_string(percent) + ".txt");
    std::cout << "Found solution with " << solution.getNbActiveLinks()
              << " active links."
              << " " << solution.getEnergySavings()
              << "% energy savings. Max mem : " << solution.getMaxMem()
              << std::endl;

    for (std::size_t i = 0; i < solution.getRules().size(); ++i) {
        auto& rule = solution.getRules()[i];
        if (!rule.isCompressionValid()) {
            std::cout << "Compression not valid for " << i << std::endl;
            for (auto& neighbor : solution.getResidual().getNeighbors(i)) {
                std::cout << neighbor << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << i << ":" << rule.getCompressedRules().size() << "/"
                  << rule.getActualNbRules() << std::endl;
    }
}

void findSolution(int argc, char** argv) {
    std::string name(argv[1]);
    auto demands = loadDemandsFromFile(
        std::string("./instances/" + name + "_demand.txt"), parameters.percent);
    auto topology =
        DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));
    std::string strNbRules(argv[5]);

    int nbRules;

    if (strNbRules.compare("max") == 0) {
        nbRules = std::numeric_limits<int>::max();
    } else if (strNbRules.compare("mini") == 0) {
        searchMini<AllWildcardRules>(topology, demands, name, "AllWildcardRules",
            argv[4], argv[3], parameters.percent);
        return;
    } else {
        nbRules = std::stoi(strNbRules);
    }

    launch<AllWildcardRules>(nbRules, topology, demands, name, "AllWildcardRules",
        argv[4], argv[3], parameters.percent);
}

int main(int argc, char** argv) {
    std::string network = std::string(argv[1]);
    parameters.rules = std::string(argv[2]);
    parameters.heuristic = std::string(argv[3]);
    parameters.weight = std::string(argv[4]);
    parameters.nbRules = std::stoi(argv[5]);
    parameters.percent = std::stod(argv[6]);
    findSolution(argc, argv);
    return 0;
}