#include <cassert>
#include <chrono>
#include <fstream> //loadDemandsFromFile
#include <functional>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <utility> //loadDemandsFromFile
#include <vector>  //loadDemandsFromFile

#include "CplexRules.hpp"
#include "DefaultPortRules.hpp"
#include "WildcardRules.hpp"

#include "DiGraph.hpp"
#include "Routing.hpp"

#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

std::vector<Demand> loadDemandsFromFile(const std::string& _filename,
    double percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!" << std::endl;
        exit(-1);
    }
    int s, t;
    double d;

    std::string line;
    while (ifs.good()) {
        line = "";
        std::getline(ifs, line);
        // std::cout << line << std::endl;
        std::stringstream lineStream(line);
        if (line != "") {
            lineStream >> s >> t >> d;
            // std::cout << s << ", " << t << ", "<< d << std::endl;
            demands.emplace_back(s, t, d * percent);
        }
    }
    return demands;
}

template <typename Rules, typename Weight>
Routing<Rules> runSave(int _maxRule, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands) {
    Routing<Rules> ear(topology, _nbServers, _nbSwitches, _maxRule);
    ear.template routeAllDemandsNoLimit<Weight>(demands, false);
    return ear;
}

template <typename Rules>
std::function<Routing<Rules>(int, const DiGraph&, int, int,
    const std::vector<Demand>&)>
getHeuristic(const std::string& heuristicName,
    const std::string& weightFunctionName) {

    std::function<Routing<Rules>(int, const DiGraph&, int, int,
        const std::vector<Demand>&)>
        run;
    if (weightFunctionName.compare("dumb") == 0) {
        run = runSave<Rules, typename Routing<Rules>::DumbWeight>;
    } else if (weightFunctionName.compare("metric") == 0) {
        run = runSave<Rules, typename Routing<Rules>::template MetricWeight<1, 1>>;
    } else if (weightFunctionName.compare("metricPlusOne") == 0) {
        run = runSave<Rules,
            typename Routing<Rules>::template MetricPlusOneWeight<1, 1>>;
    } else if (weightFunctionName.compare("metricPlusOneCapa") == 0) {
        run = runSave<Rules, typename Routing<Rules>::template MetricWeight<1, 0>>;
    } else if (weightFunctionName.compare("metricPlusOneRules") == 0) {
        run = runSave<Rules, typename Routing<Rules>::template MetricWeight<0, 1>>;
    } else if (weightFunctionName.compare("metricPlusOne75Rules") == 0) {
        run = runSave<Rules, typename Routing<Rules>::template MetricWeight<1, 3>>;
    } else if (weightFunctionName.compare("metricPlusOne75Capa") == 0) {
        run = runSave<Rules, typename Routing<Rules>::template MetricWeight<3, 1>>;
    } else {
        std::cerr << "No weight metric given!" << std::endl;
        exit(-1);
    }
    return run;
}

template <typename Rules>
void launch(int _nbRules, const DiGraph& topology, int _nbServers,
    int _nbSwitches, const std::vector<Demand>& demands,
    const std::string& name, const std::string& _rulesName,
    const std::string& weightFunctionName,
    const std::string& heuristicName, double percent) {

    std::cout << "Name : " << name << std::endl
              << "Heurisitc : " << heuristicName << std::endl
              << "Rules : " << _rulesName << std::endl
              << "Weight : " << weightFunctionName << std::endl
              << "Percent: " << percent << std::endl;

    std::chrono::high_resolution_clock::time_point t1 =
        std::chrono::high_resolution_clock::now();
    auto run = getHeuristic<Rules>(heuristicName, weightFunctionName);

    auto solution = run(_nbRules, topology, _nbServers, _nbSwitches, demands);
    double pathFindingTime =
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(
            std::chrono::high_resolution_clock::now() - t1)
            .count();
    std::cout << pathFindingTime << "ms. " << std::endl;

    if (solution.getNbDemands() != (int)demands.size()) {
        std::cout << "No solution found! (" << solution.getNbDemands() << '/'
                  << demands.size() << ')' << std::endl;
    } else {
        std::cout << "Found solution with " << solution.getNbActiveLinks()
                  << " active links."
                  << "\t" << solution.getEnergySavings()
                  << "% energy savings. Max mem : " << solution.getMaxMem()
                  << std::endl;
    }

    for (auto& i : solution.getVertices()) {
        if (solution.isSwitch(i)) {
            auto& rule = solution.getRules(i);
            std::cout << i << ':' << rule.getCompressedRules().size() << "/"
                      << rule.getActualNbRules() << '\t'
                      << 100 * (rule.getCompressedRules().size() / (double)rule.getActualNbRules())
                      << '\t' << solution.getDensity(i) << '\t'
                      << solution.getResidual().getOutDegree(i) << std::endl;
        }
    }

    if (solution.getNbDemands() == (int)demands.size()) {
        for (auto& i : solution.getVertices()) {
            if (solution.isSwitch(i)) {
                std::string filename =
                    "./rules/" + name + "_" + std::to_string(i) + ".rules";
                solution.getRules(i).saveRules(filename);
            }
        }
    }
}

void findSolution(int argc, char** argv) {
    std::string name(argv[1]);
    auto demands = loadDemandsFromFile(
        std::string("./instances/" + name + "_demand.txt"), std::stod(argv[6]));
    auto topology =
        DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));

    std::string strNbRules(argv[5]);

    int nbRules = std::stoi(strNbRules);

    if (std::string(argv[2]) == std::string("WC")) {
        launch<WildcardRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "WildcardRules",
            argv[4], argv[3], std::stod(argv[6]));
    } else if (std::string(argv[2]) == std::string("DP")) {
        launch<DefaultPortRules>(nbRules, std::get<0>(topology),
            std::get<1>(topology), std::get<2>(topology),
            demands, name, "DefaultPortRules", argv[4],
            argv[3], std::stod(argv[6]));
    } else if (std::string(argv[2]) == std::string("CPLEX")) {
        launch<CplexRules>(nbRules, std::get<0>(topology), std::get<1>(topology),
            std::get<2>(topology), demands, name, "CplexRules",
            argv[4], argv[3], std::stod(argv[6]));
    } else {
        std::cerr << "No Rule type given!" << std::endl;
        exit(-1);
    }
}

int main(int argc, char** argv) {
    findSolution(argc, argv);
    return 0;
}